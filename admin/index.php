<?php
require_once '../include/common.inc.php';
require_once 'function.php';
$auth_rules=auth_group($_SESSION['login_gid']);
?>
<!DOCTYPE HTML>
<html>
 <head>
  <title><?=$cfg['config']['title']?></title>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <link href="assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/bui-min.css" rel="stylesheet" type="text/css" />
   <link href="assets/css/main-min.css" rel="stylesheet" type="text/css" />
 </head>
 <body>

  <div class="header">
    
      <div class="dl-title">
          <span class="dl-title-text">直播室后台管理系统</span>
      </div>

    <div class="dl-log"><a href="../" target="_blank" style="color: #c1d5ec; margin-right: 10px;">返回首页</a>欢迎您，<span class="dl-log-user"><?=$_SESSION[admincp]?></span><a href="login.php?act=user_logout" title="退出系统" class="dl-log-quit">[退出]</a>
    </div>
  </div>
   <div class="content">
    <div class="dl-main-nav">
      <div class="dl-inform"><div class="dl-inform-title"><s class="dl-inform-icon dl-up"></s></div></div>
      <ul id="J_Nav"  class="nav-list ks-clear">
        <li class="nav-item dl-selected" <?php if(stripos($auth_rules,'sys_')===false)echo " style='display:none'";?>><div class="nav-item-inner nav-home">系统设置</div></li>
		<li class="nav-item" <?php if(stripos($auth_rules,'room_')===false)echo " style='display:none'";?>><div class="nav-item-inner nav-marketing">房间管理</div></li>
        <li class="nav-item" <?php if(stripos($auth_rules,'users_')===false)echo " style='display:none'";?>><div class="nav-item-inner nav-supplier">用户管理</div></li>
        <li class="nav-item" <?php if(stripos($auth_rules,'apps_')===false)echo " style='display:none'";?>><div class="nav-item-inner nav-order">应用管理</div></li>
		<li class="nav-item" <?php if(stripos($auth_rules,'tongji_')===false)echo " style='display:none'";?>><div class="nav-item-inner nav-marketing">统计</div></li>
      </ul>
    </div>
    <ul id="J_NavContent" class="dl-tab-conten">

    </ul>
   </div>
  <script type="text/javascript" src="assets/js/jquery-1.8.1.min.js"></script>
  <script type="text/javascript" src="./assets/js/bui.js"></script>
  <script type="text/javascript" src="./assets/js/config.js"></script>

  <script>
    BUI.use('common/main',function(){
		var config = [{
				id:'sys', 
				homePage : '3',
				menu:[{
					text:'系统设置',
					items:[
						<?php //if(stripos($auth_rules,'sys_base')!==false) echo "{id:'1',text:'基本配置',href:'sys/base.php'},";?>
						<?php //if(stripos($auth_rules,'sys_server')!==false) echo "{id:'2',text:'聊天&直播&机器人',href:'sys/server.php'},";?>
						<?php if(stripos($auth_rules,'sys_ban')!==false) echo "{id:'3',text:'用户&IP屏蔽',href:'sys/ban.php'},";?>
						<?php if(stripos($auth_rules,'user_send')!==false) echo "{id:'4',text:'禁言管理',href:'sys/send.php'},";?>
						<?php if(stripos($auth_rules,'sys_notice')!==false) echo " {id:'5',text:'公告板',href:'sys/notice.php'},";?>
						<?php if(stripos($auth_rules,'chongzhi')!==false) echo " {id:'6',text:'充值记录',href:'sys/chongzhi.php'},";?>
						<?php if(stripos($auth_rules,'tixian')!==false) echo " {id:'7',text:'提现申请',href:'sys/tixian.php'},";?>
						<?php if(stripos($auth_rules,'sys_chat_log')!==false) echo "{id:'8',text:'聊天记录',href:'sys/chat_log.php'},";?>
						<?php if(stripos($auth_rules,'sys_log')!==false) echo "{id:'9',text:'日志管理',href:'sys/log.php'},";?>
						<?php if(stripos($auth_rules,'sys_reply')!==false) echo "{id:'10',text:'快捷回复',href:'sys/reply.php'},";?>
						<?php if(stripos($auth_rules,'sys_service')!==false) echo "{id:'11',text:'在线客服',href:'sys/service.php'},";?>
						<?php if(stripos($auth_rules,'sys_sms')!==false) echo "{id:'12',text:'短信设置',href:'sys/sms.php'},";?>
						<?php if(stripos($auth_rules,'sys_alipay_config')!==false) echo "{id:'13',text:'支付设置',href:'sys/alipay_config.php'},";?>
					]
				}]
			},{
				id:'room',
				homePage : '1',
				menu:[{
					text:'房间管理',
					items:[
						<?php if(stripos($auth_rules,'room_manage')!==false) echo "{id:'1',text:'房间设置',href:'room/room_list.php'},";?>
						<?php if(stripos($auth_rules,'room_servicer')!==false) echo "{id:'2',text:'客服设置',href:'room/room_servicer.php'},";?>
						<?php if(stripos($auth_rules,'room_manager')!==false) echo "{id:'3',text:'管理员设置',href:'room/room_manager.php'},";?>
						<?php if(stripos($auth_rules,'gift_list')!==false) echo "{id:'4',text:'礼物管理',href:'room/gift_list.php'},";?>
					]
				}]
			},{
				id:'users',
				homePage : '1',
				menu:[{
					text:'用户管理',
					items:[
						<?php //if(stripos($auth_rules,'users_admin')!==false) echo "{id:'0',text:'我的用户',href:'users/users.php?fuser={$_SESSION[admincp]}'},";?>
						<?php if(stripos($auth_rules,'users_admin')!==false) echo "{id:'1',text:'用户管理',href:'users/users.php'},";?>
						<?php if(stripos($auth_rules,'users_admin')!==false) echo "{id:'2',text:'游客管理',href:'users/users.php?gid=0'},";?>
						<?php if(stripos($auth_rules,'users_group')!==false) echo "{id:'3',text:'分组管理',href:'users/group.php'},";?> 
						<?php if (stripos($auth_rules, 'users_invite') !== false) echo "{id:'4',text:'推广客服',href:'users/invite.php'},"; ?> 
						<?php if (stripos($auth_rules, 'robots_admin') !== false) echo "{id:'5',text:'机器人管理',href:'users/robots.php'},"; ?> 
						<?php if (stripos($auth_rules, 'user_robotmsg') !== false) echo "{id:'6',text:'机器人发言管理',href:'users/robotmsg.php'},"; ?> 
						<?php if (stripos($auth_rules, 'users_signin') !== false) echo "{id:'7',text:'签到统计',href:'users/signin.php'},"; ?> 
						<?php if (stripos($auth_rules, 'user_redbag_log') !== false) echo "{id:'8',text:'红包记录',href:'users/redbag_log.php'},"; ?> 
						<?php if (stripos($auth_rules, 'user_gift_log') !== false) echo "{id:'9',text:'礼物记录',href:'users/gift_log.php'},"; ?> 
					]
				}]
			},{
				id:'app',
				homePage : '1',
				menu:[{
					text:'应用列表',
					items:[
					<?php if(stripos($auth_rules,'apps_hd')!==false) echo "{id:'1',text:'喊单系统',href:'apps/app_hd.php'},";?>
					<?php if(stripos($auth_rules,'apps_wt')!==false) echo "{id:'2',text:'问题答疑',href:'apps/app_wt.php'},";?>
					<?php if(stripos($auth_rules,'apps_jyts')!==false) echo "{id:'3',text:'交易提示',href:'apps/app_jyts.php'},";?>
					<?php if(stripos($auth_rules,'apps_scpl')!==false) echo "{id:'4',text:'市场评论',href:'apps/app_scpl.php'},";?>
					<?php if(stripos($auth_rules,'apps_files')!==false) echo "{id:'5',text:'共享文档',href:'apps/app_files.php'},";?>
					<?php if(stripos($auth_rules,'apps_rank')!==false) echo "{id:'6',text:'服务排行榜',href:'apps/app_rank.php'},";?>
					<?php if(stripos($auth_rules,'sys_taped')!==false) echo "{id:'7',text:'录播视频',href:'apps/taped.php'},";?>
					<?php if(stripos($auth_rules,'apps_course')!==false) echo "{id:'8',text:'课程表',href:'apps/app_course.php'},";?>
					<?php if(stripos($auth_rules,'apps_teacher')!==false) echo "{id:'9',text:'讲师榜单',href:'apps/app_teacher.php'},";?>
					]
				},
				{
					text:'边栏应用',
					items:[
						<?php if(stripos($auth_rules,'apps_manage')!==false) echo "{id:'tab',text:'应用管理',href:'apps/app_manage.php'},";?>
						<?php if (stripos($auth_rules, 'header_manage')!== false) echo "{id:'tab2',text:'头部导航',href:'apps/header_manage.php'},"; ?>                  
					]
				},
				{
					text:'抽奖',
					items:[
						<?php if(stripos($auth_rules,'apps_rotary')!==false) echo "{id:'rotary',text:'转盘图片',href:'apps/app_rotary.php'},";?>
						<?php if(stripos($auth_rules,'apps_prize')!==false) echo "{id:'prize',text:'奖品设置',href:'apps/app_prize.php'},";?>
						<?php if (stripos($auth_rules, 'apps_lottery')!== false) echo "{id:'lottery',text:'抽奖记录',href:'apps/app_lottery.php'},"; ?>                  
					]
				},
				{
					text:'其他',
					items:[
						<?php if(stripos($auth_rules,'apps_vote')!==false) echo "{id:'vote',text:'投票管理',href:'apps/app_vote.php'},";?>
						<?php if(stripos($auth_rules,'apps_hd_pro')!==false) echo "{id:'hd_pro',text:'喊单商品管理',href:'apps/app_hd_pro.php'},";?>
					]
				}]
			},{
				id:'tj',
				homePage : '1',
				menu:[{
					text:'讲师统计',
					items:[
						<?php if(stripos($auth_rules,'apps_hd')!==false) echo "{id:'1',text:'发展会员数',href:'tongji/tj_reg.php?type=newuser'},";?>
						<?php if(stripos($auth_rules,'apps_hd')!==false) echo "{id:'2',text:'访客数',href:'tongji/tj_login.php?type=loginroom'},";?>
						<?php if(stripos($auth_rules,'apps_hd')!==false) echo "{id:'3',text:'在线人数',href:'tongji/tj_online2.php?type=loginroom'},";?>
					]
				}]
		  }];
      new PageUtil.MainPage({
        modulesConfig : config
      });
    });
  </script>
 </body>
</html>
