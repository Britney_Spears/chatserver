<?php
require_once '../../include/common.inc.php';
require_once '../function.php';
if(stripos(auth_group($_SESSION['login_gid']),'room_manage')===false)exit("没有权限！");

$query=$db->query("select id,title,logo,state from {$tablepre}config order by id asc");
while($row = $db->fetch_row($query)) {
	$rooms[] = $row;
}

switch($type){
	case 'del_room':
		if($roomid == '1001') {
			echo '<script>alert("默认房间不能删除");</script>';
		} else {
			$sql = "delete from {$tablepre}config where id='{$roomid}'";
			$db->query($sql);
		}
		echo '<script>location.href="room_list.php";</script>';
	break;
}

?>
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
<style type="text/css">
code { padding: 0px 4px; color: #d14; background-color: #f7f7f9; border: 1px solid #e1e1e8; }
</style>
<script>
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
function ftime(time){
	return new Date(time*1000).Format("yyyy-MM-dd hh:mm"); ; 
}
</script>
</head>
<body>
<div class="container"  style=" min-width:1100px;">

	<ul class="breadcrumb">
		<a href="room_edit.php" class="button  button-success">添加房间</a>
	</ul>
	<table  class="table table-bordered table-hover definewidth m10">
		<tr style="font-weight:bold" >
			<td width="40" align="center" bgcolor="#FFFFFF">房间号</td>
			<td width="80" align="center" bgcolor="#FFFFFF">房间名</td>
			<td width="80" align="center" bgcolor="#FFFFFF">Logo</td>
			<td width="40" align="center" bgcolor="#FFFFFF">状态</td>
			<td width="80" align="center" bgcolor="#FFFFFF">操作</td>
		</tr>
		<?php
			foreach($rooms as $room) {
		?>
		<tr>
			<td><?php echo $room['id']; ?></td>
			<td><a href="/room/index.php?rid=<?php echo $room['id']; ?>" target="_blank"><?php echo $room['title']; ?></a></td>
			<td><img src="<?php echo $room['logo']; ?>" style="width: 110px; height: 26px;"/></td>
			<td>
                            <?php 
                                if($room['state'] == '1') {
                                    echo '启用';
                                } elseif($room['state'] == '2') {
                                    echo '加密';
                                } elseif($room['state'] == '3') {
                                    echo '限时';
                                } else {
                                    echo '关闭';
                                }
                            ?>
                        </td>
			<td>
				<a href="room_edit.php?roomid=<?php echo $room['id']; ?>">修改</a>
				<a href="javascript:void(0);" onclick="del_room(<?php echo $room['id']; ?>)" <?php if($room['id'] == '1001'){echo 'style="display: none;"';}?>>删除</a>
			</td>
		</tr>
		<?php } ?>
	</table>

</div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script> 
<script type="text/javascript" src="../assets/js/bui.js"></script> 
<script type="text/javascript" src="../assets/js/config.js"></script> 
<script type="text/javascript">

	function del_room(roomid){
		if(confirm('确定删除房间？')) {
			location.href = '?type=del_room&roomid=' + roomid;
		}
	}

</script>

</body>
</html>
