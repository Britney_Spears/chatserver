<?php
require_once '../../include/common.inc.php';
require_once '../function.php';
if(stripos(auth_group($_SESSION['login_gid']), 'gift_list')===false)exit("没有权限！");

switch($act){
    case 'del_gift':
        $sql = "delete from {$tablepre}gift_goods where id='{$id}'";
        $db->query($sql);
        echo '<script>location.href="gift_list.php";</script>';
    break;
}

?>
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
<style type="text/css">
code { padding: 0px 4px; color: #d14; background-color: #f7f7f9; border: 1px solid #e1e1e8; }
</style>
<script>
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
function ftime(time){
	return new Date(time*1000).Format("yyyy-MM-dd hh:mm"); ; 
}
</script>
</head>
<body>
<div class="container"  style=" min-width:1100px;">
	<ul class="breadcrumb">
            <a href="gift_edit.php" class="button  button-success">添加礼物</a>
	</ul>
	<table  class="table table-bordered table-hover definewidth m10">
            <tr style="font-weight:bold" >
                <td width="40" align="center" bgcolor="#FFFFFF">ID</td>
                <td width="80" align="center" bgcolor="#FFFFFF">礼物名称</td>
                <td width="80" align="center" bgcolor="#FFFFFF">价格</td>
                <td width="40" align="center" bgcolor="#FFFFFF">含义</td>
                <td width="40" align="center" bgcolor="#FFFFFF">图标</td>
                <td width="80" align="center" bgcolor="#FFFFFF">操作</td>
            </tr>
            <?php
                
                $sql = "select * from {$tablepre}gift_goods";
                $count = $db->num_rows($db->query($sql));
                pageft($count, 20, "");
                $sql .= " order by id asc";
                $sql .= " limit $firstcount,$displaypg";
                $query = $db->query($sql);
                echo for_each($query,'<tr>
                                            <td>{id}</td>
                                            <td>{name}</td>
                                            <td>{price}</td>
                                            <td>{msg}</td>
                                            <td><img src="{ico}" width="40" height="40"></td>
                                            <td>
                                                <a href="gift_edit.php?giftid={id}">修改</a>
                                                <a href="javascript:void(0);" onclick="del_gift({id})">删除</a>
                                            </td>
                                        </tr>');
            
            ?>
	</table>
        <ul class="breadcrumb">
            <li class="active"><?=$pagenav?>
            </li>
        </ul>
</div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script> 
<script type="text/javascript" src="../assets/js/bui.js"></script> 
<script type="text/javascript" src="../assets/js/config.js"></script> 
<script type="text/javascript">
    function del_gift(id){
        if(confirm('确定删除该礼物？')) {
            location.href = '?act=del_gift&id=' + id;
        }
    }
</script>
</body>
</html>
