<?php
require_once '../../include/common.inc.php';
require_once '../function.php';
if(stripos(auth_group($_SESSION['login_gid']),'sys_server')===false)exit("没有权限！");

if($act=="config_edit"){
	$arr[tserver]=$tserver;
	$arr[livefp]=$livefp;
	$arr[phonefp]=$phonefp;
	$arr[rebots]=$rebots;
	$arr[defkf]=$defkf;
	$arr[vserver]=$vserver;
	$arr[livetype]=$livetype;
	$arr[defkf]=$defkf;
	
	config_edit($arr);
}


$query=$db->query("select * from  {$tablepre}config where id=1");
$row=$db->fetch_row($query);
?>
<!DOCTYPE HTML>
<html>
 <head>
  <title> </title>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
       <link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />   <!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
   <link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
   <style type="text/css">
    code {
      padding: 0px 4px;
      color: #d14;
      background-color: #f7f7f9;
      border: 1px solid #e1e1e8;
    }
   </style>
 </head>
 <body>
  
    
    <div class="container">
     <form action="" method="post" enctype="application/x-www-form-urlencoded">
        <table class="table table-bordered table-hover definewidth m10">
          <tr>
            <td class="tableleft" style="width:100px;">文字服务器：</td>
            <td><input name="tserver" type="text" id="tserver" style="width:400px;" value="<?=$row[tserver]?>"/></td>
          </tr>
          <tr>
            <td class="tableleft">视频服务器：</td>
            <td><input name="vserver" type="text" id="vserver" style="width:400px;" value="<?=$row[vserver]?>"/></td>
          </tr>
          <tr>
            <td class="tableleft">直播模块：</td>
            <td><select name="livetype" id="livetype">
            <option value="<?=$row[livetype]?>"><?=$row[livetype]?>-保持不变</option>
              <option value="1">1-站外转播</option>
              <option value="0">0-自带直播</option>
            </select></td>
          </tr>
          <tr>
            <td class="tableleft">电脑直播代码：</td>
            <td><textarea name="livefp" rows="10" id="livefp" style="width:99%; height:100px;"><?=$row[livefp]?></textarea>
              <br>
              <a href="../../ckplayer/demo.htm" target="_blank">示例</a></td>
          </tr>
          <tr>
            <td class="tableleft">手机直播代码：</td>
            <td><textarea name="phonefp" rows="10" id="phonefp" style="width:99%;height:100px;"><?=$row[phonefp]?></textarea></td>
          </tr>
          <tr>
            <td class="tableleft">机器人在线：</td>
            <td><input name="rebots" type="text" id="rebots"  value="<?=$row[rebots]?>"/>
            20|50 20至50人</td>
          </tr>
          <tr>
            <td class="tableleft">默认客服：</td>
            <td><input name="defkf" type="text" id="defkf"  value="<?=$row[defkf]?>"/></td>
          </tr>
          <tr>
            <td class="tableleft">&nbsp;</td>
            <td><button type="submit" class="button button-success"> 保存 </button><input type="hidden" name="act" value="config_edit"></td>
          </tr>
        </table>
      </form>
     
 </div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script> 

<body>
</html>  