<?php
require_once '../../include/common.inc.php';
require_once '../function.php';
if(stripos(auth_group($_SESSION['login_gid']), 'gift_list')===false)exit("没有权限！");

if($act == "gift_edit"){
    $arr['name'] = $name;
    $arr['price'] = $price;
    $arr['msg'] = $msg;
    $arr['ico'] = $ico;
    if($op == 'add') {
		//echo '1';exit;
        gift_edit($op, $arr);
        echo '<script>location.href="gift_list.php";</script>';
    } elseif($op == 'edit') {
		//echo '2';exit;
        gift_edit($op, $arr, $giftid);
        echo '<script>location.href="gift_edit.php?giftid='.$giftid.'";</script>';
    }
} elseif($act == 'edit_ico') {
		//echo '3';exit;
    $ret['code'] = 'err';
    $res = rename('../..'.urldecode($path), '../../gift/img/'.$giftid.'.png');
    if($res) {
        $ret['code'] = 'ok';
        $ret['url'] = '../../gift/img/'.$giftid.'.png';
    }
    exit(json_encode($ret));
}

if(!empty($giftid)) {
    $query=$db->query("select * from  {$tablepre}gift_goods where id={$giftid}");
    $row=$db->fetch_row($query);
}

?>
<!DOCTYPE HTML>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
    <!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
    <link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        code {
            padding: 0px 4px;
            color: #d14;
            background-color: #f7f7f9;
            border: 1px solid #e1e1e8;
        }
        input, button {
            vertical-align:middle
        }
    </style>
</head>
<body>
<div class="container">
    <form action="" method="post" enctype="application/x-www-form-urlencoded">
        <table class="table table-bordered table-hover definewidth m10">
            <tr>
                <td class="tableleft" style="width:100px;">名称：</td>
                <td><input name="name" type="text" id="name" style="width:400px;" value="<?=$row['name']?>"/></td>
            </tr>
            <tr>
                <td class="tableleft" style="width:100px;">价格：</td>
                <td><input name="price" type="text" id="price" style="width:400px;" value="<?=$row['price']?>"/></td>
            </tr>
            <tr>
                <td class="tableleft">含义：</td>
                <td>
                    <textarea name="msg" rows="2" id="msg" style="width:400px;"><?=$row['msg']?></textarea>
                </td>
            </tr>
			<tr>
				<td class="tableleft">图标：</td>
				<td>
					<input name="ico" type="text" id="ico" style="width:350px;" value="<?=$row['ico']?>"/>
					<button  type="button" class="button button-mini button-success" ><span id="ico_up_bnt"></span></button>
				</td>
			</tr>			
<!--            <tr>
                <td class="tableleft">图标：</td>
                <td><img src="" id="ico_img" style="display: none;"><button  type="button" class="button button-mini button-success"><span id="ico"></span></button></td>
            </tr>-->
            <tr>
                <td class="tableleft"></td>
                <td>
                    <button type="submit" class="button button-success"> 保存 </button>
                    <input type="hidden" name="giftid" value="<?=$row['id']?>">
                    <input type="hidden" name="act" value="gift_edit">
                    <input type="hidden" name="op" value="<?php echo empty($giftid) ? 'add' : 'edit'; ?>"/>
                </td>
            </tr>
        </table>
    </form>
</div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script>
<script type="text/javascript" src="../../upload/swfupload/swfupload.js"></script>
<script>
    /*function swfupload_ok(fileObj, server_data){
        console.log(fileObj);
        console.log(server_data);
        var data = eval("("+server_data+")") ;
//        $("#"+data.msg.info).val(data.msg.url);
        $.getJSON('gift_edit.php?act=edit_ico&path='+encodeURI(data.msg.url)+'&giftid='+$('input[name="giftid"]').val(), function(data) {
            if(data.code == 'ok') {
                $('#ico_img').attr('src', data.url);
                $('#ico_img').show();
            } else {
                alert('上传失败');
            }
        });
    })*/
    function swfupload_ok(fileObj,server_data){
        var data = eval("("+server_data+")") ;
        $("#"+data.msg.info).val(data.msg.url);
    }
    $(function(){
        var swfdef={
            // 按钮设置
            file_post_name:"filedata",
            button_width: 30,
            button_height: 18,
            button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
            button_cursor: SWFUpload.CURSOR.HAND,
            button_text: '上传',
            button_text_style: ".upbnt{ color:#00F}",
            button_text_left_padding: 2,
            button_text_top_padding: 2,
            upload_success_handler : swfupload_ok,
            file_dialog_complete_handler:function(){this.startUpload();},
            file_queue_error_handler:function(){alert("选择文件错误");}
        }
        swfdef.flash_url = "../../upload/swfupload/swfupload.swf";
        swfdef.button_placeholder_id = "ico_up_bnt";
        swfdef.file_types = "*.png;*.jpg;*.gif";
        swfdef.upload_url = "../../upload/upload.php";
        swfdef.post_params = {"info":"ico"}
        swfu = new SWFUpload(swfdef);
    });
</script>
</body>
</html>
