<?php
require_once '../../include/common.inc.php';
require_once '../function.php';
if(stripos(auth_group($_SESSION['login_gid']),'room_manage')===false)exit("没有权限！");

if($act=="config_edit"){
	$arr[id] = $rid;
	$arr[title] = $title;
	$arr[keys] = $keys;
	$arr[dc] = $dc;
	$arr[logo] = $logo;
	$arr[ico] = $ico;
	$arr[bg] = $bg;
	$arr[msgban] = $msgban;
	$arr[state] = $state;
	$arr[pwd] = $pwd;
	$arr[pwd_tip] = $pwd_tip;
	$arr[regaudit] = $regaudit;
	$arr[msgaudit] = $msgaudit;
	$arr[msglog] = $msglog;
	$arr[logintip] = $logintip;
	$arr[loginguest] = $loginguest;
	$arr[loginqq] = $loginqq;
	$arr[tongji] = $tongji;
	$arr[copyright] = $copyright;
	$arr[regban] = $regban;
	$arr[msgblock] = $msgblock;
	$arr[ewm] = $ewm;
	$arr[tserver] = $tserver;
	$arr[livefp0] = $livefp0;
	$arr[livefp1] = $livefp1;
	$arr[livefp2] = $livefp2;
	$arr[phonefp] = $phonefp;
	$arr[rebots] = $rebots;
	$arr[defkf] = $defkf;
	$arr[livetype] = $livetype;
	$arr[defkf] = $defkf;
	$arr[state] = $state;
	$arr[limit] = $limit;
	$arr[discount] = $discount;
	$arr[showvideo] = $showvideo;
	$arr[lottery] = $lottery;
	
	if($op == 'add') {
		config_add($arr);
		echo '<script>location.href="room_list.php";</script>';
	} elseif($op == 'edit') {
		config_edit($arr, $roomid);
		echo '<script>location.href="room_edit.php?roomid='.$rid.'";</script>';
	}
}

if(!empty($roomid)) {
	$query=$db->query("select * from  {$tablepre}config where id={$roomid}");
	$row=$db->fetch_row($query);
	
	//房间配置写入缓存	
	$cachePath = '../../cache/room_' . $rid . '.txt';
	file_put_contents($cachePath, json_encode($row));
}

?>
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
<style type="text/css">
code {
	padding: 0px 4px;
	color: #d14;
	background-color: #f7f7f9;
	border: 1px solid #e1e1e8;
}
input, button {
	vertical-align:middle
}
</style>
</head>
<body>
<div class="container">
  <form action="" method="post" enctype="application/x-www-form-urlencoded">
    <table class="table table-bordered table-hover definewidth m10">
      <tr>
        <td class="tableleft" style="width:100px;">房间号：</td>
        <td><input name="rid" type="text" id="rid" style="width:400px;" value="<?=$row[id]?>"/></td>
      </tr>
      <tr>
        <td class="tableleft" style="width:100px;">网站标题：</td>
        <td><input name="title" type="text" id="title" style="width:400px;" value="<?=$row[title]?>"/></td>
      </tr>
      <tr>
        <td class="tableleft">关 键 字：</td>
        <td><textarea name="keys" rows="2" id="keys" style="width:400px;"><?=$row[keys]?>
</textarea></td>
      </tr>
      <tr>
        <td class="tableleft">站点描述：</td>
        <td><textarea name="dc" id="dc" style="width:400px;"><?=$row[dc]?>
</textarea></td>
      </tr>
      <tr>
        <td class="tableleft">LOGO：</td>
        <td><input name="logo" type="text" id="logo" style="width:400px;" value="<?=$row[logo]?>"/>
          <button  type="button" class="button button-mini button-success"><span id="logo_up_bnt"></span></button></td>
      </tr>
      <tr>
        <td class="tableleft">ICO：</td>
        <td><input name="ico" type="text" id="ico" style="width:400px;" value="<?=$row[ico]?>"/>
          <button  type="button" class="button button-mini button-success" ><span id="ico_up_bnt"></span></button></td>
      </tr>
      <tr>
        <td class="tableleft">背景：</td>
        <td><input name="bg" type="text" id="bg" style="width:400px;" value="<?=$row[bg]?>"/>
          <button  type="button" class="button button-mini button-success" ><span id="bg_up_bnt"></span></button></td>
      </tr>
	        <tr>
        <td class="tableleft">二维码：</td>
        <td><input name="ewm" type="text" id="ewm" style="width:400px;" value="<?=$row[ewm]?>"/>
          <button  type="button" class="button button-mini button-success" ><span id="ewm_up_bnt"></span></button></td>
      </tr>
      <tr>
        <td class="tableleft">注册过滤：</td>
        <td><textarea name="regban" id="regban" style="width:400px;"><?=$row[regban]?>
</textarea></td>
      </tr>
      <tr>
        <td class="tableleft">聊天过滤：</td>
        <td><textarea name="msgban" id="msgban" style="width:400px;"><?=$row[msgban]?>
</textarea></td>
      </tr>
      <tr>
<td class="tableleft">系统状态：</td>
    <td>
        <select name="state" id="state" style="width:60px;" onChange="change_rstate(this)">
            <option value="<?=$row[state]?>">
                <?=$row[state]?>:不变
            </option>
            <option value="1">1开启</option>
            <option value="0">0关闭</option>
            <option value="2">2加密</option>
            <option value="3">3限时</option>
        </select>
        <label id="pwd_s" style="display:none;">
            &nbsp;&nbsp;密码：
            <input name="pwd" type="text" id="pwd" value="<?=$row[pwd]?>"/>
            &nbsp;&nbsp;加密提示信息：
            <input name="pwd_tip" type="text" id="pwd_tip" value="<?=$row[pwd_tip]?>"/>
        </label>
        <label id="limit" style="display:none;">
            &nbsp;&nbsp;限定观看时间：
            <input name="limit" type="text" id="limit" value="<?=$row[limit]?>" style="width: 50px;"/>(单位：分钟)
        </label>
    </td>
      </tr>
      <tr>
        <td class="tableleft">&nbsp;</td>
        <td>消息屏蔽：
          <label for="msgblock"></label>
          <select name="msgblock" id="msgblock" style="width:70px;">
            <option value="<?=$row[msgblock]?>">
            <?=$row[msgblock]?>
            :不变</option>
            <option value="1">1是</option>
            <option value="0">0否</option>
          </select>
          &nbsp;消息记录：
          <label for="msglog"></label>
          <select name="msglog" id="msglog" style="width:70px;">
            <option value="<?=$row[msglog]?>">
            <?=$row[msglog]?>
            :不变</option>
            <option value="1">1是</option>
            <option value="0">0否</option>
          </select>
         &nbsp; 消&nbsp;息&nbsp;审核：
          <label for="msgaudit"></label>
          <select name="msgaudit" id="msgaudit" style="width:70px;">
            <option value="<?=$row[msgaudit]?>">
              <?=$row[msgaudit]?>
              :不变</option>
            <option value="1">1是</option>
            <option value="0">0否</option>
          </select>
         &nbsp; 显示视频：
          <label for="showvideo"></label>
          <select name="showvideo" id="showvideo" style="width:70px;">
            <option value="<?=$row[showvideo]?>">
              <?=$row[showvideo]?>
              :不变</option>
            <option value="1">1是</option>
            <option value="0">0否</option>
          </select>
		  </td>
      </tr>
      <tr>
        <td class="tableleft">&nbsp;</td>
        <td>登录提示：
          <label for="logintip"></label>
          <select name="logintip" id="select6" style="width:70px;">
            <option value="<?=$row[logintip]?>">
            <?=$row[logintip]?>
            :不变</option>
            <option value="1">1是</option>
            <option value="0">0否</option>
          </select>
          &nbsp;游客登录：
          <select name="loginguest" id="loginguest" style="width:70px;">
            <option value="<?=$row[loginguest]?>">
            <?=$row[loginguest]?>
            :不变</option>
            <option value="1">1是</option>
            <option value="0">0否</option>
          </select>
        &nbsp;第三方登录：
        <label for="loginqq"></label>
        <select name="loginqq" id="loginqq" style="width:70px;">
            <option value="<?=$row[loginqq]?>"><?=$row[loginqq]?>:不变</option>
            <option value="1">1是</option>
            <option value="0">0否</option>
        </select>
          &nbsp; 注册审核：
          <select name="regaudit" id="regaudit" style="width:70px;">
            <option value="<?=$row[regaudit]?>">
              <?=$row[regaudit]?>
              :不变</option>
            <option value="1">1是</option>
            <option value="0">0否</option>
          </select>
          &nbsp; 启用抽奖：
          <select name="lottery" id="lottery" style="width:70px;">
            <option value="<?=$row[lottery]?>"><?=$row[lottery]?>:不变</option>
            <option value="1">1是</option>
            <option value="0">0否</option>
          </select>
&nbsp;</td>
      </tr>
		<tr>
			<td class="tableleft" style="width:100px;">文字服务器：</td>
			<td><input name="tserver" type="text" id="tserver" style="width:400px;" value="<?=$row[tserver]?>"/></td>
		</tr>
		<tr>
			<td class="tableleft">直播模块：</td>
			<td><select name="livetype" id="livetype">
			<option value="<?=$row[livetype]?>"><?=$row[livetype]?>-保持不变</option>
			  <option value="0">0-直播视频</option>
			  <option value="1">1-广告视频</option>
			  <option value="2">2-图片</option>
			</select></td>
		</tr>
		<tr>
			<td class="tableleft">电脑端代码：</td>
			<td>
				<textarea name="livefp0" rows="10" id="livefp0" style="width:100%; height:100px;<?php echo $row[livetype] != '0' ? 'display: none;' : ''; ?>"><?=$row[livefp0]?></textarea>
				<input type="text" name="livefp1" id="livefp1" value="<?=$row['livefp1']?>" style="width:400px;<?php echo $row[livetype] != '1' ? 'display: none;' : ''; ?>">
				<input type="text" name="livefp2" id="livefp2" value="<?=$row['livefp2']?>" style="width:400px;<?php echo $row[livetype] != '2' ? 'display: none;' : ''; ?>">
				<button id="livefp2_up" type="button" class="button button-mini button-success" style="<?php echo $row[livetype] != '2' ? 'display: none;' : ''; ?>"><span id="livefp2_up_bnt"></span></button>
			<br>
			<a href="../../ckplayer/demo.htm" target="_blank">示例</a></td>
		</tr>
		<tr>
			<td class="tableleft">手机端代码：</td>
			<td><textarea name="phonefp" rows="10" id="phonefp" style="width:99%;height:100px;"><?=$row[phonefp]?></textarea></td>
		</tr>
		<tr>
			<td class="tableleft">机器人在线：</td>
			<td><input name="rebots" type="text" id="rebots"  value="<?=$row[rebots]?>"/>在线人数区间，例如：20|50表示20至50人在线</td>
		</tr>
		<tr>
			<td class="tableleft">默认客服：</td>
			<td><input name="defkf" type="text" id="defkf"  value="<?=$row[defkf]?>"/></td>
		</tr>
		<tr>
			<td class="tableleft">礼物折扣率：</td>
			<td><input name="discount" type="text" id="discount"  value="<?=$row[discount]?>"/></td>
		</tr>
		<tr>
			<td class="tableleft">统计代码：</td>
			<td><textarea name="tongji" id="tongji" style="width:400px;"><?=tohtml($row[tongji])?></textarea></td>
		</tr>
		<tr>
			<td class="tableleft">版权信息：</td>
			<td><textarea name="copyright" rows="15" id="copyright" style="width:400px;"><?=tohtml($row[copyright])?></textarea></td>
		</tr>
		<tr>
			<td class="tableleft"></td>
			<td>
				<button type="submit" class="button button-success"> 保存 </button>
				<input type="hidden" name="act" value="config_edit">
				<input type="hidden" name="op" value="<?php echo empty($roomid) ? 'add' : 'edit'; ?>"/>
			</td>
		</tr>
    </table>
  </form>
</div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script>
<script type="text/javascript" src="../../upload/swfupload/swfupload.js"></script>
<script>
    function change_rstate(obj) {
        if(obj.value=='2') {
            $('#pwd_s').show();
        } else {
            $('#pwd_s').hide();
        }
        if(obj.value=='3') {
            $('#limit').show();
        } else {
            $('#limit').hide();
        }
    }
    function swfupload_ok(fileObj,server_data){
        var data = eval("("+server_data+")") ;
		//if(data.msg.info) {
			$("#"+data.msg.info).val(data.msg.url);
		//} else {
			
		//}
    }
    $(function(){
        var swfdef={
            //按钮设置
            file_post_name:"filedata",
            button_width: 30,
            button_height: 18,
            button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
            button_cursor: SWFUpload.CURSOR.HAND,
            button_text: '上传',
            button_text_style: ".upbnt{ color:#00F}",
            button_text_left_padding: 0,
            button_text_top_padding: 0,
            upload_success_handler : swfupload_ok,
            file_dialog_complete_handler:function(){this.startUpload();},
            file_queue_error_handler:function(){alert("选择文件错误");}
        }
        swfdef.flash_url="../../upload/swfupload/swfupload.swf";
        swfdef.button_placeholder_id="logo_up_bnt";
        swfdef.file_types="*.gif;*.jpg;*.png";
        swfdef.upload_url="../../upload/upload.php";
        swfdef.post_params={"info":"logo"}

        swfu = new SWFUpload(swfdef);

        var swfico=swfdef;
        swfico.button_placeholder_id="ico_up_bnt";
        swfico.file_types="*.ico";
        swfico.post_params={"info":"ico"}
        swfuico = new SWFUpload(swfico);

        var swfbg=swfdef;
        swfbg.button_placeholder_id="bg_up_bnt";
        swfbg.file_types="*.gif;*.jpg;*.png";
        swfbg.post_params={"info":"bg"}
        swfubg = new SWFUpload(swfbg);

        var swfewm=swfdef;
        swfewm.button_placeholder_id="ewm_up_bnt";
        swfewm.file_types="*.gif;*.jpg;*.png";
        swfewm.post_params={"info":"ewm"}
        swfuewm = new SWFUpload(swfewm);

        var swflivefp=swfdef;
        swflivefp.button_placeholder_id="livefp2_up_bnt";
        swflivefp.file_types="*.gif;*.jpg;*.png";
        swflivefp.post_params={"info":"livefp2"}
        swfulivefp = new SWFUpload(swflivefp);
		
		$('#livetype').change(function() {
			if($(this).val() == '0') {
				$('#livefp0').show();
				$('#livefp1').hide();
				$('#livefp2').hide();
				$('#livefp2_up').hide();
			} else if($(this).val() == '1') {
				$('#livefp0').hide();
				$('#livefp1').show();
				$('#livefp2').hide();
				$('#livefp2_up').hide();
			} else if($(this).val() == '2') {
				$('#livefp0').hide();
				$('#livefp1').hide();
				$('#livefp2').show();
				$('#livefp2_up').show();
			}
		});
		
    });
</script>
<body>
</html>
