<?php
require_once '../../include/common.inc.php';
require_once '../function.php';
if(stripos(auth_group($_SESSION['login_gid']), 'sys_service')===false)exit("没有权限！");

if($act == "service_edit"){
    $arr['name'] = $name;
    $arr['phone'] = $phone;
    $arr['qq'] = $qq;
    $arr['rid'] = $rid;
    $arr['image'] = $image;
    if($op == 'add') {
        service_edit($op, $arr);
        echo '<script>location.href="service.php";</script>';
    } elseif($op == 'edit') {
        service_edit($op, $arr, $sid);
        echo '<script>location.href="service_edit.php?id='.$sid.'";</script>';
    }
}

if(!empty($id)) {
    $query=$db->query("select * from  {$tablepre}service where id={$id}");
    $row=$db->fetch_row($query);
}

?>
<!DOCTYPE HTML>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
    <!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
    <link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        code {
            padding: 0px 4px;
            color: #d14;
            background-color: #f7f7f9;
            border: 1px solid #e1e1e8;
        }
        input, button {
            vertical-align:middle
        }
    </style>
</head>
<body>
<div class="container">
    <form action="" method="post" enctype="application/x-www-form-urlencoded">
        <table class="table table-bordered table-hover definewidth m10">
            <tr>
                <td class="tableleft" style="width:100px;">名称：</td>
                <td><input name="name" type="text" id="name" style="width:400px;" value="<?=$row['name']?>"/></td>
            </tr>
            <tr>
                <td class="tableleft" style="width:100px;">手机号：</td>
                <td><input name="phone" type="text" id="phone" style="width:400px;" value="<?=$row['phone']?>"/></td>
            </tr>
            <tr>
                <td class="tableleft" style="width:100px;">QQ：</td>
                <td><input name="qq" type="text" id="qq" style="width:400px;" value="<?=$row['qq']?>"/></td>
            </tr>
            <tr>
                <td class="tableleft" style="width:100px;">所属房间：</td>
                <td><input name="rid" type="text" id="rid" style="width:400px;" value="<?=$row['rid']?>"/></td>
            </tr>
			<tr>
				<td class="tableleft">图片：</td>
				<td>
					<input name="image" type="text" id="image" style="width:350px;" value="<?=$row['image']?>"/>
					<button  type="button" class="button button-mini button-success" ><span id="image_up_bnt"></span></button>
				</td>
			</tr>
            <tr>
                <td class="tableleft"></td>
                <td>
                    <button type="submit" class="button button-success"> 保存 </button>
                    <input type="hidden" name="sid" value="<?=$row['id']?>">
                    <input type="hidden" name="act" value="service_edit">
                    <input type="hidden" name="op" value="<?php echo empty($id) ? 'add' : 'edit'; ?>"/>
                </td>
            </tr>
        </table>
    </form>
</div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script>
<script type="text/javascript" src="../../upload/swfupload/swfupload.js"></script>
<script>
    function swfupload_ok(fileObj,server_data){
        var data = eval("("+server_data+")") ;
        $("#"+data.msg.info).val(data.msg.url);
    }
    $(function(){
        var swfdef={
            // 按钮设置
            file_post_name:"filedata",
            button_width: 30,
            button_height: 18,
            button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
            button_cursor: SWFUpload.CURSOR.HAND,
            button_text: '上传',
            button_text_style: ".upbnt{ color:#00F}",
            button_text_left_padding: 2,
            button_text_top_padding: 2,
            upload_success_handler : swfupload_ok,
            file_dialog_complete_handler:function(){this.startUpload();},
            file_queue_error_handler:function(){alert("选择文件错误");}
        }
        swfdef.flash_url = "../../upload/swfupload/swfupload.swf";
        swfdef.button_placeholder_id = "image_up_bnt";
        swfdef.file_types = "*.png;*.jpg;*.gif";
        swfdef.upload_url = "../../upload/upload.php";
        swfdef.post_params = {"info":"image"}
        swfu = new SWFUpload(swfdef);
    });
</script>
</body>
</html>
