<?php
require_once '../../include/common.inc.php';
require_once '../function.php';
if(stripos(auth_group($_SESSION['login_gid']),'sys_reply')===false)exit("没有权限！");
switch($act){
    case "reply_del":
//        reply_del(implode(',',$id));
        reply_del($id);
        break;
}
?>
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
<style type="text/css">
code { padding: 0px 4px; color: #d14; background-color: #f7f7f9; border: 1px solid #e1e1e8; }
</style>
</head>
<body>
<div class="container"  style=" min-width:1000px;">
    <form  class="form-horizontal" action="" method="get"> 
        <ul class="breadcrumb">
            <li class="active">
                <button type="button"  class="button  button-success" id="add_ban_bt" onClick="openreply(0, 'add')">添加</button>&nbsp;&nbsp;
            </li>   
        </ul>
    </form>
    <form action="" method="POST" enctype="application/x-www-form-urlencoded"  class="form-horizontal" onsubmit="return false;">
        <table  class="table table-bordered table-hover definewidth m10">
            <thead>
                <tr style="font-weight:bold" >
                    <td width="30" align="center" bgcolor="#FFFFFF">编号</td>
                    <td width="500" align="center" bgcolor="#FFFFFF">内容</td>
                    <td align="center" bgcolor="#FFFFFF">操作</td>
                </tr>
            </thead>    
            <?php
                $query=$db->query("select * from {$tablepre}quick_reply order by id desc");
                while($row = $db->fetch_row($query)) {
            ?>
            <tr>
                <td><?php echo $row['id']; ?></td>
                <td><?php echo substr($row['content'], 0, 150); ?></td>
                <td>
                    <button class="button button-mini button-success" onClick="openreply('<?php echo $row['id']; ?>', 'edit')"><i class="x-icon x-icon-small icon-trash icon-white"></i>修改</button>
                    <button class="button button-mini button-danger" onclick="if(confirm('确定删除？'))location.href='?act=reply_del&id=<?php echo $row['id']; ?>'" ><i class="x-icon x-icon-small icon-trash icon-white"></i>删除</button>
                </td>
            </tr>
            <?php } ?>
        </table>
    </form> 
</div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script> 
<script type="text/javascript" src="../assets/js/bui.js"></script> 
<script type="text/javascript" src="../assets/js/config.js"></script> 
<script>
    BUI.use('bui/overlay',function(Overlay){
        dialog = new Overlay.Dialog({
            title:'公告板',
            width:700,
            height:300,
            buttons:[],
            bodyContent:''
        });
    });
    function openreply(id, type){
        dialog.set('bodyContent','<iframe src="reply_edit.php?id='+id+'&type='+type+'" scrolling="yes" frameborder="0" height="100%" width="100%"></iframe>');
        dialog.updateContent();
        dialog.show();
    }
</script>
</body>
</html>
