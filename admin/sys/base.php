<?php
require_once '../../include/common.inc.php';
require_once '../function.php';
if(stripos(auth_group($_SESSION['login_gid']),'sys_base')===false)exit("没有权限！");

if($act=="config_edit"){
	$arr[title]=$title;
	$arr[keys]=$keys;
	$arr[dc]=$dc;
	$arr[logo]=$logo;
	$arr[ico]=$ico;
	$arr[bg]=$bg;
	$arr[msgban]=$msgban;
	$arr[state]=$state;
	$arr[pwd]=$pwd;
	$arr[regaudit]=$regaudit;
	$arr[msgaudit]=$msgaudit;
	$arr[msglog]=$msglog;
	$arr[logintip]=$logintip;
	$arr[loginguest]=$loginguest;
	$arr[loginqq]=$loginqq;
	$arr[tongji]=$tongji;
	$arr[copyright]=$copyright;
	$arr[regban]=$regban;
	$arr[msgblock]=$msgblock;
	$arr[ewm]=$ewm;
	config_edit($arr);
}


$query=$db->query("select * from  {$tablepre}config where id=1");
$row=$db->fetch_row($query);
?>
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
<style type="text/css">
code {
	padding: 0px 4px;
	color: #d14;
	background-color: #f7f7f9;
	border: 1px solid #e1e1e8;
}
input, button {
	vertical-align:middle
}
</style>
</head>
<body>
<div class="container">
  <form action="" method="post" enctype="application/x-www-form-urlencoded">
    <table class="table table-bordered table-hover definewidth m10">
      <tr>
        <td class="tableleft" style="width:100px;">网站标题：</td>
        <td><input name="title" type="text" id="title" style="width:400px;" value="<?=$row[title]?>"/></td>
      </tr>
      <tr>
        <td class="tableleft">关 键 字：</td>
        <td><textarea name="keys" rows="2" id="keys" style="width:400px;"><?=$row[keys]?>
</textarea></td>
      </tr>
      <tr>
        <td class="tableleft">站点描述：</td>
        <td><textarea name="dc" id="dc" style="width:400px;"><?=$row[dc]?>
</textarea></td>
      </tr>
      <tr>
        <td class="tableleft">LOGO：</td>
        <td><input name="logo" type="text" id="logo" style="width:400px;" value="<?=$row[logo]?>"/>
          <button  type="button" class="button button-mini button-success"><span id="logo_up_bnt"></span></button></td>
      </tr>
      <tr>
        <td class="tableleft">ICO：</td>
        <td><input name="ico" type="text" id="ico" style="width:400px;" value="<?=$row[ico]?>"/>
          <button  type="button" class="button button-mini button-success" ><span id="ico_up_bnt"></span></button></td>
      </tr>
      <tr>
        <td class="tableleft">背景：</td>
        <td><input name="bg" type="text" id="bg" style="width:400px;" value="<?=$row[bg]?>"/>
          <button  type="button" class="button button-mini button-success" ><span id="bg_up_bnt"></span></button></td>
      </tr>
	        <tr>
        <td class="tableleft">二维码：</td>
        <td><input name="ewm" type="text" id="ewm" style="width:400px;" value="<?=$row[ewm]?>"/>
          <button  type="button" class="button button-mini button-success" ><span id="ewm_up_bnt"></span></button></td>
      </tr>
      <tr>
        <td class="tableleft">注册过滤：</td>
        <td><textarea name="regban" id="regban" style="width:400px;"><?=$row[regban]?>
</textarea></td>
      </tr>
      <tr>
        <td class="tableleft">聊天过滤：</td>
        <td><textarea name="msgban" id="msgban" style="width:400px;"><?=$row[msgban]?>
</textarea></td>
      </tr>
      <tr>
        <td class="tableleft">系统状态：</td>
        <td><select name="state" id="state" style="width:60px;" onChange="if(this.value=='2')$('#pwd_s').show();else $('#pwd_s').hide(); ">
            <option value="<?=$row[state]?>">
            <?=$row[state]?>
            :不变</option>
            <option value="1">1开启</option>
            <option value="0">0关闭</option>
            <option value="2">2加密</option>
          </select>
          <label id="pwd_s" style="display:none"> 密码：
            <input name="pwd" type="text" id="pwd" value="<?=$row[pwd]?>"/>
          </label></td>
      </tr>
      <tr>
        <td class="tableleft">&nbsp;</td>
        <td>消息屏蔽：
          <label for="msgblock"></label>
          <select name="msgblock" id="msgblock" style="width:70px;">
            <option value="<?=$row[msgblock]?>">
            <?=$row[msgblock]?>
            :不变</option>
            <option value="1">1是</option>
            <option value="0">0否</option>
          </select>
          &nbsp;消息记录：
          <label for="msglog"></label>
          <select name="msglog" id="msglog" style="width:70px;">
            <option value="<?=$row[msglog]?>">
            <?=$row[msglog]?>
            :不变</option>
            <option value="1">1是</option>
            <option value="0">0否</option>
          </select>
         &nbsp; 消息审核：
          <label for="msgaudit"></label>
          <select name="msgaudit" id="msgaudit" style="width:70px;">
            <option value="<?=$row[msgaudit]?>">
              <?=$row[msgaudit]?>
              :不变</option>
            <option value="1">1是</option>
            <option value="0">0否</option>
          </select></td>
      </tr>
      <tr>
        <td class="tableleft">&nbsp;</td>
        <td>登录提示：
          <label for="logintip"></label>
          <select name="logintip" id="select6" style="width:70px;">
            <option value="<?=$row[logintip]?>">
            <?=$row[logintip]?>
            :不变</option>
            <option value="1">1是</option>
            <option value="0">0否</option>
          </select>
          &nbsp;游客登录：
          <select name="loginguest" id="loginguest" style="width:70px;">
            <option value="<?=$row[loginguest]?>">
            <?=$row[loginguest]?>
            :不变</option>
            <option value="1">1是</option>
            <option value="0">0否</option>
          </select>
          <!--
&nbsp;第三方登录：
<label for="loginqq"></label>
<select name="loginqq" id="loginqq" style="width:70px;">
<option value="<?=$row[loginqq]?>"><?=$row[loginqq]?>:不变</option>
  <option value="1">1是</option>
  <option value="0">0否</option>
</select>
-->
          &nbsp; 注册审核：
          <select name="regaudit" id="regaudit" style="width:70px;">
            <option value="<?=$row[regaudit]?>">
              <?=$row[regaudit]?>
              :不变</option>
            <option value="1">1是</option>
            <option value="0">0否</option>
          </select>
&nbsp;</td>
      </tr>
      <tr>
        <td class="tableleft">统计代码：</td>
        <td><textarea name="tongji" id="tongji" style="width:400px;"><?=tohtml($row[tongji])?>
</textarea></td>
      </tr>
      <tr>
        <td class="tableleft">版权信息：</td>
        <td><textarea name="copyright" rows="15" id="copyright" style="width:400px;"><?=tohtml($row[copyright])?>
</textarea></td>
      </tr>
      <tr>
        <td class="tableleft"></td>
        <td><button type="submit" class="button button-success"> 保存 </button>
          <input type="hidden" name="act" value="config_edit"></td>
      </tr>
    </table>
  </form>
</div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script>
<script type="text/javascript" src="../../upload/swfupload/swfupload.js"></script>
<script>
  function swfupload_ok(fileObj,server_data){
	 
	 var data=eval("("+server_data+")") ;
	 $("#"+data.msg.info).val(data.msg.url);
  }
  $(function(){


  var swfdef={
	  // 按钮设置
	    file_post_name:"filedata",
		button_width: 30,
		button_height: 18,
		button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
		button_cursor: SWFUpload.CURSOR.HAND,
		button_text: '上传',
		button_text_style: ".upbnt{ color:#00F}",
		button_text_left_padding: 0,
		button_text_top_padding: 0,
		upload_success_handler : swfupload_ok,
		file_dialog_complete_handler:function(){this.startUpload();},
		file_queue_error_handler:function(){alert("选择文件错误");}
	}

	swfdef.flash_url="../../upload/swfupload/swfupload.swf";
	swfdef.button_placeholder_id="logo_up_bnt";
	swfdef.file_types="*.gif;*.jpg;*.png";
	swfdef.upload_url="../../upload/upload.php";
	swfdef.post_params={"info":"logo"}
	swfu = new SWFUpload(swfdef);

	var swfico=swfdef;
	swfico.button_placeholder_id="ico_up_bnt";
	swfico.file_types="*.ico";
	swfico.post_params={"info":"ico"}
	swfuico = new SWFUpload(swfico);

	var swfbg=swfdef;
	swfbg.button_placeholder_id="bg_up_bnt";
	swfbg.file_types="*.gif;*.jpg;*.png";
	swfbg.post_params={"info":"bg"}
	swfubg = new SWFUpload(swfbg);

	var swfewm=swfdef;
	swfewm.button_placeholder_id="ewm_up_bnt";
	swfewm.file_types="*.gif;*.jpg;*.png";
	swfewm.post_params={"info":"ewm"}
	swfuewm = new SWFUpload(swfewm);
	  
});


</script>
<body>
</html>
