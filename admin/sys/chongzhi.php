<?php

	require_once '../../include/common.inc.php';
	require_once '../function.php';
	if(stripos(auth_group($_SESSION['login_gid']),'chongzhi')===false)exit("没有权限！");

	switch($act){
		case "log_del":
			$del_ids = (implode(',',$id));
			$sql = "delete from {$tablepre}alipay where id in($del_ids)";
			$db->query($sql);
			break;
		case "clear_log":
			$sql = "delete from {$tablepre}alipay where type = '0'";
			$db->query($sql);
			break;
	}

	$sql = "select * from {$tablepre}alipay where type = '0'";
	$query = $db->query($sql);

	$count = $db->num_rows($db->query($sql));
	pageft($count, 20, "");

?>
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
<style type="text/css">
code { padding: 0px 4px; color: #d14; background-color: #f7f7f9; border: 1px solid #e1e1e8; }
</style>
</head>
<body>
<div class="container"  style=" min-width:1000px;">
	<form  class="form-horizontal" action="" method="get"> 
		<ul class="breadcrumb">
			<li class="active">
				<button type="button"  class="button  button-danger" id="add_ban_bt" onClick="if(confirm('确定删除？'))$('#log_list').submit()">删除所选</button>
				<button type="button"  class="button  button-danger" id="add_ban_bt" onClick="if(confirm('确定清空日志？'))location.href='?act=clear_log'">清空日志</button>
				&nbsp;&nbsp;
			</li>
		</ul>
	</form>
	<form action="" method="POST" enctype="application/x-www-form-urlencoded"  class="form-horizontal" id="log_list">
		<input type="hidden" name="act" value="log_del"> 
		<table  class="table table-bordered table-hover definewidth m10" style="text-align: center;">
			<thead>
				<tr>
					<td width="19" align="center" bgcolor="#FFFFFF"><input type="checkbox" onClick="$('.ids').attr('checked',this.checked); "></td>
					<th width="45" bgcolor="#FFFFFF">序号</th>
					<th bgcolor="#FFFFFF">用户名</th>
					<th bgcolor="#FFFFFF">充值账户</th>
					<th bgcolor="#FFFFFF">充值金额</th>
					<th bgcolor="#FFFFFF">充值时间</th>
				</tr>
				<?php
					while ($row = $db->fetch_row($query)) {
				?>
					<tr>
						<td bgcolor="#FFFFFF"><input type="checkbox" class="ids" name="id[]" value="<?php echo $row['id']; ?>"></td>
						<td bgcolor="#FFFFFF"><?php echo $row['id']; ?></td>
						<td bgcolor="#FFFFFF"><?php echo get_userinfo($row['uid'], 'nickname'); ?></td>
						<td bgcolor="#FFFFFF"><?php echo $row['buyer_email']; ?></td>
						<td bgcolor="#FFFFFF"><?php echo $row['total_fee']; ?></td>
						<td bgcolor="#FFFFFF"><?php echo date('Y-m-d H:i:s', $row['pay_time']); ?></td>
					</tr>
				<?php } ?>
			</thead>		
		</table>
	</form> 
    <ul class="breadcrumb">
    <li class="active"><?=$pagenav?>
    </li>
  </ul>
</div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script> 
<script type="text/javascript" src="../assets/js/bui.js"></script> 
<script type="text/javascript" src="../assets/js/config.js"></script> 
<script type="text/javascript">
    var Calendar = BUI.Calendar
    var datepicker = new Calendar.DatePicker({
        trigger:'.calendar-time',
        showTime:true,
        autoRender : true
    });
</script>
</body>
</html>
