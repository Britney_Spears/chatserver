<?php
require_once '../../include/common.inc.php';
require_once '../function.php';
if(stripos(auth_group($_SESSION['login_gid']),'sys_sms')===false)exit("没有权限！");

if($act=="sms_edit"){
	$db->query("update {$tablepre}sms set account='{$account}',password='{$password}',provider='{$provider}',remark='{$remark}',enabled='{$enabled}' where id='1'");
	echo '<script>location.href="sms.php";</script>';
}

$query=$db->query("select * from  {$tablepre}sms where id='1'");
$row=$db->fetch_row($query);

?>
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
<style type="text/css">
code {
	padding: 0px 4px;
	color: #d14;
	background-color: #f7f7f9;
	border: 1px solid #e1e1e8;
}
input, button {
	vertical-align:middle
}
.provider span{float: left; width: 190px;}
.remark{display: none;}
</style>
</head>
<body>
<div class="container">
	<form action="" method="post" enctype="application/x-www-form-urlencoded">
    <table class="table table-bordered table-hover definewidth m10">
		<tr>
			<td class="tableleft" style="width:120px;">短信服务商：</td>
			<td class="provider">
				<span><input name="provider" type="radio" value="1" <?php if($row['provider'] == 1) {echo 'checked="checked"';}?>>短信宝(<a href="http://www.smsbao.com/reg?r=10459" target="_blank">http://www.smsbao.com</a>)</span>
				<span><input name="provider" type="radio" value="2" <?php if($row['provider'] == 2) {echo 'checked="checked"';}?>>莫名短信(<a href="http://www.duanxin.cm/" target="_blank">http://pc.duanxin.cm</a>)</span>
				<span><input name="provider" type="radio" value="3" <?php if($row['provider'] == 3) {echo 'checked="checked"';}?>>云信使平台(<a href="http://www.sms.cn" target="_blank">http://www.sms.cn</a>)</span>
			</td>
		</tr>
		<tr>
			<td class="tableleft" style="width:100px;">短信平台账号：</td>
			<td><input name="account" type="text" id="account" style="width:400px;" value="<?=$row['account']?>"/></td>
		</tr>
		<tr>
			<td class="tableleft" style="width:100px;">短信平台密码：</td>
			<td><input name="password" type="text" id="password" style="width:400px;" value="<?=$row['password']?>"/></td>
		</tr>
		<tr class="remark">
			<td class="tableleft" style="width:100px;">通道组编号：</td>
			<td><input name="remark" type="text" id="remark" style="width:400px;" value="<?=$row['remark']?>"/></td>
		</tr>
		<tr>
			<td class="tableleft" style="width:100px;">是否开启短信注册：</td>
			<td class="smsEnabled">
				<span style="width: 40px; display: inline-block;"><input name="enabled" type="radio" value="1" <?php if($row['enabled'] == 1) {echo 'checked="checked"';}?>>是</span>
				<span style="width: 40px; display: inline-block;"><input name="enabled" type="radio" value="0" <?php if($row['enabled'] == 0) {echo 'checked="checked"';}?>>否</span>
			</td>
		</tr>
		<tr>
			<td class="tableleft"></td>
			<td>
				<button type="submit" class="button button-success"> 保存 </button>
				<input type="hidden" name="act" value="sms_edit">
			</td>
		</tr>
    </table>
	</form>
</div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script>
<script>
	$('input[name="provider"]').change(function() {
		if($(this).val() == '2') {
			$('.remark').show();
		} else {
			$('.remark').hide();
		}
	});
</script>
<body>
</html>
