<?php
	require_once '../../include/common.inc.php';
	require_once '../function.php';
	if(stripos(auth_group($_SESSION['login_gid']),'sys_alipay_config')===false)exit("没有权限！");

	if($act == "pay_edit"){
		//$db->query("update {$tablepre}alipay_config set partner='{$partner}',partner_key='{$partner_key}',seller_id='{$seller_id}' where id='1'");
		$db->query("update {$tablepre}setting set item='{$partner}' where idx='partner'");
		$db->query("update {$tablepre}setting set item='{$partner_key}' where idx='partner_key'");
		$db->query("update {$tablepre}setting set item='{$seller_id}' where idx='seller_id'");
		$db->query("update {$tablepre}setting set item='{$APPID}' where idx='APPID'");
		$db->query("update {$tablepre}setting set item='{$MCHID}' where idx='MCHID'");
		$db->query("update {$tablepre}setting set item='{$KEY}' where idx='KEY'");
		echo '<script>location.href="alipay_config.php";</script>';
	}

	//$query=$db->query("select * from  {$tablepre}alipay_config where id='1'");
	//$row=$db->fetch_row($query);

	$setting = array();
	$query = $db->query("select * from {$tablepre}setting;");
	while($row = $db->fetch_row($query)) {
		$setting[$row['idx']] = $row['item'];
	}	

?>
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
<style type="text/css">
code {
	padding: 0px 4px;
	color: #d14;
	background-color: #f7f7f9;
	border: 1px solid #e1e1e8;
}
input, button {
	vertical-align:middle
}
.container{
	padding-top: 20px;
}
.offset0{
	margin-left: 20px;
}
</style>
</head>
<body>
<div class="container">
	<form action="" method="post" enctype="application/x-www-form-urlencoded" class="form-horizontal bui-form bui-form-field-container">
		<!--<div class="control-group">
			<label class="control-label">&nbsp;</label>
			<div class="controls">
				<label class="radio" for=""><input class="bui-form-field-radio bui-form-check-field bui-form-field" aria-disabled="false" name="payType" value="ali" aria-pressed="false" selected="selected" type="radio">支付宝配置</label>&nbsp;&nbsp;&nbsp;
				<label class="radio" for=""><input class="bui-form-field-radio bui-form-check-field bui-form-field" aria-disabled="false" name="payType" value="wx" aria-pressed="false" type="radio">微信支付配置</label>
			</div>
		</div>-->
		<div class="control-group">
			<label class="control-label" style="font-size: 16px; font-weight: bold;">支付宝配置</label>
			<div class="controls">&nbsp;&nbsp;&nbsp;</div>
		</div>
		<div class="control-group">
			<label class="control-label"><s>*</s>合作身份者ID：</label>
			<div class="controls">
				<input name="partner" value="<?=$setting['partner']?>" class="input-large bui-form-field" data-rules="{required : true}" aria-disabled="false" type="text">
			</div>
			<label class="control-label control-label-auto offset0">合作身份者ID，签约账号，以2088开头由16位纯数字组成的字符串，查看地址：https://b.alipay.com/order/pidAndKey.htm</label>
		</div>
		<div class="control-group">
			<label class="control-label"><s>*</s>收款支付宝账号：</label>
			<div class="controls">
				<input name="seller_id" value="<?=$setting['seller_id']?>" class="input-large bui-form-field" data-rules="{required : true}" aria-disabled="false" type="text">
			</div>
			<label class="control-label control-label-auto offset0">收款支付宝账号，以2088开头由16位纯数字组成的字符串，一般情况下收款账号就是签约账号</label>
		</div>
		<div class="control-group">
			<label class="control-label"><s>*</s>安全检验码：</label>
			<div class="controls">
				<input name="partner_key" value="<?=$setting['partner_key']?>" class="input-large bui-form-field" data-rules="{required : true}" aria-disabled="false" type="text">
			</div>
			<label class="control-label control-label-auto offset0">MD5密钥，安全检验码，由数字和字母组成的32位字符串，查看地址：https://b.alipay.com/order/pidAndKey.htm</label>
		</div>
		<div class="control-group">
			<label class="control-label" style="font-size: 16px; font-weight: bold;">微信支付配置</label>
			<div class="controls">&nbsp;&nbsp;&nbsp;</div>
		</div>
		<div class="control-group">
			<label class="control-label"><s>*</s>APPID：</label>
			<div class="controls">
				<input name="APPID" value="<?=$setting['APPID']?>" class="input-large bui-form-field" data-rules="{required : true}" aria-disabled="false" type="text">
			</div>
			<label class="control-label control-label-auto offset0">绑定支付的APPID（必须配置，开户邮件中可查看）</label>
		</div>
		<div class="control-group">
			<label class="control-label"><s>*</s>商户号：</label>
			<div class="controls">
				<input name="MCHID" value="<?=$setting['MCHID']?>" class="input-large bui-form-field" data-rules="{required : true}" aria-disabled="false" type="text">
			</div>
			<label class="control-label control-label-auto offset0">商户号（必须配置，开户邮件中可查看）</label>
		</div>
		<div class="control-group">
			<label class="control-label"><s>*</s>商户支付密钥：</label>
			<div class="controls">
				<input name="KEY" value="<?=$setting['KEY']?>" class="input-large bui-form-field" data-rules="{required : true}" aria-disabled="false" type="text">
			</div>
			<label class="control-label control-label-auto offset0">商户支付密钥，参考开户邮件设置（必须配置，登录商户平台自行设置）</label>
		</div>
		<div class="row actions-bar">       
			<div class="form-actions span13 offset3">
				<input type="hidden" name="act" value="pay_edit" />
				<button type="submit" class="button button-primary">保存</button>
			</div>
		</div>
	</form>
</div>
<body>
</html>
