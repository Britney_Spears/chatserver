<?php
require_once '../../include/common.inc.php';
require_once '../function.php';
if(stripos(auth_group($_SESSION['login_gid']),'apps_lottery')===false)exit("没有权限！");

switch($act){
	case "lottery_del":
		if(is_array($id) && !empty($id)) {
			$ids = implode(',',$id);
		} else {
			$ids = $id;
		}		
		$db->query("delete from {$tablepre}lottery where id in ($ids);");
		header("location:" . $_SERVER['HTTP_REFERER']);
	break;
}
?>
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
<style type="text/css">
code { padding: 0px 4px; color: #d14; background-color: #f7f7f9; border: 1px solid #e1e1e8; }
</style>
<script>
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
function ftime(time){
	return new Date(time*1000).Format("yyyy-MM-dd hh:mm"); ; 
}
</script>
</head>
<body>
<div class="container"  style=" min-width:1150px;">
<form  class="form-horizontal" action="" method="get"> 
  <ul class="breadcrumb">
    <li class="active">
	<button type="button"  class="button  button-danger"  onClick="if(confirm('确定删除？'))$('#hd_list').submit()">删除所选</button>
  </ul>
  </form>
    <form action="" method="POST" enctype="application/x-www-form-urlencoded"  class="form-horizontal" id="hd_list">
	<input type="hidden" name="gid" value="<?=$gid?>">
	<input type="hidden" name="act" value="lottery_del">
  <table  class="table table-bordered table-hover definewidth m10">
    <thead>
      <tr style="font-weight:bold" >
        <td width="19" align="center" bgcolor="#FFFFFF"><input type="checkbox" onClick="$('.ids').attr('checked',this.checked); "></td>
        <td width="40" align="center" bgcolor="#FFFFFF">编号</td>
        <td width="50"  align="center" bgcolor="#FFFFFF">用户ID</td>
        <td width=""  align="center" bgcolor="#FFFFFF">奖品</td>
        <td width="150" align="center" bgcolor="#FFFFFF">抽奖时间</td>
        <td width="150" align="center" bgcolor="#FFFFFF">操作</td>
      </tr>
    </thead>
	<?php

		$sql = "select * from {$tablepre}lottery";
		$count = $db->num_rows($db->query($sql));
		pageft($count, 30, "");
		$sql .= " order by id desc";
		$sql .= " limit $firstcount,$displaypg";
		$query = $db->query($sql);
		echo for_each($query,'<tr>
								<td align="center" bgcolor="#FFFFFF"><input type="checkbox" class="ids" name="id[]" value="{id}"></td>
								<td>{id}</td>
								<td>{uid}</td>
								<td>{prize}</td>
								<td><script>document.write(ftime("{ctime}"));</script></td>
								<td align="center" bgcolor="#FFFFFF">
									<button type="button" class="button button-mini button-danger" onclick="if(confirm(\'确定删除发言内容？\'))location.href=\'?act=lottery_del&id={id}\'">
									<i class="x-icon x-icon-small icon-trash icon-white"></i>删除</button>
								</td>
							</tr>');
	
	?>

		</table>
	</form> 
	<ul class="breadcrumb">
		<li class="active"><?=$pagenav?>
		</li>
	</ul>
</div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script>
</body>
</html>
