<?php
	require_once '../../include/common.inc.php';
	require_once '../function.php';
	if(stripos(auth_group($_SESSION['login_gid']),'apps_teacher')===false)exit("没有权限！");
	switch($act) {
		case "teacher_edit":
			$query = $db->query("select name from {$tablepre}apps_teacher where uid='$uid'");
			if($db->num_rows($query) < 1) {
				$db->query("insert into {$tablepre}apps_teacher set uid='$uid',name='$name',image='$image',info='$info'");
			} else {
				$db->query("update {$tablepre}apps_teacher set name='$name',image='$image',info='$info' where uid='$uid'");
			}
			echo '<script>parent.location.reload();</script>';exit();
		break;
	}
	$query=$db->query("select * from {$tablepre}apps_teacher where uid='$uid'");
	if($db->num_rows($query)>0) {
		$row=$db->fetch_row($query);
	}
?>
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
<style type="text/css">
code { padding: 0px 4px; color: #d14; background-color: #f7f7f9; border: 1px solid #e1e1e8; }
input,select{vertical-align:middle;}
.liw { width:160px; height:25px; line-height:25px;}
</style>
</head>
<body>
<div class="container" style="margin-bottom:50px;">
<form action="?id=<?=$id?>" method="post" enctype="application/x-www-form-urlencoded">
	<ul class="breadcrumb">
		<table class="table table-bordered table-hover definewidth m10">
			<tr>
				<td width="60" class="tableleft">讲师名：</td>
				<td><input name="name" type="text" id="name" value="<?php echo $row['name'] ?>"/></td>
			</tr>
			<tr>
				<td class="tableleft">讲师照片：</td>
				<td><input name="image" type="text" id="image" value="<?=$row[image]?>"  class="input-large"/><br><br>
				<button  type="button" class="button button-mini button-success" ><span id="image_bt"></span></button></td>
			</tr>
			<tr>
				<td width="60" class="tableleft" style="width:70px;">简介：</td>
				<td><textarea name="info" id="info" style="width:100%;"><?=$row['info']?></textarea></td>
			</tr>
		</table>
	</ul>
	<div style="position:fixed; bottom:0; background: #FFF; width:100%; padding-top:5px;">
		<button type="submit"  class="button button-success">确定</button>
		<button type="button"  class="button" onclick="window.parent.dialog.close()">关闭</button>
		<input type="hidden" name="act" value="teacher_edit">
		<input type="hidden" name="uid" value="<?=$uid?>">
	</div>
</form>
</div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script> 
<script type="text/javascript" src="../assets/js/bui.js"></script> 
<script type="text/javascript" src="../assets/js/config.js"></script>
<script type="text/javascript" src="../../upload/swfupload/swfupload.js"></script> 
<script>
	function swfupload_ok(fileObj, server_data) {
		var data = eval("("+server_data+")");
		$("#"+data.msg.info).val(data.msg.url);
	}
  
	$(function(){
		var swfdef = {
			// 按钮设置
			file_post_name: "filedata",
			button_width: 30,
			button_height: 18,
			button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
			button_cursor: SWFUpload.CURSOR.HAND,
			button_text: '上传',
			button_text_style: ".upbnt{ color:#00F}",
			button_text_left_padding: 0,
			button_text_top_padding: 0,
			upload_success_handler : swfupload_ok,
			file_dialog_complete_handler: function(){this.startUpload();},
			file_queue_error_handler: function(){alert("选择文件错误");}
		}
		
		swfdef.flash_url = "../../upload/swfupload/swfupload.swf";
		swfdef.button_placeholder_id = "image_bt";
		swfdef.file_types = "*.jpg;*.gif;*.png";
		swfdef.upload_url = "../../upload/upload.php";
		swfdef.post_params = {"info": "image"}		
		swfu = new SWFUpload(swfdef);
		
	});
</script>
</body>
</html>
