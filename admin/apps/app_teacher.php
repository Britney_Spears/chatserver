<?php
	require_once '../../include/common.inc.php';
	require_once '../function.php';
	if(stripos(auth_group($_SESSION['login_gid']),'apps_teacher')===false)exit("没有权限！");
?>
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
<style type="text/css">
code { padding: 0px 4px; color: #d14; background-color: #f7f7f9; border: 1px solid #e1e1e8; }
</style>
</head>
<body>
<div class="container"  style=" min-width:1150px;">
    <form action="" method="POST" enctype="application/x-www-form-urlencoded"  class="form-horizontal" id="hd_list">
		<table  class="table table-bordered table-hover definewidth m10">
			<thead>
				<tr style="font-weight:bold" >
					<td width="50" align="center" bgcolor="#FFFFFF">用户ID</td>
					<td width="120" align="center" bgcolor="#FFFFFF">讲师名</td>
					<td width=""  align="center" bgcolor="#FFFFFF">简介</td>
					<td width="150" align="center" bgcolor="#FFFFFF">操作</td>
				</tr>
			</thead>
			<?php

				$sql = "select *,m.uid uid from {$tablepre}members m left join {$tablepre}apps_teacher mt on m.uid=mt.uid where m.gid='4'";
				$count = $db->num_rows($db->query($sql));
				pageft($count, 30, "");
				$sql .= " order by m.uid asc";
				$sql .= " limit $firstcount,$displaypg";
				$query = $db->query($sql);
				echo for_each($query,'<tr>
										<td>{uid}</td>
										<td>{name}</td>
										<td>{info}</td>
										<td align="center" bgcolor="#FFFFFF">
											<button type="button" class="button button-mini button-info" onClick="openTeacher({uid})" >
											<i class="x-icon x-icon-small icon-wrench icon-white"></i>修改</button>
										</td>
									</tr>');
			
			?>
		</table>
	</form> 
	<ul class="breadcrumb">
		<li class="active"><?=$pagenav?></li>
	</ul>
</div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script> 
<script type="text/javascript" src="../assets/js/bui.js"></script> 
<script type="text/javascript" src="../assets/js/config.js"></script> 
<script type="text/javascript" src="../../upload/swfupload/swfupload.js"></script> 
<script>
	BUI.use('bui/overlay',function(Overlay){
		dialog = new Overlay.Dialog({
			title:'讲师修改',
			width:700,
			height:520,
			buttons:[],
			bodyContent:''
		});
	});
	function openTeacher(uid){
		dialog.set('bodyContent', '<iframe src="app_teacher_edit.php?uid='+uid+'" scrolling="yes" frameborder="0" height="100%" width="100%"></iframe>');
		dialog.updateContent();
		dialog.show();
	}
</script>
</body>
</html>
