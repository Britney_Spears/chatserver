<?php
require_once '../../include/common.inc.php';
require_once '../function.php';
if(stripos(auth_group($_SESSION['login_gid']),'apps_rotary')===false)exit("没有权限！");
switch($act){
	case "rotary_edit":
		$sql = "update {$tablepre}rotary set rotary='{$rotary}' where id='{$id}'";
		$db->query($sql);
	break;
}
?>
<!DOCTYPE HTML>
<html>
   <head>
   <title></title>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
   <link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
   <link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
   <!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
   <link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
   <style type="text/css">
code { padding: 0px 4px; color: #d14; background-color: #f7f7f9; border: 1px solid #e1e1e8; }
</style>
   </head>
   <body>
<div class="container" style=" min-width:700px;">
     <table  class="table table-bordered table-hover definewidth m10" >
    <thead>
         <tr style="font-weight:bold" >
        <td width="50" align="center" bgcolor="#FFFFFF">编号</td>
        <td width="120" align="center" bgcolor="#FFFFFF">分组图标</td>
        <td width="230" align="center" bgcolor="#FFFFFF">操作</td>
      </tr>
       </thead>
    <form method="post" enctype="application/x-www-form-urlencoded">
		<tr id="act_rotary" style="display:none">
			<td align="center" valign="middle" bgcolor="#FFFFFF"><span class="label label-info" id="actstate">Info</span></td>
			<td align="center" valign="middle" bgcolor="#FFFFFF"><button class="button button-mini button-danger"  id="rotary_up_bnt">上传</button>
				<input type="hidden" id="rotary" name="rotary">
				<input type="hidden" id="act" name="act">
				<input type="hidden" id="id" name="id">
				<img src="" style="border:0px; height:20px; float:left" id="rotary_src"/></td>
			<td align="center" valign="middle" bgcolor="#FFFFFF"><button class="button   button-success" id="act_group_sub" type="submit"><i class="x-icon icon-ok icon-white"></i> 确定</button></td>
		</tr>
	</form>
<?php
$query=$db->query("select * from {$tablepre}rotary order by id desc");
while($row=$db->fetch_row($query)){
?>      
    <tr>
		<td bgcolor="#FFFFFF" align="center"><?=$row[id]?></td>
		<td bgcolor="#FFFFFF" align="center"><img src="<?=$row[rotary]?>" style="border:0px; height:200px; float:left" id="rotary_src"/></td>
		<td bgcolor="#FFFFFF" align="center">
        <button class="button button-mini button-info"  onClick="group_edit_bt('<?=$row[id]?>','<?=$row[rotary]?>')"><i class="x-icon x-icon-small icon-wrench icon-white"></i>修改</button>
    </tr>
<?php }?>       
  </table>
     <div class="row">
    
  </div>
   </div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script> 
<script type="text/javascript" src="../assets/js/bui.js"></script> 
<script type="text/javascript" src="../assets/js/config.js"></script> 
<script type="text/javascript" src="../../upload/swfupload/swfupload.js"></script> 
<script>
	function group_edit_bt(id,rotary){
  		$("#act").val("rotary_edit");
		$("#act_rotary").show();
		$("#id").val(id);
		$("#rotary_src").attr("src",rotary);
		$("#actstate").html("修改");
	}
	function swfupload_ok(fileObj,server_data){
		var data=eval("("+server_data+")") ;
		$('#rotary').val(data.msg.url);
		$('#rotary_src').attr("src",data.msg.url);
		//alert(data.msg.url);
	}
	$(function(){
		var swfdef={
		  // 按钮设置
			file_post_name:"filedata",
			button_width: 35,
			button_height: 18,
			button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
			button_cursor: SWFUpload.CURSOR.HAND,
			button_text: '<span class="upbnt">上传</span>',
			button_text_style: ".upbnt{ color:#00F}",
			button_text_left_padding: 0,
			button_text_top_padding: 0,
			upload_success_handler : swfupload_ok,
			file_dialog_complete_handler:function(){this.startUpload();},
			file_queue_error_handler:function(){alert("选择文件错误");}
		}
		swfdef.flash_url="../../upload/swfupload/swfupload.swf";
		swfdef.button_placeholder_id="rotary_up_bnt";
		swfdef.file_types="*.gif;*.jpg;*.png";
		swfdef.upload_url="../../upload/upload.php";

		swfu = new SWFUpload(swfdef);	  
	});
</script>
</body>
</html>
