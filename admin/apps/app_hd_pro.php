<?php
	require_once '../../include/common.inc.php';
	require_once '../function.php';
	if(stripos(auth_group($_SESSION['login_gid']),'apps_hd_pro')===false)exit("没有权限！");
	switch($act){
		case "pro_add":
			$db->query("insert into {$tablepre}apps_hd_pro set product='{$product}'");
			header('location: app_hd_pro.php');
		break;
		case "pro_del":
			$db->query("delete from {$tablepre}apps_hd_pro where id='{$id}'");
			header('location: app_hd_pro.php');
		break;
		case "pro_edit":
			$db->query("update {$tablepre}apps_hd_pro set product='{$product}' where id='{$id}'");
			header('location: app_hd_pro.php');
		break;
	}
?>
<!DOCTYPE HTML>
<html>
<head>
	<title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
	<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
	<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
	<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
	<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
	code { padding: 0px 4px; color: #d14; background-color: #f7f7f9; border: 1px solid #e1e1e8; }
	</style>
</head>
<body>
	<div class="container" style=" min-width:700px;">
		<ul class="breadcrumb">
			<li class="active">
				<button type="submit"  class="button button-success" id="add_pro_bt"><i class="icon icon-plus icon-white"></i> 添加</button>&nbsp;&nbsp;
			</li>
		</ul>
		<table  class="table table-bordered table-hover definewidth m10" >
		<thead>
			<tr style="font-weight:bold" >
				<td width="50" align="center" bgcolor="#FFFFFF">编号</td>
				<td width="150" align="center" bgcolor="#FFFFFF">商品名</td>
				<td width="230" align="center" bgcolor="#FFFFFF">操作</td>
			</tr>
		</thead>
		<form method="post" enctype="application/x-www-form-urlencoded">
			<tr id="act_pro" style="display:none">
				<td align="center" valign="middle" bgcolor="#FFFFFF"><span class="label label-info" id="actstate">Info</span></td>
				<td align="center" valign="middle" bgcolor="#FFFFFF"><input name="product" type="text" id="product"/></td>
				<td align="center" valign="middle" bgcolor="#FFFFFF">
					<input type="hidden" id="act" name="act"/>
					<input type="hidden" id="id" name="id">
					<button class="button   button-success" id="act_pro_sub" type="submit"><i class="x-icon icon-ok icon-white"></i> 确定</button>
				 </td>
			</tr>
		</form>
		<?php
			$query = $db->query("select * from {$tablepre}apps_hd_pro order by id asc");
			while($row=$db->fetch_row($query)) {
		?>      
			<tr>
				<td bgcolor="#FFFFFF" align="center"><?=$row[id]?></td>
				<td bgcolor="#FFFFFF" align="center"><?=$row[product]?>&nbsp; </td>
				<td bgcolor="#FFFFFF" align="center">
					<button class="button button-mini button-info"  onClick="pro_edit_bt('<?=$row['id']?>', '<?=$row['product']?>')"><i class="x-icon x-icon-small icon-wrench icon-white"></i>修改</button>
					<button class="button button-mini button-danger" onclick="if(confirm('是否继续？'))location.href='?act=pro_del&id=<?=$row[id]?>'"><i class="x-icon x-icon-small icon-trash icon-white"></i>删除</button>
				</td>
			</tr>
		<?php } ?>       
		</table>
		<div class="row"></div>
	</div>
	<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script> 
	<script type="text/javascript" src="../assets/js/bui.js"></script> 
	<script type="text/javascript" src="../assets/js/bui.js"></script> 
	<script type="text/javascript" src="../assets/js/config.js"></script> 
	<script>
		function pro_edit_bt(id, product){
			$("#act_pro").show();
			$("#act").val("pro_edit");
			$("#id").val(id);
			$("#product").val(product);
			$("#actstate").html("修改");
		}
		$(function(){
			$("#add_pro_bt").on("click",function(){
				$("#act_pro").toggle();
				$("#act").val("pro_add");
				$("#product").val("");
				$("#actstate").html("添加");
			});
		});
	</script>
</body>
</html>
