<?php
require_once '../../include/common.inc.php';
require_once '../function.php';
if(stripos(auth_group($_SESSION['login_gid']),'apps_vote')===false)
	exit("没有权限！");

if($act=="edit"){
	$db->query("update {$tablepre}vote set enabled='{$enabled}',field1='{$field1}',field2='{$field2}',field3='{$field3}' where id = '{$id}'");
	header('location: app_vote.php');
}

$query=$db->query("select * from  {$tablepre}vote where id=1");
$row=$db->fetch_row($query);

?>
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
<style type="text/css">
code {
	padding: 0px 4px;
	color: #d14;
	background-color: #f7f7f9;
	border: 1px solid #e1e1e8;
}
input, button {
	vertical-align:middle
}
</style>
</head>
<body>
<div class="container">
  <form action="" method="post" enctype="application/x-www-form-urlencoded">
	<input type="hidden" name="act" value="edit">
	<input type="hidden" name="id" value="1">
    <table class="table table-bordered table-hover definewidth m10">
		<tr>
			<td class="tableleft" style="width:100px;">开启投票：</td>
			<td>
				<span style="width:80px; display: block; float: left;"><input name="enabled" type="radio" value="1" <?php echo $row['enabled'] == '1' ? 'checked="checked"' : ''; ?>/>是</span>
				<span style="width:80px; display: block; float: left;"><input name="enabled" type="radio" value="0" <?php echo $row['enabled'] == '0' ? 'checked="checked"' : ''; ?>/>否</span>
			</td>
		</tr>
		<tr>
			<td class="tableleft" style="width:100px;">投票种类一：</td>
			<td><input name="field1" type="text" id="field1" style="width:200px;" value="<?=$row[field1]?>"/></td>
		</tr>
		<tr>
			<td class="tableleft" style="width:100px;">投票种类二：</td>
			<td><input name="field2" type="text" id="field2" style="width:200px;" value="<?=$row[field2]?>"/></td>
		</tr>
		<tr>
			<td class="tableleft" style="width:100px;">投票种类三：</td>
			<td><input name="field3" type="text" id="field3" style="width:200px;" value="<?=$row[field3]?>"/></td>
		</tr>
		<tr>
			<td colspan="2"><button type="submit"  class="button button-success">确定</button></td>
		</tr>
    </table>
  </form>
</div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script>
<body>
</html>
