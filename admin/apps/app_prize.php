<?php
require_once '../../include/common.inc.php';
require_once '../function.php';
if(stripos(auth_group($_SESSION['login_gid']),'apps_prize')===false)exit("没有权限！");
switch($act){
	case "prize_add":
		$sql = "insert into {$tablepre}prize set num='{$num}',prize='{$prize}',prob='{$prob}',state='{$state}'";
		$db->query($sql);
	break;
	case "prize_del":
		$sql = "delete from {$tablepre}prize where id='{$id}'";
		$db->query($sql);
	break;
	case "prize_edit":
		$sql = "update {$tablepre}prize set num='{$num}',prize='{$prize}',prob='{$prob}',state='{$state}' where id='{$id}'";
		$db->query($sql);
	break;
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
	<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
	<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
	<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
	<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		code { padding: 0px 4px; color: #d14; background-color: #f7f7f9; border: 1px solid #e1e1e8; }
	</style>
</head>
<body>
<div class="container" style=" min-width:700px;">
	<ul class="breadcrumb">
		<li class="active">
			 <button type="submit"  class="button button-success" id="add_prize_bt"><i class="icon icon-plus icon-white"></i> 添加</button>
			 &nbsp;&nbsp;</li>
	</ul>
	<table  class="table table-bordered table-hover definewidth m10" >
		<thead>
			 <tr style="font-weight:bold" >
			<td width="50" align="center" bgcolor="#FFFFFF">编号</td>
			<td width="150" align="center" bgcolor="#FFFFFF">序号</td>
			<td align="center" bgcolor="#FFFFFF">奖品</td>
			<td width="60" align="center" bgcolor="#FFFFFF">中奖概率</td>
			<td width="150" align="center" bgcolor="#FFFFFF">是奖品</td>
			<td width="230" align="center" bgcolor="#FFFFFF">操作</td>
		  </tr>
		</thead>
		<form method="post" enctype="application/x-www-form-urlencoded">
			<tr id="act_prize" style="display:none">
				<td align="center" valign="middle" bgcolor="#FFFFFF"><span class="label label-info" id="actstate">Info</span></td>
				<td align="center" valign="middle" bgcolor="#FFFFFF"><input name="num" type="text" id="num"/></td>
				<td align="center" valign="middle" bgcolor="#FFFFFF"><input name="prize" type="text" id="prize" style="width:400px;"/></td>
				<td align="center" valign="middle" bgcolor="#FFFFFF"><input name="prob" type="text" id="prob" style="width:40px;"/></td>
				<td align="center" valign="middle" bgcolor="#FFFFFF"><input name="state" type="text" id="state" style="width:40px;"/></td>
					<input type="hidden" id="act" name="act">
					<input type="hidden" id="id" name="id">
					<td align="center" valign="middle" bgcolor="#FFFFFF"><button class="button button-success" id="act_prize_sub" type="submit"><i class="x-icon icon-ok icon-white"></i> 确定</button>
				</td>
			</tr>
		</form>
		<?php
		$query=$db->query("select * from {$tablepre}prize order by id asc");
		while($row=$db->fetch_row($query)){
		?>      
			<tr>
				 <td bgcolor="#FFFFFF" align="center"><?=$row[id]?></td>
				 <td bgcolor="#FFFFFF" align="center"><?=$row[num]?>&nbsp; </td>
				 <td align="center" bgcolor="#FFFFFF"><?=$row[prize]?>&nbsp; </td>
				 <td align="center" bgcolor="#FFFFFF"><?=$row[prob]?>&nbsp;</td>
				 <td align="center" bgcolor="#FFFFFF"><?php echo $row[state] == '1' ? '是' : '不是'; ?>&nbsp;</td>
				 <td bgcolor="#FFFFFF" align="center">
					<button class="button button-mini button-info"  onClick="prize_edit_bt('<?=$row[id]?>','<?=$row[num]?>','<?=$row[prize]?>','<?=$row[prob]?>','<?=$row[state]?>')">
						<i class="x-icon x-icon-small icon-wrench icon-white"></i>修改
					</button>
					<button class="button button-mini button-danger" onclick="if(confirm('是否继续？'))location.href='?act=prize_del&id=<?=$row[id]?>'">
						<i class="x-icon x-icon-small icon-trash icon-white"></i>删除
					</button>
				</td>
			</tr>
		<?php }?>       
	</table>
	<div class="row"></div>
</div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script> 
<script type="text/javascript" src="../assets/js/bui.js"></script> 
<script type="text/javascript" src="../assets/js/config.js"></script> 
<script type="text/javascript" src="../../upload/swfupload/swfupload.js"></script> 
<script>
	BUI.use('bui/overlay',function(Overlay){
		dialog = new Overlay.Dialog({
			title:'用户组权限编辑',
			width:800,
			height:600,
			buttons:[],
			bodyContent:''
		});
	});
	function prize_edit_bt(id,num,prize,prob,state){
		$("#act").val("prize_edit");
		$("#act_prize").show();
		$("#prize").val(prize);
		$("#num").val(num);
		$("#prob").val(prob);
		$("#state").val(state);
		$("#id").val(id);
		$("#actstate").html("修改");
	}
	$(function(){
		$("#add_prize_bt").on("click",function(){
			$("#act_prize").toggle();
			$("#act").val("prize_add");
			$("#num").val("");
			$("#prize").val("");
			$("#prob").val("");
			$("#state").val("");
			$("#actstate").html("添加");
		});	  
	});
</script>
</body>
</html>
