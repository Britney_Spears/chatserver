<?php
require_once '../../include/common.inc.php';
require_once '../function.php';
if(stripos(auth_group($_SESSION['login_gid']),'users_invite')===false)exit("没有权限！");
?>
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
<style type="text/css">
code { padding: 0px 4px; color: #d14; background-color: #f7f7f9; border: 1px solid #e1e1e8; }
</style>
<script>
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
function ftime(time){
	return new Date(time*1000).Format("yyyy-MM-dd hh:mm"); ; 
}
function ftime2(time){
	if(time>(60*60*24)) return parseInt(time/(60*60*24))+"天";
	else if(time>(60*60))return parseInt(time/(60*60))+"小时";
	else if(time>60)return parseInt(time/60)+"分钟";
	else return parseInt(time)+"秒";
}
var tuser_arr;
function sgid(tuser){
	if(typeof(tuser_arr) == 'undefined') {
		tuser_arr = new Array();
		<?php
			$query=$db->query("select tuser,count(*) as count from {$tablepre}members where state = 1 and tuser != '' group by tuser;");
			while($row=$db->fetch_row($query)){
				echo "tuser_arr['{$row[tuser]}']='$row[count]';";
			}
		?>
	}
	return tuser_arr[tuser] == undefined ? 0 : '<a href="users.php?fuser='+tuser+'"  aria-disabled="false" data-id="0" aria-pressed="false">'+tuser_arr[tuser]+"</a>";
}
</script>
</head>
<body>
<div class="container"  style=" min-width:1000px;">
    <form action="" method="POST" enctype="application/x-www-form-urlencoded"  class="form-horizontal" id="hd_list">
	<input type="hidden" name="gid" value="<?=$gid?>">
	<input type="hidden" name="act" value="user_del">
  <table  class="table table-bordered table-hover definewidth m10">
    <thead>
      <tr style="font-weight:bold" >
        <td width="40" align="center" bgcolor="#FFFFFF">编号</td>
        <td width="19" align="center" bgcolor="#FFFFFF"><input type="checkbox" onClick="$('.ids').attr('checked',this.checked); "></td>
        <td width="80" align="center" bgcolor="#FFFFFF">客服名</td>
        <td width="100" align="center" bgcolor="#FFFFFF">昵称</td>
        <td width="80" align="center" bgcolor="#FFFFFF">手机</td>
        <td width="80" align="center" bgcolor="#FFFFFF">QQ</td>
        <td width="60" align="center" bgcolor="#FFFFFF">推广人数</td>
        <td width="50" align="center" bgcolor="#FFFFFF">在线</td>
        <td width="100" align="center" bgcolor="#FFFFFF">注册时间</td>
        <td width="100" align="center" bgcolor="#FFFFFF">最近登录</td>
        <td align="center" bgcolor="#FFFFFF">推广链接</td>
      </tr>
    </thead>
<?php
$sql="select m.*,ms.*  from {$tablepre}members m,{$tablepre}memberfields ms  where m.uid=ms.uid and m.uid!=0";
if($skey!=""){
	$sql.=" and (m.username like '%$skey%' or m.fuser like '%$skey%' or m.tuser like '%$skey%' or ms.nickname like 	'%$skey%')";
}
//$sql.=" and m.gid='3'";
$sql.=" and m.gid!='0'";
if($fuser!=""){
	$sql.=" and(m.fuser='$fuser' or m.tuser='$fuser')";
}
if($_SESSION['login_gid']=='3'){
	$sql.=" and m.fuser='".$_SESSION['login_user']."'";
}
$sql.=" order by m.uid desc";
if(!$auth_rule_userdel)$display_delbutton='style="display:none"';
echo userlist(20,$sql,'
    <tr>
      <td bgcolor="#FFFFFF" align="center">{uid}</td>
      <td align="center" bgcolor="#FFFFFF"><input type="checkbox" class="ids" name="id[]" value="{uid}"></td>
      <td align="center" bgcolor="#FFFFFF">{username}</td>
      <td align="center" bgcolor="#FFFFFF">{nickname}</td>
      <td align="center" bgcolor="#FFFFFF">{phone}&nbsp;</td>
      <td align="center" bgcolor="#FFFFFF">{realname}&nbsp;</td>
      <td align="center" bgcolor="#FFFFFF" style="text-align: center;"><script>document.write(sgid("{username}")); </script>&nbsp;</td>
      <td align="center" bgcolor="#FFFFFF"><script>document.write(ftime2({onlinetime})); </script></td>
      <td align="center" bgcolor="#FFFFFF"><script>document.write(ftime({regdate})); </script></td>
      <td align="center" bgcolor="#FFFFFF" title="登录IP:{regip}" onClick="alert(\'登录IP:{regip}\')"><script>document.write(ftime({lastactivity})); </script></td>
      <td align="center" bgcolor="#FFFFFF">
            http://'.$_SERVER['HTTP_HOST'].'/room/index.php?tg={username}
      </td>
    </tr>
')?>


  </table>
    </form> 
    <ul class="breadcrumb">
    <li class="active"><?=$pagenav?>
    </li>
  </ul>
</div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script> 
<script type="text/javascript" src="../assets/js/bui.js"></script> 
<script type="text/javascript" src="../assets/js/config.js"></script> 
<script>
BUI.use('bui/overlay',function(Overlay){
            dialog = new Overlay.Dialog({
            title:'用户设置',
            width:630,
            height:600,
            buttons:[],
            bodyContent:''
          });
});
</script>
</body>
</html>
