<?php
require_once '../../include/common.inc.php';
require_once '../function.php';
if(stripos(auth_group($_SESSION['login_gid']),'robots_admin')===false)exit("没有权限！");

switch($act){
	case "robots_del":
		if(is_array($id)) {
			robot_del(implode(',',$id));
		} else {
			robot_del($id);
		}
		header("location:" . $_SERVER['HTTP_REFERER']);
	break;
}
?>
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
<style type="text/css">
code { padding: 0px 4px; color: #d14; background-color: #f7f7f9; border: 1px solid #e1e1e8; }
</style>
<script>
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
function ftime(time){
	return new Date(time*1000).Format("yyyy-MM-dd hh:mm"); ; 
}
function ftime2(time){
	if(time>(60*60*24)) return parseInt(time/(60*60*24))+"天";
	else if(time>(60*60))return parseInt(time/(60*60))+"小时";
	else if(time>60)return parseInt(time/60)+"分钟";
	else return parseInt(time)+"秒";
}
function sgid(id){
	var arr=new Array();
	if(isNaN(id)) return '';
	<?php
	$query=$db->query("select * from {$tablepre}auth_group order by id desc");
	while($row=$db->fetch_row($query)){
		echo "arr['{$row[id]}']='$row[title]';";
	}
	?>
	return arr[id];
}
</script>
</head>
<body>
<div class="container"  style=" min-width:1150px;">
<form  class="form-horizontal" action="" method="get"> 
  <ul class="breadcrumb">
    <li class="active">
	<button type="button"  class="button  button-danger"  onClick="if(confirm('确定删除？'))$('#hd_list').submit()">删除所选</button>
	<?php
		if(!isset($fuser) && !isset($gid)) {
	?>
	<button type="button"  class="button  button-success"  onClick="addRobot()">添加</button>
	<?php } ?>
  </ul>
  </form>
    <form action="" method="POST" enctype="application/x-www-form-urlencoded"  class="form-horizontal" id="hd_list">
	<input type="hidden" name="gid" value="<?=$gid?>">
	<input type="hidden" name="act" value="robots_del">
  <table  class="table table-bordered table-hover definewidth m10">
    <thead>
      <tr style="font-weight:bold" >
        <td width="40" align="center" bgcolor="#FFFFFF">编号</td>
        <td width="19" align="center" bgcolor="#FFFFFF"><input type="checkbox" onClick="$('.ids').attr('checked',this.checked); "></td>
        <td width="100"  align="center" bgcolor="#FFFFFF">昵称</td>
        <td width="70" align="center" bgcolor="#FFFFFF">用户组</td>
        <td width="60" align="center" bgcolor="#FFFFFF">推广人</td>
        <td width="60" align="center" bgcolor="#FFFFFF">在线星期</td>
        <td width="100" align="center" bgcolor="#FFFFFF">上线时间</td>
        <td width="100" align="center" bgcolor="#FFFFFF">下线时间</td>
        <td align="center" bgcolor="#FFFFFF">操作</td>
      </tr>
    </thead>
<?php
$sql="select *  from {$tablepre}robots order by uid desc";
echo robotslist(20, $sql, '
    <tr>
      <td bgcolor="#FFFFFF" align="center">{uid}</td>
	  <td align="center" bgcolor="#FFFFFF"><input type="checkbox" class="ids" name="id[]" value="{uid}"></td>
      <td align="center" bgcolor="#FFFFFF">{nickname}</td>
      <td align="center" bgcolor="#FFFFFF"><script>document.write(sgid({gid})); </script>&nbsp;</td>      
      <td align="center" bgcolor="#FFFFFF">{tuser}&nbsp;</td>
      <td align="center" bgcolor="#FFFFFF">{week}&nbsp;</td>
      <td align="center" bgcolor="#FFFFFF">{up_hour}:{up_minute}:{up_second}</td>
      <td align="center" bgcolor="#FFFFFF">{down_hour}:{down_minute}:{down_second}</td>
      <td align="center" bgcolor="#FFFFFF">
      <button type="button" class="button button-mini button-info" onClick="openRobot({uid},0)" ><i class="x-icon x-icon-small icon-wrench icon-white"></i>设置</button>
      <button type="button" class="button button-mini button-danger" onclick="if(confirm(\'确定删除用户？\'))location.href=\'?act=robots_del&id={uid}\'"><i class="x-icon x-icon-small icon-trash icon-white"></i>删除</button></td>
    </tr>
')?>

<!--<td align="center" bgcolor="#FFFFFF">{fuser}&nbsp;</td>-->
		</table>
	</form> 
	<ul class="breadcrumb">
		<li class="active"><?=$pagenav?>
		</li>
	</ul>
</div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script> 
<script type="text/javascript" src="../assets/js/bui.js"></script> 
<script type="text/javascript" src="../assets/js/config.js"></script> 
<script type="text/javascript" src="../../upload/swfupload/swfupload.js"></script> 
<script>
	BUI.use('bui/overlay',function(Overlay){
				dialog = new Overlay.Dialog({
				title:'用户设置',
				width:630,
				height:600,
				buttons:[],
				bodyContent:''
			  });
	});
	function openRobot(id,type){
		dialog.set('bodyContent','<iframe src="robot_edit.php?id='+id+'&type='+type+'" scrolling="yes" frameborder="0" height="100%" width="100%"></iframe>');
		dialog.updateContent();
		dialog.show();
	}
	function addRobot(){
		dialog.set('bodyContent','<iframe src="robot_add.php" scrolling="yes" frameborder="0" height="100%" width="100%"></iframe>');
		dialog.updateContent();
		dialog.show();
	}
</script>
</body>
</html>
