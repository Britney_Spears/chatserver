<?php
	
	//清空缓存文件
	$dir = "cache/";  //要获取的目录
	//先判断指定的路径是不是一个文件夹
	if (is_dir($dir)){
		$dir_arr = scandir($dir);
        foreach($dir_arr as $key=>$val){
			if(is_file($dir . $val)) {
				unlink($dir.'/'.$val);
			}
        }
	}

	//$sql="select * from {$tablepre}msgs order by id desc";
	$sql = "select m.*,ms.*  from {$tablepre}members m,{$tablepre}memberfields ms where m.uid=ms.uid and m.uid!=0 and m.gid!='0' order by m.uid asc";
	$query=$db->query($sql);
	$msgs = array();
	while($row=$db->fetch_row($query)) {
		$msgs[] = $row;
	}

	$gids = array();
	$query=$db->query("select id,title from {$tablepre}auth_group order by id desc");
	while($row=$db->fetch_row($query)){
		$gids[$row['id']] = $row['title'];
	}
	/*$type = array();
	$type["0"]="聊天";
	$type["1"]="登陆";
	$type["2"]="注册";
	$type["3"]="入室";*/
	//print_r($gids);exit;

	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
	date_default_timezone_set('Asia/Shanghai');

	define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

	/** Include PHPExcel */
	require_once '../../include/PHPExcel/PHPExcel.php';

	// Create new PHPExcel object
	//echo date('H:i:s') , " Create new PHPExcel object" , EOL;
	$objPHPExcel = new PHPExcel();

	// Set document properties
	//echo date('H:i:s') , " Set document properties" , EOL;
	$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
								 ->setLastModifiedBy("Maarten Balliauw")
								 ->setTitle("PHPExcel Test Document")
								 ->setSubject("PHPExcel Test Document")
								 ->setDescription("Test document for PHPExcel, generated using PHP classes.")
								 ->setKeywords("office PHPExcel php")
								 ->setCategory("Test result file");


	// Add some data
	//echo date('H:i:s') , " Add some data" , EOL;
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('B1', '编号')
				->setCellValue('C1', '用户名')
				->setCellValue('D1', '昵称')
				->setCellValue('E1', '手机')
				->setCellValue('F1', '用户组')
				->setCellValue('G1', 'QQ')
				->setCellValue('H1', '推广人')
				->setCellValue('I1', '注册时间')
				->setCellValue('J1', '注册IP')
				->setCellValue('K1', '最近登录')
				->setCellValue('L1', '来源房间')
				->setCellValue('M1', '是否审核');
				
	foreach($msgs as $k => $v) {
		$objPHPExcel->setActiveSheetIndex(0)
				//->setCellValue('A' . ($k + 2), $k + 1)
				->setCellValue('B' . ($k + 2), $v['uid'])
				->setCellValue('C' . ($k + 2), $v['username'])
				->setCellValue('D' . ($k + 2), $v['nickname'])
				->setCellValue('E' . ($k + 2), $v['phone'])
				->setCellValue('F' . ($k + 2), $gids[$v['gid']])
				->setCellValue('G' . ($k + 2), $v['realname'])
				->setCellValue('H' . ($k + 2), $v['tuser'])
				->setCellValue('I' . ($k + 2), date('Y-m-d H:i', $v['regdate']))
				->setCellValue('J' . ($k + 2), $v['regip'])
				->setCellValue('K' . ($k + 2), date('Y-m-d H:i', $v['lastactivity']))
				->setCellValue('L' . ($k + 2), $v['rid'])
				->setCellValue('M' . ($k + 2), ($v['state'] == '1' ? "是" : "否"));
	}

	// print_r($res);exit;
	//foreach($res2 as $k => $re) {
	//	$objPHPExcel->setActiveSheetIndex(0)
	//			->setCellValue('D' . ($k + 2), $re['media_name'])
	//			->setCellValue('E' . ($k + 2), $re['c']);
	//}


	// Rename worksheet
	//echo date('H:i:s') , " Rename worksheet" , EOL;
	$objPHPExcel->getActiveSheet()->setTitle('Simple');


	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);


	// Save Excel 2007 file
	//echo date('H:i:s') , " Write to Excel2007 format" , EOL;
	$callStartTime = microtime(true);

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$filename = 'cache/' . date('YmdHis').'.xls';
	//echo $filename;exit;
	//echo 'ok1';
	$objWriter->save($filename);
	//echo 'ok';exit;
	
	if (isset($filename)) {
		Header("HTTP/1.1 303 See Other");
		Header("Location: $filename");
	}