<?php
require_once '../../include/common.inc.php';
require_once '../function.php';
if(stripos(auth_group($_SESSION['login_gid']),'robots_admin')===false)exit("没有权限！");
if($act=="user_add"){
    if(user_exist($username)) {
        echo '<script>alert("用户名已存在！");location.href="user_add.php";</script>';
    } else {
        list($up_hour, $up_minute, $up_second) = explode(':', $online);
        list($down_hour, $down_minute, $down_second) = explode(':', $downline);
        robot_add($username,$nickname,$gid,$tuser,$week,$up_hour, $up_minute, $up_second,$down_hour, $down_minute, $down_second);
    }
}
?>
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
<style type="text/css">
code { padding: 0px 4px; color: #d14; background-color: #f7f7f9; border: 1px solid #e1e1e8; }
input,select{vertical-align:middle;}
.liw { width:160px; height:25px; line-height:25px;}
</style>
</head>
<body>
<div class="container" style="margin:20px; padding: 20px 0 0;">
<?php
$query=$db->query("select * from {$tablepre}auth_group order by id desc");
while($row=$db->fetch_row($query)){
	$group.='<option value="'.$row[id].'">GID:'.$row[id].'-'.$row[title].'</option>';
}
if(stripos(auth_group($_SESSION['login_gid']),'users_group')===false){
	$group='';
}

echo '<form action="" onsubmit="return validate();" method="post" enctype="application/x-www-form-urlencoded">
    <ul class="breadcrumb">
        <table class="table table-bordered table-hover definewidth m10">
          <tr>
            <td width="120" class="tableleft" style="width:70px;">用户名：</td>
            <td><input name="username" type="text" id="username" style="width:260px;" value=""/>&nbsp;&nbsp;<span style="color: red;">* 必填</span> 4-16位</td>
          </tr>
          <tr>
            <td width="80" class="tableleft" style="width:70px;">昵称：</td>
            <td><input name="nickname" type="text" id="nickname" style="width:260px;" value=""/>&nbsp;&nbsp;<span style="color: red;">* 必填</span></td>
          </tr>
          <tr>
            <td width="80" class="tableleft">用 户 组：</td>
            <td><select name="gid" id="gid" >
              '.$group.'
            </select>&nbsp;</td>
          </tr>
          <tr>
            <td width="80" class="tableleft">推广用户：</td>
            <td><input name="tuser" type="text" id="tuser" style="width:260px;" value=""/></td>
          </tr>
          <tr>
            <td width="80" class="tableleft">在线时段：</td>
            <td>
              <input name="week" type="text" id="week" style="width:150px;" value=""/>&nbsp;&nbsp;周(一,二,三,四,五,六,日)  填1,2,3,4,5,6,7 逗号隔开<br><br>
              <input name="online" type="text" id="online" class="calendar" style="width:150px;" value=""/>&nbsp;&nbsp;上线时间<br><br>
              <input name="downline" type="text" id="downline" class="calendar" style="width:150px;" value=""/>&nbsp;&nbsp;下线时间<br><br>
            </td>
          </tr>
        </table>
    </ul>
    <div style="bottom:0; background: #FFF; width:100%; padding-top:5px;">
        <button type="submit"  class="button button-success">确定</button>
        <button type="button"  class="button" onclick="window.parent.dialog.close()">关闭</button>
        <input type="hidden" name="act" value="user_add">
    </div>
</form>
</div>
';?>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script> 
<script type="text/javascript" src="../assets/js/bui.js"></script> 
<script type="text/javascript" src="../assets/js/config.js"></script> 
<script type="text/javascript" src="../../upload/swfupload/swfupload.js"></script> 
<script>
    function validate() {
        var username = $('#username').val().trim();
        if(username === '') {
            $('#username').focus();
            return false;
        } else if(username.length < 4 || username.length > 16) {
            $('#username').val('');
            $('#username').focus();
            return false;
        }
        var nickname = $('#nickname').val().trim();
        if(nickname === '') {
            $('#nickname').focus();
            return false;
        }
        var password = $('#password').val().trim();
        if(password === '') {
            $('#password').focus();
            return false;
        } else if(password.length < 3 || password.length > 16) {
            $('#password').val('');
            $('#password').focus();
            return false;
        }
        return true;
    }

    BUI.use('bui/calendar',function(Calendar){
      var datepicker1 = new Calendar.DatePicker({
        trigger:'#online',
        showTime : true,
        dateMask : 'HH:MM:ss',
        autoRender : true
      });
      var datepicker1 = new Calendar.DatePicker({
        trigger:'#downline',
        showTime : true,
        dateMask : 'HH:MM:ss',
        autoRender : true
      });
    });
</script>
</body>
</html>
