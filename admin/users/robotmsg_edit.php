<?php
	require_once '../../include/common.inc.php';
	require_once '../function.php';
	if(stripos(auth_group($_SESSION['login_gid']),'user_robotmsg')===false)exit("没有权限！");
	switch($act) {
		case "robotmsg_edit":
			$sendtime = strtotime($sendtime);
			$db->query("update {$tablepre}robotmsg set rid='$rid',msg='$msg',sendtime='$sendtime',state='0' where id='$id'");
			echo '<script>parent.location.reload();</script>';exit();
		break;
		case "robotmsg_add":
			$sendtime = strtotime($sendtime);
			$db->query("insert into {$tablepre}robotmsg set rid='$rid',msg='$msg',sendtime='$sendtime'");
			echo '<script>parent.location.reload();</script>';exit();
		break;
	}
	$query=$db->query("select * from {$tablepre}robotmsg where id='$id'");
	if($db->num_rows($query)>0) {
		$row=$db->fetch_row($query);
	}
?>
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
<style type="text/css">
code { padding: 0px 4px; color: #d14; background-color: #f7f7f9; border: 1px solid #e1e1e8; }
input,select{vertical-align:middle;}
.liw { width:160px; height:25px; line-height:25px;}
</style>
</head>
<body>
<div class="container" style="margin-bottom:50px;">
<form action="?id=<?=$id?>" method="post" enctype="application/x-www-form-urlencoded">
	<ul class="breadcrumb">
		<table class="table table-bordered table-hover definewidth m10">
			<tr>
				<td width="60" class="tableleft">房间号：</td>
				<td><input name="rid" type="text" id="rid" value="<?php echo empty($row[rid]) ? '1001' : $row[rid]; ?>"/></td>
			</tr>
			<tr>
				<td width="60" class="tableleft" style="width:70px;">发言内容：</td>
				<td><textarea name="msg" id="msg" style="width:100%;"  class="xheditor {cleanPaste:0,height:'200',internalScript:true,inlineScript:true,linkTag:true,upLinkUrl:'../../upload/upload.php',upImgUrl:'../../upload/upload.php',upFlashUrl:'../../upload/upload.php',upMediaUrl:'../../upload/upload.php'}"><?=tohtml($row[msg])?></textarea></td>
			</tr>
			<tr>
				<td width="60" class="tableleft" style="width:70px;">发言时间：</td>
				<td><input name="sendtime" type="text" id="sendtime" value="<?php echo empty($row[sendtime]) ? date('Y-m-d H:i:s') : date('Y-m-d H:i:s', $row[sendtime]); ?>" class="calendar calendar-time" /></td>
			</tr>
		</table>
	</ul>
	<div style="position:fixed; bottom:0; background: #FFF; width:100%; padding-top:5px;">
		<button type="submit"  class="button button-success">确定</button>
		<button type="button"  class="button" onclick="window.parent.dialog.close()">关闭</button>
		<input type="hidden" name="act" value="<?php echo empty($id) ? 'robotmsg_add' : 'robotmsg_edit'; ?>">
	</div>
</form>
</div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script> 
<script type="text/javascript" src="../assets/js/bui.js"></script> 
<script type="text/javascript" src="../assets/js/config.js"></script>
<script type="text/javascript" src="../../xheditor/xheditor.js"></script>
<script type="text/javascript" src="../../xheditor/xheditor_lang/zh-cn.js"></script>
<script>
	BUI.use('bui/calendar',function(Calendar){
		var datepicker = new Calendar.DatePicker({
			trigger:'.calendar-time',
			minDate : '<?php echo date('Y-m-d');?>',
			showTime:true,
			autoRender : true,
		});
	});
</script>
</body>
</html>
