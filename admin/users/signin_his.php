<?php
    require_once '../../include/common.inc.php';
    require_once '../function.php';
    if(stripos(auth_group($_SESSION['login_gid']),'users_signin')===false)exit("没有权限！");
?>
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
<style type="text/css">
code { padding: 0px 4px; color: #d14; background-color: #f7f7f9; border: 1px solid #e1e1e8; }
</style>
</head>
<body>
<div class="container">
    <table  class="table table-bordered table-hover definewidth m10">
        <thead>
          <tr style="font-weight:bold" >
            <td width="20" align="center" bgcolor="#FFFFFF">编号</td>
            <td width="20" align="center" bgcolor="#FFFFFF">用户名</td>
            <td width="20" align="center" bgcolor="#FFFFFF">签到时间</td>
          </tr>
        </thead>
        <?php
        $sql="select uname,FROM_UNIXTIME(signtime, '%Y-%m-%d %H:%i:%s') as signtime from {$tablepre}signin where uid='" . $uid ."'";
        $sql .= " order by signtime desc";
        echo userlist(15,$sql,'
            <tr>
                <td align="center" bgcolor="#FFFFFF">{unique_id}</td>
                <td align="center" bgcolor="#FFFFFF">{uname}</td>
                <td align="center" bgcolor="#FFFFFF">{signtime}</td>
            </tr>
        ')?>
	</table>
</div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script> 
<script type="text/javascript" src="../assets/js/bui.js"></script> 
<script type="text/javascript" src="../assets/js/config.js"></script> 
</body>
</html>
