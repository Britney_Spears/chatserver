<?php
require_once '../../include/common.inc.php';
require_once '../function.php';
if(stripos(auth_group($_SESSION['login_gid']),'users_admin')===false)exit("没有权限！");
if($act=="user_add"){
    if(user_exist($username)) {
        echo '<script>alert("用户名已存在！");location.href="user_add.php";</script>';
    } else {
        //user_add($username,$password,$phone,$gid,$sex,$tuser,$sn,$nickname,$realname,$email,$gold);
		$db->query("insert into {$tablepre}members (username, password, phone, tuser, sex, realname, email, gold, regdate, regip, state) values('".$username."', '".md5($password)."', '".$phone."', '".$tuser."','".$sex."','".$realname."','".$gold."','".$email."','".time()."','".$onlineip."', '".$state."')");	
		$uid=$db->insert_id();
		if(stripos(auth_group($_SESSION['login_gid']),'users_group')!==false) {
			$db->query("update {$tablepre}members set gid='$gid'  where uid='$uid'");
		}
		$db->query("insert into {$tablepre}memberfields (uid,sn,nickname) values('$uid','$sn','$nickname')");	
    }
}
?>
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
<style type="text/css">
code { padding: 0px 4px; color: #d14; background-color: #f7f7f9; border: 1px solid #e1e1e8; }
input,select{vertical-align:middle;}
.liw { width:160px; height:25px; line-height:25px;}
</style>
</head>
<body>
<div class="container" style="margin:20px; padding: 20px 0 0;">
<?php
$query=$db->query("select * from {$tablepre}auth_group order by id desc");
while($row=$db->fetch_row($query)){
	$group.='<option value="'.$row[id].'">GID:'.$row[id].'-'.$row[title].'</option>';
}
if(stripos(auth_group($_SESSION['login_gid']),'users_group')===false){
	$group='';
}

echo '<form action="" onsubmit="return validate();" method="post" enctype="application/x-www-form-urlencoded">
    <ul class="breadcrumb">
        <table class="table table-bordered table-hover definewidth m10">
			<tr>
            <td width="80" class="tableleft">状态：</td>
            <td><select name="state" id="state" >
              <option value="1">S:1已审核</option>
			  <option value="0">S:0未审核</option>
			  <option value="3">S:3梦游</option>
            </select>&nbsp;</td>
          </tr>
            <tr>
            <td width="120" class="tableleft" style="width:70px;">用户名：</td>
            <td><input name="username" type="text" id="username" style="width:350px;" value=""/>&nbsp;&nbsp;<span style="color: red;">* 必填</span> 4-16位</td>
          </tr>
            <tr>
            <td width="80" class="tableleft" style="width:70px;">昵称：</td>
            <td><input name="nickname" type="text" id="nickname" style="width:350px;" value=""/>&nbsp;&nbsp;<span style="color: red;">* 必填</span></td>
          </tr>
          <tr>
            <td width="80" class="tableleft">用户密码：</td>
            <td><input name="password" type="text" id="password" style="width:350px;" value="" />&nbsp;&nbsp;<span style="color: red;">* 必填</span> 6-16位</td>
          </tr>
          <tr>
            <td width="80" class="tableleft" style="width:70px;">QQ号码：</td>
            <td><input name="realname" type="text" id="realname" style="width:350px;" value=""/></td>
          </tr>
          <!--<tr>
            <td width="80" class="tableleft" style="width:70px;">Email：</td>
            <td><input name="email" type="text" id="email" style="width:350px;" value=""/></td>
          </tr>-->
          <tr>
            <td width="80" class="tableleft">手机号码：</td>
            <td><input name="phone" type="text" id="phone" style="width:350px;" value=""/></td>
          </tr>
          <tr>
            <td width="80" class="tableleft">余额：</td>
            <td><input name="gold" type="text" id="gold" style="width:350px;" value=""/></td>
          </tr>
          <tr>
            <td width="80" class="tableleft">用 户 组：</td>
            <td><select name="gid" id="gid" >
              '.$group.'
            </select>&nbsp;</td>
          </tr>
          <tr>
            <td width="80" class="tableleft">性别：</td>
            <td>
                <select name="sex" id="sex" >
                    <option value="1">男</option>
                    <option value="2">女</option>
                </select>
            </td>
          </tr>
          <tr>
            <td width="80" class="tableleft">推广用户：</td>
            <td><input name="tuser" type="text" id="tuser" style="width:350px;" value=""/></td>
          </tr>
          <tr>
            <td width="80" class="tableleft">其他备注：</td>
            <td><textarea name="sn" id="sn" style="width:350px;"></textarea></td>
          </tr>
        </table>
    </ul>
    <div style="bottom:0; background: #FFF; width:100%; padding-top:5px;">
        <button type="submit"  class="button button-success">确定</button>
        <button type="button"  class="button" onclick="window.parent.dialog.close()">关闭</button>
        <input type="hidden" name="act" value="user_add">
    </div>
</form>
</div>
';?>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script> 
<script type="text/javascript" src="../assets/js/bui.js"></script> 
<script type="text/javascript" src="../assets/js/config.js"></script> 
<script>
    function validate() {
        var username = $('#username').val().trim();
        if(username === '') {
            $('#username').focus();
            return false;
        } else if(username.length < 4 || username.length > 16) {
            $('#username').val('');
            $('#username').focus();
            return false;
        }
        var nickname = $('#nickname').val().trim();
        if(nickname === '') {
            $('#nickname').focus();
            return false;
        }
        var password = $('#password').val().trim();
        if(password === '') {
            $('#password').focus();
            return false;
        } else if(password.length < 3 || password.length > 16) {
            $('#password').val('');
            $('#password').focus();
            return false;
        }
        return true;
    }
</script>
</body>
</html>
