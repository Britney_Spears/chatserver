<?php  

	header("Content-type: text/html; charset=utf-8");   
	require_once '../../include/PHPExcel/PHPExcel.php';
	require_once '../../include/PHPExcel/PHPExcel/IOFactory.php';
	require_once '../../include/PHPExcel/PHPExcel/Reader/Excel2007.php';
	//$filepath = '../../upload/upfile/day_170518/201705181755324940.xls';

	$objReader = PHPExcel_IOFactory::createReader('Excel2007');/*Excel5 for 2003 excel2007 for 2007*/  
	$objPHPExcel = $objReader->load($filepath); //Excel 路径  
	$sheet = $objPHPExcel->getSheet(0);  
	$highestRow = $sheet->getHighestRow(); // 取得总行数  
	$highestColumn = $sheet->getHighestColumn(); // 取得总列数 
	/*方法二【推荐】*/  
	$objWorksheet = $objPHPExcel->getActiveSheet();          
	$highestRow = $objWorksheet->getHighestRow();   // 取得总行数       
	$highestColumn = $objWorksheet->getHighestColumn();          
	$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);//总列数  
  
	$ret = array();
	$insert_count = 0;
	$name_exist = '';
	for ($row = 2;$row <= $highestRow;$row++)         {  
		$strs=array();  
		//注意highestColumnIndex的列数索引从0开始  
		for ($col = 0;$col < $highestColumnIndex;$col++) {  
			$temp =$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();  
			if($col == 0) {
				$username = $temp;
			} elseif ($col == 1) {
				$nickname = $temp;
			} elseif ($col == 2) {
				$pwd = md5($temp);
			} elseif ($col == 3) {
				$phone = $temp;
			} elseif ($col == 4) {
				$gid = $temp;
			} elseif ($col == 5) {
				$realname = $temp;
			} elseif ($col == 6) {
				$tuser = $temp;
			} elseif ($col == 7) {
				$rid = $temp;
			} elseif ($col == 8) {
				$state = $temp;
			}
		}  
		//print_r($strs); 

		$res = insert_user($username, $nickname, $pwd, $phone, $gid, $realname, $tuser, $rid, $state);
		if($res) {
			++$insert_count;
		} else {
			$name_exist .= $username . ',';
		}
	}
	
	$ret = '共导入' . $insert_count . '条数据，';
	if($name_exist != '') {
		$name_exist = trim($name_exist, ',');
		$ret .= "用户名[{$name_exist}]已存在"; 
	}
	
	
	function insert_user($username, $nickname, $password, $phone, $gid, $qq, $tuser, $rid, $state) {
		//echo 3 . '<br>';
		global $db,$tablepre;
		//print_r($db);exit;
		$row = $db->fetch_row($db->query("select username from {$tablepre}members where username='$username'"));
		if(!empty($row)) {
			return false;
		}
		
		$db->query("insert into {$tablepre}members(rid,username,password,sex,email,regdate,regip,lastvisit,lastactivity,gold,realname,gid,phone,fuser,tuser,state)	values('$rid','$username','$password','2','$email','$regtime','$onlineip','$regtime','$regtime','0','$qq','1','$phone','$tuser','$tuser','$state')");
		$uid = $db->insert_id();
		$db->query("replace into {$tablepre}memberfields (uid,nickname)	values('$uid','$nickname')	");
		
		return true;
	}
	
	