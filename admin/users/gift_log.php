<?php

    require_once '../../include/common.inc.php';
    require_once '../function.php';
    if(stripos(auth_group($_SESSION['login_gid']),'user_gift_log')===false)
		exit("没有权限！");

	switch($act){
		case "log_del":
			$del_ids = (implode(',',$id));
			$sql = "delete from {$tablepre}gold_log where id in($del_ids)";
			$db->query($sql);
			break;
		case "clear_log":
			$sql = "delete from {$tablepre}gold_log";
			$db->query($sql);
			break;
	}

?>
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
<style type="text/css">
code { padding: 0px 4px; color: #d14; background-color: #f7f7f9; border: 1px solid #e1e1e8; }
</style>
<script>
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
function ftime(time){
	return new Date(time*1000).Format("yyyy-MM-dd hh:mm"); ; 
}
</script>
</head>
<body>
<div class="container"  style=" min-width:1150px;">
	<form  class="form-horizontal" action="" method="get"> 
		<ul class="breadcrumb">
			<li class="active">
				<button type="button"  class="button  button-danger" id="add_ban_bt" onClick="if(confirm('确定删除？'))$('#log_list').submit()">删除所选</button>
				<button type="button"  class="button  button-danger" id="add_ban_bt" onClick="if(confirm('确定清空日志？'))location.href='?act=clear_log'">清空日志</button>
				&nbsp;&nbsp;
			</li>
		</ul>
	</form>
	<form action="" method="POST" enctype="application/x-www-form-urlencoded"  class="form-horizontal" id="log_list">
		<input type="hidden" name="act" value="log_del"> 
		<table  class="table table-bordered table-hover definewidth m10">
			<thead>
			  <tr style="font-weight:bold" >
				<td width="19" align="center" bgcolor="#FFFFFF"><input type="checkbox" onClick="$('.ids').attr('checked',this.checked); "></td>  
				<td width="80" align="center" bgcolor="#FFFFFF">序号</td>
				<td width="150" align="center" bgcolor="#FFFFFF">用户</td>
				<td width="150" align="center" bgcolor="#FFFFFF">消费(秀币)</td>
				<td width="150" align="center" bgcolor="#FFFFFF">消费时间</td>
				<td width="" align="center" bgcolor="#FFFFFF">备注</td>
			  </tr>
			</thead>
			<?php
				$sql = "select gl.*,mf.nickname from {$tablepre}gold_log gl, {$tablepre}memberfields mf where gl.uid=mf.uid";
				$count = $db->num_rows($db->query($sql));
				pageft($count, 20, "");
				$sql .= " order by dateline desc";
				$sql .= " limit $firstcount,$displaypg";
				$query = $db->query($sql);
				echo for_each($query,'<tr>
											<td><input type="checkbox" class="ids" name="id[]" value="{id}"></td>
											<td>{id}</td>
											<td>{nickname}</td>
											<td>{gold}</td>
											<td><script>document.write(ftime("{dateline}"));</script></td>
											<td>{txt}</td>
										</tr>');
			?>
		</table>
    </form> 
    <ul class="breadcrumb">
        <li class="active"><?=$pagenav?></li>
    </ul>
</div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script> 
<script type="text/javascript" src="../assets/js/bui.js"></script> 
<script type="text/javascript" src="../assets/js/config.js"></script>
</body>
</html>