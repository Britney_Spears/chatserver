<?php
require_once '../../include/common.inc.php';
require_once '../function.php';
if(stripos(auth_group($_SESSION['login_gid']),'tongji_reg')===false)exit("没有权限！");
function count_guest($tuid,$ym,$end){
	global $db,$tablepre;
	$sql="select count(*) as t1 from {$tablepre}msgs where type='3' and tuid='$tuid' and uname like '游客%'";
	if($ym!=""){$sql.=" and mtime>'".strtotime($ym)."' and mtime<'".strtotime($end)."'";}
	$row=$db->fetch_row($query=$db->query($sql));
	return $row[t1];
}
function count_guest_ip($tuid,$ym,$end){
	global $db,$tablepre;
	$sql="select count(DISTINCT ip) as t1 from {$tablepre}msgs where type='3' and tuid='$tuid' and uname like '游客%'";
	if($ym!=""){$sql.=" and mtime>'".strtotime($ym)."' and mtime<'".strtotime($end)."'";}
	$row=$db->fetch_row($query=$db->query($sql));
	return $row[t1];
}
switch($type){
	case 'loginroom':
		$sql="select count(*) as t1,COUNT( DISTINCT ip )as ips,tname,tuid from {$tablepre}msgs where type='3'";
		//if($ym!=""){$sql.=" and FROM_UNIXTIME(mtime,'%Y-%m')='$ym'";}
		if($ym!=""){$sql.=" and mtime>'".strtotime($ym)."' and mtime<'".strtotime($end)."'";}
		$query=$db->query($sql." group by tname order by t1 desc");
		//echo $sql." group by tname order by t1 desc";exit;
		//exit($sql." group by tname order by t1 desc");
		$c1['tag']=array();
		$c1['data1']=array();
		$c1['data2']=array();
		$c1['data3']=array();
		$c1['data4']=array();
		$c1['data1_tag']="访问数";
		$c1['data2_tag']="游客访问数";
		$c1['data3_tag']="访客独立IP数";
		$c1['data4_tag']="游客独立IP数";
		$c1['sn']="";
		$c1['title']="访客数统计";
		while($row=$db->fetch_row($query)){
			array_push($c1['tag'],"'{$row[tname]}'");
			array_push($c1['data1'],$row[t1]);
			array_push($c1['data2'],count_guest($row[tuid],$ym,$end));
			array_push($c1['data3'],$row[ips]);
			array_push($c1['data4'],count_guest_ip($row[tuid],$ym,$end));
		}
	break;
}

//今日游客
global $db,$tablepre;
$sql="select count(*) as t1 from {$tablepre}msgs where type='3' and uname like '游客%' and mtime > '" . strtotime(date('y-m-d')) . "';";
$row=$db->fetch_row($query=$db->query($sql));
$today_guest_count = $row[t1];

//同时在线人数
$sql="select * from {$tablepre}online_stat where cdate = '" . date('y-m-d') . "';";
$query=$db->query($sql);
while($row=$db->fetch_row($query)) {
	$categories[] = $row['ctime'];
	$datas[] = $row['online_num'];
}
$categorie = "'" . implode("','", $categories) . "'";
$data = "'" . implode("','", $datas) . "'";
//echo $sql . '<br>';
//echo $data . '<br>' . $categorie;exit;
$categories = '';

?>
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
<style type="text/css">
code { padding: 0px 4px; color: #d14; background-color: #f7f7f9; border: 1px solid #e1e1e8; }
</style>
<script>
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
function ftime(time){
	return new Date(time*1000).Format("yyyy-MM-dd hh:mm"); ; 
}
</script>
</head>
<body>
<div class="container"  style=" min-width:1100px;">
<form  class="form-horizontal" action="" method="get"> 
  <!--<ul class="breadcrumb">
    <li class="active">
    <input type="hidden" name="type" value="<?=$type?>">
    按时间段：
      <input type="text" name="ym" id="ym"  class="calendar" value="<?=$ym?>"> 至
      <input type="text" name="end" id="end"  class="calendar" value="<?=$end?>">
      &nbsp;&nbsp;
      <button type="submit"  class="button ">查询</button> 为空统计所有
    &nbsp;&nbsp;</li>
   
  </ul>-->
  </form>
  <table  class="table table-bordered table-hover definewidth m10">
	<tr>
		<td>	
			当前在线人数：<span id="online_num"></span>
		</td>
		<td>
			今日访客数：<span id="today_visit"><?php echo $today_guest_count; ?></span>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<div id="canvas"></div>
		</td>
	</tr>
  </table>
</div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script> 
<script type="text/javascript" src="../assets/js/bui.js"></script> 
<script type="text/javascript" src="../assets/js/config.js"></script> 
<script type="text/javascript" src="../assets/js/socket.io.js"></script>
<script type="text/javascript" src="http://g.tbcdn.cn/bui/acharts/1.0.18/acharts.js"></script> 
<script type="text/javascript">
	var onlineNum = 0;
	$(document).ready(function() {
		OnSocket();
	});

	function OnSocket() {    
		socket = io("ws://<?php echo $cfg['config']['tserver']; ?>");
		socket.on('connect', function() {
			//var str = 'Login=M=www.dyzbs.cn/1|-1|从良|1|0|1|127.0.0.1|2|0|0|0|我是管理员';
			//socket.emit('message', str);
			socket.emit('onlineUser', '');
		});
		socket.on('onlineUser', function(data) {
			WriteMessage(data);
		});
	}
	
	function WriteMessage(txt) {
	//    if(txt.indexOf('SendMsg')!=-1)
	//    alert(txt);
		var Msg;
		try {
			Msg = eval("(" + txt + ")");
		} catch (e) {
			return;
		}
		switch (Msg.type)
		{
			case "Ulogin":
				onlineNum = $('#online_num').html();
				$('#online_num').html(parseInt(onlineNum) + 1);

				break;
			case "onlineUser":

				var onlineNum = Msg.roomListUser.length;
				//console.log(onlineNum);
				$('#online_num').html(onlineNum - 1);
				break;
			case "Ulogout":
				onlineNum = $('#online_num').html();
				$('#online_num').html(parseInt(onlineNum) + 1);
				
				break;
		}

	}

    BUI.use('bui/calendar',function(Calendar){
          var datepicker = new Calendar.DatePicker({
            trigger:'.calendar',
			dateMask : 'yyyy-mm-dd',
            autoRender : true
          });
        });
    BUI.use('common/page');
	
	//折线统计图开始http://builive.com/chart/line.php#line/base.php
	var chart = new AChart({
		id : 'canvas', //render改成 id
		theme : Chart.Theme.Smooth1,
		width : 950,
		height : 500,
		plotCfg : {
		margin : [50,50,80] //画板的边距
	},
	title : {
		text : '同时在线人数'
	},
	xAxis : {
		categories : [<?php echo $categorie; ?>]
	},
	yAxis : {
		title : {
			text : '人数',
			rotate : -90
		}
	},
	yTickCounts : [1,2],
	tooltip : {
		shared : true, //是否多个数据序列共同显示信息
		crosshairs : true //是否出现基准线
	},
	series : [
	{
		name: '同时在线人数',
		markers : {
		markerclick : function (ev) {
			console.log(ev);
		}
	},
	data: [<?php echo $data; ?>]
	}]
	});

	chart.render();
	//折线统计图结束

  </script>

</body>
</html>
