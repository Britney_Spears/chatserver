<?php
require_once '../../include/common.inc.php';
require_once '../function.php';
if(stripos(auth_group($_SESSION['login_gid']),'tongji_reg')===false)exit("没有权限！");

switch($type){
    case 'loginroom':
        if(empty($ymd)) {
            $ymd = date('y-m-d');
            if(intval(date('H')) < 12) {
                $day = '';
            } else {
                $day = 'and type = 2';
            }
        } else {
            $day = ' and type = 1';
        }
        //同时在线人数
        $sql="select * from {$tablepre}online_stat where cdate = '$ymd' $day order by ctime;";
        $query=$db->query($sql);
        $c1['tag']=array();
        $c1['data1']=array();
        $c1['data2']=array();
        $c1['data3']=array();
        $c1['data4']=array();
        $c1['data1_tag']="同时在线人数";
        $c1['sn']="";
        $c1['title']="同时在线人数统计";
        while($row=$db->fetch_row($query)){
                array_push($c1['tag'],"'{$row[ctime]}'");
                array_push($c1['data1'],$row[online_num]);
        }
    break;
}

//今日游客
global $db,$tablepre;
$sql="select count(*) as t1 from {$tablepre}msgs where type='3' and uname like '游客%' and mtime > '" . strtotime(date('y-m-d')) . "';";
$row=$db->fetch_row($query=$db->query($sql));
$today_guest_count = $row[t1];

?>
<!DOCTYPE HTML>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/bui-min.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/page-min.css" rel="stylesheet" type="text/css" />
<!-- 下面的样式，仅是为了显示代码，而不应该在项目中使用-->
<link href="../assets/css/prettify.css" rel="stylesheet" type="text/css" />
<style type="text/css">
code { padding: 0px 4px; color: #d14; background-color: #f7f7f9; border: 1px solid #e1e1e8; }
</style>
<script>
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
function ftime(time){
	return new Date(time*1000).Format("yyyy-MM-dd hh:mm"); ; 
}
</script>
</head>
<body>
<div class="container"  style=" min-width:1000px;">
	<form  class="form-horizontal" action="" method="get"> 
		<ul class="breadcrumb">
			<li class="active">
				<input type="hidden" name="type" value="<?=$type?>">
				按统计日期：<input type="text" name="ymd" id="ymd"  class="calendar" value="<?=$ymd?>">&nbsp;&nbsp;
				<button type="submit"  class="button ">查询</button> 为空统计当天数据 &nbsp;&nbsp;
			</li>
		</ul>
	</form>
	<table class="table table-bordered table-hover definewidth m10">
		<tr>
			<td>	
				当前在线人数：<span id="online_num"></span>
			</td>
			<td>
				今日访客数：<span id="today_visit"><?php echo $today_guest_count; ?></span>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div id="canvas"></div>
			</td>
		</tr>
	</table>
	<table class="table table-bordered table-hover definewidth m10">
		<tr>
			<td>
				<div class="row">
					<div class="span24" id="canvas"></div>
				</div>
			</td>
		</tr>
	</table>
</div>
<script type="text/javascript" src="../assets/js/jquery-1.8.1.min.js"></script> 
<script type="text/javascript" src="../assets/js/bui.js"></script> 
<script type="text/javascript" src="../assets/js/config.js"></script>
<script type="text/javascript" src="../assets/js/socket.io.js"></script>
<script type="text/javascript">
	var onlineNum = 0;
	$(document).ready(function() {
		OnSocket();
	});

	function OnSocket() {    
		socket = io("ws://<?php echo $cfg['config']['tserver']; ?>");
		socket.on('connect', function() {
			socket.emit('onlineUser', '');
		});
		socket.on('onlineUser', function(data) {
			WriteMessage(data);
		});
	}
	
	function WriteMessage(txt) {
		var Msg;
		try {
			Msg = eval("(" + txt + ")");
		} catch (e) {
			return;
		}
		switch (Msg.type)
		{
			case "Ulogin":
				onlineNum = $('#online_num').html();
				$('#online_num').html(parseInt(onlineNum) + 1);
				break;
			case "onlineUser":
				var onlineNum = Msg.roomListUser.length;
				onlineNum = onlineNum < 1 ? 1 : onlineNum;
				//console.log(onlineNum);
				$('#online_num').html(onlineNum - 1);
				break;
			case "Ulogout":
				onlineNum = $('#online_num').html();
				onlineNum = onlineNum < 1 ? 1 : onlineNum;
				$('#online_num').html(parseInt(onlineNum) - 1);				
				break;
		}

	}
	
    BUI.use('bui/calendar',function(Calendar){
          var datepicker = new Calendar.DatePicker({
            trigger:'.calendar',
			dateMask : 'yy-mm-dd',
            autoRender : true
          });
        });
    BUI.use('common/page');

    BUI.use('bui/chart',function (Chart) {
      
        var chart = new Chart.Chart({
          render : '#canvas',
          width : 1150,
          height : 400,
          title : {
            text : '<?=$c1['title']?>',
            'font-size' : '16px'
          },
          subTitle : {
            text : '<?=$c1['sn']?>'
          },
          xAxis : {
            categories: [
                      <?=implode(',',$c1[tag])?>
                  ]
          },
          yAxis : {
            title : {
              text : ''
            },
            min : 0
          },  
          tooltip : {
            shared : true
          },
          seriesOptions : {
              columnCfg : {
                  
              }
          },
          series: [ {
                  name: '<?=$c1['data1_tag']?>',
                  data: [<?=implode(',',$c1['data1'])?>]
 
              }]
              
        });
 
        chart.render();
    });

  </script>

</body>
</html>
