<?php

    $dbhost = 'localhost';   // 数据库服务器
    $dbuser = 'root';   // 数据库用户名
    $dbpw = 'root';    // 数据库密码
    $dbname = 'chat';   // 数据库名开
    // [CH] 投入使用后不能修改的变量

    $tablepre = 'chat_';      // 表名前缀, 同一数据库安装多个聊天室请修改此处
    // [CH] 小心修改以下变量, 否则可能导致无法正常使用
	$app_id = '';
	$app_secret = '';

    $dbcharset = '';   // MySQL 字符集, 可选 'gbk', 'big5', 'utf8', 'latin1', 留空为按照论坛字符集设定
    $charset = 'utf-8';   // 页面默认字符集, 可选 'gbk', 'big5', 'utf-8'
    $def_cfg = '1';
    $goldname = "金币";
    //$discount = 0.5; //礼物折扣率
    $adminemail = 'admin@your.com';  // 系统管理员 Email
    date_default_timezone_set("Asia/Shanghai");
    $timeoffset = 0; //时差 单位 秒
    $upgrade = 15; //15小时升一级
    $tserver_key = "this is key!!!"; //服务器连接密钥！

    //$guest = true; //游客登录 true开启 false关闭
    //$reg_unallowable="|江泽民|毛泽东|邓小平"; //注册屏蔽关键字 并为空以"|" 开头并隔开
    //$msg_unallowable="黑平台|返佣|日返|高返佣|头寸|打包|手续费|刷单|套利|黑公司|QQ|私聊|群|加群|返佣|黑平台|代理|代客操盘|违规操作"; //聊天屏蔽关键字 空以"|" 并隔开
    $ipmax = 5; //同一IP每天限制注册次数
    
?>