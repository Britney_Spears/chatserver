<?php

require_once './include/common.inc.php';
require_once PPCHAT_ROOT . './include/json.php';
$json = new JSON_obj;
$uid = (int) $uid;
$id = (int) $id;
switch ($act) {
	//讲师上课
    case "setdefvideosrc";
		if(!empty($rid)) {
			$db->query("update {$tablepre}config set defvideo='$vid',defvideonick='$nick' where id='$rid'");
			
			//房间配置写入缓存	
			$query= $db->query("select * from  {$tablepre}config where id={$roomid}");
			$row = $db->fetch_row($query);
			$cachePath = './cache/room_' . $rid . '.txt';
			file_put_contents($cachePath, json_encode($row));
		}		
        break;
    //私聊聊天记录
    case "mymsgold":
        $uid = $_SESSION['login_uid'];
        $query = $db->query("select *  from {$tablepre}msgs where ((uid='$uid' and tuid='$tuid')or(uid='$tuid' and tuid='$uid')) and p='true' and type='0' order by id desc limit 0,20");
        while ($row = $db->fetch_row($query)) {
            $str1 = '<li class="layim_chate[me]"><div class="layim_chatuser"><span class="layim_chattime">[date]</span><span class="layim_chatname">[uname]</span><img src="../face/img.php?t=p1&u=[uid]"></div><div class="layim_chatsay"><font style="color:#000">[msg]</font><em class="layim_zero"></em></div></li>';
            $str2 = '<li class="layim_chate[me]"><div class="layim_chatuser"><img src="../face/img.php?t=p1&u=[uid]"><span class="layim_chatname">[uname]</span><span class="layim_chattime">[date]</span></div><div class="layim_chatsay"><font style="color:#000">[msg]</font><em class="layim_zero"></em></div></li>';
            if ($row['uid'] == $uid)
                $str = str_replace("[me]", "me", $str1);
            else
                $str = str_replace("[me]", "he", $str2);
            $str = str_replace("[uid]", $row['uid'], $str);
            $str = str_replace("[uname]", $row['uname'], $str);
            $str = str_replace("[msg]", tohtml($row['msg']), $str);
            $str = str_replace("[date]", date("Y-m-d H:i:s", $row['mtime']), $str);
            $msgold = $str . $msgold;
        }
        empty($msgold) && exit();
        $data['realname'] = userinfo($tuid, '{realname}');
        $data['tuid'] = $tuid;
        $data['msg'] = $msgold;
        exit($json->encode($data));
        break;
    //屏蔽消息
    case "msgblock":
        $db->query("update {$tablepre}msgs set state='$s' where msgid='$msgid'");
        exit();
        break;
    //我的客服
    case "getmylist":
        $i = 0;
        $fuser = userinfo($_SESSION['login_uid'], '{fuser}');
        //print_r($fuser);exit();
        if ($fuser == "")
            $fuser = $cfg['config']['defkf'];
        //$query = $db->query("select m.*,ms.* from {$tablepre}members m left join {$tablepre}memberfields ms on m.uid=ms.uid where m.username ='$fuser'");
		$query = $db->query("select m.*,ms.*  from {$tablepre}members m,{$tablepre}memberfields ms  where m.uid=ms.uid and m.uid!=0 and m.gid = '3'");
        while ($row = $db->fetch_row($query)) {
            if(!empty($row['uid']) && !empty($row['username'])) {
				$tmp['uid'] = $row['uid'];
				$tmp['chatid'] = $row['uid'];
				$tmp['nick'] = $row['nickname'];
				$tmp['phone'] = $row['phone'];
				$tmp['qq'] = $row['realname'];
				$tmp['gid'] = $row['gid'];
				$tmp['mood'] = $row['mood'];
				$tmp['sn'] = $row['sn'];
				$data['row'][$i++] = $tmp;
			}
        }
		//print_r($data);exit;
		if(empty($data)) {
			$query = $db->query("select m.*,ms.* from {$tablepre}members m left join {$tablepre}memberfields ms on m.uid=ms.uid where m.username ='".$cfg['config']['defkf']."'");
			while ($row = $db->fetch_row($query)) {
				if(!empty($row['uid']) && !empty($row['username'])) {
					$tmp['uid'] = $row['uid'];
					$tmp['chatid'] = $row['uid'];
					$tmp['nick'] = $row['nickname'];
					$tmp['phone'] = $row['phone'];
					$tmp['qq'] = $row['realname'];
					$tmp['gid'] = $row['gid'];
					$tmp['mood'] = $row['mood'];
					$tmp['sn'] = $row['sn'];
					$data['row'][$i++] = $tmp;
				}
			}
		}

        $query = $db->query("select m.*,ms.* from {$tablepre}members m left join {$tablepre}memberfields ms on m.uid=ms.uid   where m.fuser='{$user}'");
        while ($row = $db->fetch_row($query)) {
            if(!empty($row['uid']) && !empty($row['username'])) {
				$tmp['uid'] = $row['uid'];
				$tmp['chatid'] = $row['uid'];
				$tmp['nick'] = $row['nickname'];
				$tmp['phone'] = $row['phone'];
				$tmp['qq'] = $row['realname'];
				$tmp['gid'] = $row['gid'];
				$tmp['mood'] = $row['mood'];
				$tmp['sn'] = $row['sn'];
				$data['row'][$i++] = $tmp;
			}
        }
        $data['state'] = 'true';
        exit($json->encode($data));
        break;
    case "getrlist":
        //机器人列表
        //$rid:房间ID $r:20|50 20-50随机数 机器人个数
        $r = explode("|", $r);
        $r_max = rand($r[0], $r[1]);
        $time = time();
        $query = $db->query("select * from {$tablepre}rebots where rid='$rid' and losttime>{$time}");
        if ($db->num_rows($query) <= 0) {
            //$query = $db->query("select * from {$tablepre}rebots where id='1'");
            //$row = $db->fetch_row($query);
            //$virtual_arr = explode("\r\n", $row['rebots']);
            $now_week = date('N');
            $now_hour = date('H');
            $now_minute = date('i');
            $now_second = date('s');
            $query = $db->query("select * from {$tablepre}robots where week like '%".$now_week."%' and up_hour <= " . $now_hour . " and down_hour >= " . $now_hour);
            $rebots_arr = array();
            while ($row = $db->fetch_row($query)) {
                if($row['up_hour'] == $now_hour) {
                    if($row['up_minute'] > $now_minute) {
                        continue;
                    } elseif($row['up_minute'] == $now_minute) {
                        if($row['second'] > $now_second) {
                            continue;
                        }
                    }
                }
                array_push($rebots_arr, $row);
            }
            shuffle($rebots_arr);
            $roomListUserJsonStr = array("type" => "UonlineUser", "stat" => "OK");
            $roomListUser = array();
            $roomUser = array("roomid" => $_SERVER['HTTP_HOST'] . "." . $rid, "chatid" => "", "ip" => "0.0.0.0", "qx" => "0", "cam" => "", "vip" => "0", "age" => "-", "sex" => "", "mood" => "", "state" => "0", "nick" => "", "color" => "0");
            $count = count($rebots_arr);

			//机器人
            for ($i = 0; $i < $count; $i++) {
                if (empty($rebots_arr[$i]))
                    continue;
                $roomUser['chatid'] = 'xr' . $rebots_arr[$i]['uid'];
                $roomUser['sex'] = rand(0, 2);
                $roomUser['cam'] = rand(0, 2);
                $roomUser['nick'] = $rebots_arr[$i]['nickname'];
                $roomUser['color'] = $rebots_arr[$i]['gid'];
                $roomUser['tname'] = $rebots_arr[$i]['tuser'];
                $roomListUser[$i] = $roomUser;
            }
			//虚拟人
			if ($r_max > 0) {
				$roomUser['color'] = "0";
				for ($i = 0; $i < $r_max; $i++) {
					$roomUser['chatid'] = 'x_r' . $i;
					$roomUser['sex'] = rand(0, 2);
					$roomUser['cam'] = rand(0, 2);
					//$roomUser['nick'] = '游客' . strtoupper(substr(md5(microtime()),0,4));
					$roomUser['nick'] = '游客' . strtoupper(substr(md5(rand()),0,4));
					array_push($roomListUser, $roomUser);
				}
			}
			
			//print_r($roomListUser);exit;
            $roomListUserJsonStr['roomListUser'] = $roomListUser;
            $data = base64_encode($json->encode($roomListUserJsonStr));
            //机器人列表20分钟一换
            $losttime = time() + 20 * 60;
            //$db->query("delete from {$tablepre}rebots where rid='$rid'");
            //$db->query("insert into {$tablepre}rebots(rid,rebots,losttime)values('$rid','$data','$losttime')");
			$row = $db->fetch_row($db->query("select rid from {$tablepre}rebots where rid='$rid'"));
			if(empty($row)) {
				$db->query("insert into {$tablepre}rebots(rid,rebots,losttime)values('$rid','$data','$losttime')");
			} else {
				$db->query("update {$tablepre}rebots set rebots='{$data}',losttime='{$losttime}' where rid='{$rid}'");
			}
        } else {
            //获取有效列表
            $row = $db->fetch_row($query);
            $data = $row['rebots'];
        }
        
        exit(base64_decode($data));
        break;
    case "putmsg":
        $msg = RemoveXSS($_POST[msg]);
        if ($cfg['config']['msgaudit'] == '1' && (!preg_match('/<img src=\\\"\/room\/face\/colorbar\/[a-z]{2,6}.gif\\\">/i', $msg) && !preg_match('/(<img src=\\\"\/room\/face\/pic\/[0-9a-z_]{2,11}.gif\\\">){8}/i', $msg))) {
            $state = '1';
        }
        if ($msgtip == "2")
            $state = '2';
        if ($msgtip == "3")
            $state = '3';
        if ($msgtip == "4")
            $state = '4';
        if ($msgtip == "5")
            $state = '5';
        if ($msgtip == "7")
            $state = '7';
		
        if (check_auth('room_admin') && stripos($muid, 'xr') !== FALSE) {
			$gid = $ugid;
		} else if($muid != $_SESSION['login_uid']) {
			$gid = 0;
		} else {
			$gid = $_SESSION['login_gid'];
		}
		
		if($address == '127'){
			$gid = $ugid;
		}
		
		/*if(stripos($_POST['msg'], 'data:image/png;base64') !== FALSE) {
			preg_match('/src=\\\"(.*?)\\\"/i', $_POST['msg'], $match_base64);
			echo $match_base64[1] . '<hr>';
			//exit;
			$base64_img = str_replace('data:image/png;base64,', '', $match_base64[1]);
			echo $base64_img . '<hr>';
			//exit;
			$img = base64_decode($base64_img);
		echo $img;exit;
			$filename = date("YmdHis") . mt_rand(1000,9999).'.jpg';
			$filepath = 'upload/upfile/cut_' . date('ymd') . '/';
			//echo $filepath;
			//exit;
			if(!is_dir($filepath)) {
				@mkdir($filepath, 0777);
				@fclose(fopen($filepath.'/index.htm', 'w'));
			}
			//exit;
		echo file_put_contents($filename, $img);
		echo '<img src="'.$filename.'">';
		exit;
			file_put_contents($filepath . $filename, $img);
			//exit;
			$_POST['msg'] = "<img src='" . $filepath . $filename . "'>";
			$_POST['msg'] = addslashes($_POST['msg']);
		}*/
		
		//if(stripos($msg, "[喊单提醒]") === FALSE) {
		if(preg_match('/\[<font(.*?)>详细<\/font>\]/i', $msg) || preg_match('/id="radbagmsg"/i', $msg) || $address == '127') {
			$msg = str_replace("<x>","",$msg);
			$msg = addslashes($msg);
		} else {
			$msg = html_entity_decode($msg);
			$msg = strip_tags($msg, '<img>');
		}
		//echo $msg;exit;
        $sql = "insert into {$tablepre}msgs(rid,uid,tuid,uname,tname,p,style,msg,mtime,ugid,msgid,ip,state,address) values('$rid','$muid','$tid','$uname','$tname','$privacy','$style','$msg'," . gdate() . ",'$gid','$msgid','$onlineip','$state','$address')";
        $db->query($sql);
        break;
    case "regcheck":
        $guestexp = '^Guest|' . $cfg['config']['regban'] . "Guest";
        if (preg_match("/\s+|{$guestexp}/is", $username))
            exit('-1');

        if ($db->num_rows($db->query("select * from {$tablepre}members where username='$username' ")) > 0)
            exit('0');
        else
            exit('1');
        break;
    case "setvideo":
        $uid = $_SESSION['login_uid'];
        if (check_auth('room_admin')) {
            $db->query("update {$tablepre}config set defvideo='{$vid}' where id='{$def_cfg}'");
        }
        break;
    case "userstate":
        if (isset($_SESSION['login_uid'])) {
            $userstate['state'] = "login";
            $id = $_SESSION['login_uid'];
            $query = $db->query("select m.uid,m.sex,m.onlinetime,m.gold,ms.nickname,ms.mood,ms.city,ms.bday
						  from {$tablepre}members m,{$tablepre}memberfields ms
						  where m.uid=ms.uid and m.uid='{$id}'
						  ");
            $row = $db->fetch_row($query);
            $userinfo['id'] = $row['uid'];
            $userinfo['nick'] = $row['nickname'];
            $userinfo['sn'] = $row['mood'];
            $userinfo['rank'] = showstars($row['onlinetime']);
            $userinfo['gold'] = $goldname . ':' . $row['gold'];
            $userstate['info'] = $userinfo;
        } else {
            $userstate['state'] = "logout";
        }
        $data = $json->encode($userstate);
        exit($data);
        break;
    case "userinfo":
        $query = $db->query("select m.*,ms.*
						  from {$tablepre}members m,{$tablepre}memberfields ms
						  where m.uid=ms.uid and m.uid='{$id}'
						  ");
        $row = $db->fetch_row($query);
        $row['password'] = '';
        $data = $json->encode($row);
        exit($data);
        break;
    case "delimpression":
        if (!isset($_SESSION['login_uid']) || $_SESSION['login_uid'] == 0)
            $state['state'] = 'logout';
        else {
            $uid = $_SESSION['login_uid'];
            $db->query("delete from {$tablepre}membersapp1 where uid='$uid' and fuid='$fuid' and ftime='$ftime'");
            $state['state'] = 'ok';
        }
        $data = $json->encode($state);
        exit($data);
        break;
    case "impression":
        if (!isset($_SESSION['login_uid']) || $_SESSION['login_uid'] == 0)
            $state['state'] = 'logout';
        else {
            $color = rand_color();
            $time = gdate();
            $fuid = $_SESSION['login_uid'];
            $db->query("delete from {$tablepre}membersapp1 where uid='$uid' and fuid='$fuid'");
            $sql = "insert into {$tablepre}membersapp1(uid,color,txt,fuid,ftime)
				  values('$uid','$color','$t','$fuid','$time')";
            $db->query($sql);
            $state['state'] = 'ok';
        }
        $data = $json->encode($state);
        exit($data);
        break;
    case "memberfriends":
        if (!isset($_SESSION['login_uid']))
            $state['state'] = 'logout';
        else {
            $ftime = gdate();
            $uid = $_SESSION['login_uid'];
            if (isset($a))
                $db->query("replace into {$tablepre}membersapp3(uid,fuid,ftime)values('$uid','$a','$ftime')");
            if (isset($d))
                $db->query("delete from {$tablepre}membersapp3 where uid='$uid' and fuid='$d'");
            $state['state'] = 'ok';
        }
        $data = $json->encode($state);
        exit($data);
        break;
    case "message":
        if (!isset($_SESSION['login_uid']) || $_SESSION['login_uid'] == 0)
            $state['state'] = 'logout';
        else {
            if (isset($d)) {
                $db->query("delete from {$tablepre}membersapp4 where id='$d' and uid='$_SESSION[login_uid]'");
                $state['state'] = 'ok';
            } else {
                if (trim($txt) != '') {
                    $txt = $db->totxt($txt);
                    $ftime = gdate() - 2;
                    $fuid = $_SESSION['login_uid'];
                    $db->query("insert into {$tablepre}membersapp4(uid,fuid,ftime,tag,txt)values('$uid','$fuid','$ftime','$tag','$txt')");
                }
                $state['state'] = 'ok';
            }
        }
        $data = $json->encode($state);
        exit($data);

        break;
    case "kick":
        if (check_user_auth($aid, 'user_kick')) {
            $losttime = $ktime * 60 + gdate();
            $db->query("insert into {$tablepre}ban(username,ip,losttime,sn)values('$u','$onlineip','$losttime','$cause')");
            $state['state'] = 'yes';
            $data = $json->encode($state);
            exit($data);
        }

        break;
    case "kick_send":
        if (check_user_auth($aid, 'user_send')) {
            $db->query("insert into {$tablepre}send(username,ip)values('$u','$onlineip')");
            $state['state'] = 'yes';
            $data = $json->encode($state);
            exit($data);
        }
        break;
    case "sleepwalk":
        if (check_user_auth($_SESSION['login_uid'], 'user_sleepwalk')) {
			if(empty($suid)) {
				$state['state'] = 'no';
				$state['msg'] = '网络错误请重试';
				$data = $json->encode($state);
				exit($data);
			}
			try{
				if($gid == 0) {
					$row = $db->fetch_row($db->query("select state from {$tablepre}members where username='{$suid}'"));
					if($row['state'] == '3') {
						$state['code'] = 1;
						$db->query("update {$tablepre}members set state=1 where username='{$suid}'");
					} else {
						$state['code'] = 3;
						$db->query("update {$tablepre}members set state=3 where username='{$suid}'");
					}					
				} else {
					$row = $db->fetch_row($db->query("select state from {$tablepre}members where uid='{$suid}'"));
					if($row['state'] == '3') {
						$state['code'] = 1;
						$db->query("update {$tablepre}members set state=1 where uid='{$suid}'");
					} else {
						$state['code'] = 3;
						$db->query("update {$tablepre}members set state=3 where uid='{$suid}'");
					}					
				} 
				$state['state'] = 'yes';
			} catch(Exception $e) {
				$state['state'] = 'no';
				$state['msg'] = '设置失败';
			}
        } else {
			$state['state'] = 'no';
			$state['msg'] = '没有权限';
		}
		$data = $json->encode($state);
		exit($data);
        break;
    case "online":		
		if(empty($rid) || intval($rid) <= 0) {
			$state['state'] = 'reload';
			$data = $json->encode($state);
			exit($data);
		}
        if (!isset($_SESSION['login_uid'])) {
            $state['state'] = 'logout';
        } else {
            //if ($_SESSION['login_uid'] == 0) {
            //    $state['state'] = 'ok';
            //    $data = $json->encode($state);
            //    exit($data);
            //}
            if (!empty($rst)) {
                $time = gdate();
                $u_id = $_SESSION['login_uid'];
                $query_row = $db->fetch_row($db->query("select lastactivity from {$tablepre}members where uid='$u_id'"));
                $_time = 3;//(int) ($time - $query_row['lastactivity']);
				//$online_time_query = $db->query("select * from {$tablepre}onlinetime where rid='{$rid}' and lastactivityip='{$onlineip}';");
				$online_time_query = $db->query("select onlinetime from {$tablepre}members where uid='{$u_id}';");
				if($db->num_rows($online_time_query) <= 0) {
					$user_onlinetime = 0;
				} else {
					$onlinetime = $db->fetch_row($online_time_query);
					$user_onlinetime = $onlinetime['onlinetime'];
				}

                if($_SESSION['login_gid'] == 0 && $cfg['config']['state'] == '3' && $user_onlinetime > ($cfg['config']['limit'] * 60)) {
                    $state['state'] = 'timeout';
                } else {
                    $db->query("update {$tablepre}members set lastactivity='{$time}',onlinetime=onlinetime+$_time where uid='$u_id'");
                    $state['state'] = 'ok';
                    $onlineNum = (int) $num;
                    if ($num >= 1) {
                        $db->query("update {$tablepre}config set online='$num' where id='$rid'");
                    }
                }
            } else {
                reonline();
                $state['state'] = 'ok';
            }
        }
		$hangqing = '';//file_get_contents('http://115.29.249.68/WebService/ForeignProductPriceService.ashx?Method=GetForeignProductPrice&Encypt=0a4df49f380e019b734099ace31c5ea4&_=' . time());
		$state['data'] = json_decode($hangqing, TRUE);
		
		$query=$db->query("select * from {$tablepre}notice where id='21'");
		$row=$db->fetch_row($query);
		$state['ssbs'] = array('txt' => strip_tags(tohtml($row['txt'])), 'url' => $row['url']);
		
        $data = $json->encode($state);
        exit($data);
        break;
    case "online_20170530del":		
		if(empty($rid) || intval($rid) <= 0) {
			$state['state'] = 'reload';
			$data = $json->encode($state);
			exit($data);
		}
        if (!isset($_SESSION['login_uid'])) {
            $state['state'] = 'logout';
        } else {
            //if ($_SESSION['login_uid'] == 0) {
            //    $state['state'] = 'ok';
            //    $data = $json->encode($state);
            //    exit($data);
            //}
            if (!empty($rst)) {
                $time = gdate();
                $u_id = $_SESSION['login_uid'];
                $query_row = $db->fetch_row($db->query("select lastactivity from {$tablepre}members where uid='$u_id'"));
                $_time = 3;//(int) ($time - $query_row['lastactivity']);
				//$online_time_query = $db->query("select * from {$tablepre}onlinetime where rid='{$rid}' and lastactivityip='{$onlineip}';");
				$online_time_query = $db->query("select * from {$tablepre}onlinetime where uid='{$u_id}';");
				if($db->num_rows($online_time_query) <= 0) {
					$user_onlinetime = 0;
				} else {
					$onlinetime = $db->fetch_row($online_time_query);
					$user_onlinetime = $onlinetime['onlinetime'];
				}

                if($_SESSION['login_gid'] == 0 && $cfg['config']['state'] == '3' && $user_onlinetime > ($cfg['config']['limit'] * 60)) {
                    $state['state'] = 'timeout';
                } else {
                
                    /*$rid = $_SESSION['onlines_state']['rid'];
                    $query = $db->query("select * from {$tablepre}memberonlines where rst='$rst' and uid='$u_id'");
                    if ($db->num_rows($query) <= 0) {

                        $db->query("replace into {$tablepre}memberonlines(uid,rid,lastactivity,ip,rst)values('$u_id','$rid','$time','$onlineip',$rst)");
                        $state['state'] = 'ologin';
                        $data = $json->encode($state);
                        exit($data);
                    }
                    $db->query("update {$tablepre}memberonlines set lastactivity='$time' where rst='$rst' and uid='$u_id'");*/
                    $db->query("update {$tablepre}members set lastactivity='{$time}' where uid='$u_id'");
					
					if($db->num_rows($online_time_query) <= 0) {
						$db->query("insert into {$tablepre}onlinetime set rid='{$rid}',uid='{$u_id}',onlinetime='{$_time}',lastactivityip='{$onlineip}';");
					} else {
						//$db->query("update {$tablepre}onlinetime set onlinetime=onlinetime+$_time where rid='{$rid}' and lastactivityip='{$onlineip}';");
						//$db->query("update {$tablepre}onlinetime set onlinetime=onlinetime+$_time where rid='{$rid}' and uid='{$u_id}';");
						$db->query("update {$tablepre}onlinetime set onlinetime=onlinetime+$_time where uid='{$u_id}';");
					}
                    $state['state'] = 'ok';
                    $onlineNum = (int) $num;
                    if ($num >= 1) {
                        $db->query("update {$tablepre}config set online='$num' where id='$rid'");
                    }
                }
            } else {
                reonline();
                $state['state'] = 'ok';
            }
        }
		$hangqing = file_get_contents('http://115.29.249.68/WebService/ForeignProductPriceService.ashx?Method=GetForeignProductPrice&Encypt=0a4df49f380e019b734099ace31c5ea4&_=' . time());
		$state['data'] = json_decode($hangqing, TRUE);
        $data = $json->encode($state);
        exit($data);
        break;
    case "send_redbag":
        $uid = $_SESSION['login_uid'];
		$username = $_SESSION['login_user'];
        $query = $db->query("select gold from {$tablepre}members where uid='{$uid}'");
        $row = $db->fetch_row($query);
		if(intval($total) < 1 || $num < 1) {
			$state['state'] = '-3';
		} elseif($_SESSION['login_gid'] == '0') {
			$state['state'] = '-2';
		} elseif(intval($row['gold']) < intval($total)) {
			$state['state'] = '-1';
		} else {
			$gold = $total;
			$db->query("insert into {$tablepre}redbag(`uid`, `nickname`, `money`, `count`, `type`, `ctime`, `info`) values('$uid', '$username', '$total', '$num', 'redbag', '" . time() . "', '$info')");//insert redbag
			$rbid = $db->insert_id();
			$min = 0.01;//每个人最少能收到0.01元
			for ($i=1;$i<$num;$i++) {
				$safe_total=($total-($num-$i)*$min)/($num-$i);//随机安全上限
				$money=mt_rand($min*100,$safe_total*100)/100;
				$total=$total-$money;
				$db->query("insert into {$tablepre}redbag_info(`rbid`, `money`, `state`) values('$rbid', '$money', '0')");//insert redbag_info
			}
			$db->query("insert into {$tablepre}redbag_info(`rbid`, `money`, `state`) values('$rbid', '$total', '0')");//insert redbag_info
			
			$db->query("update {$tablepre}members set gold = gold-$gold where uid='$uid'");//update user gold
			
			$state['state'] = 'ok';
			$state['rbid'] = $rbid;
		}
        $data = $json->encode($state);
        exit($data);
        break;
    case "get_redbag":
        $uid = $_SESSION['login_uid'];
		$username = $_SESSION['login_user'];
		
        $query = $db->query("select id from {$tablepre}redbag_info where uid='{$uid}' and rbid = '" . $rbid . "'");
        $row = $db->fetch_row($query);
		
		if(!empty($row)) {
			$state['state'] = '-4';
		} elseif(intval($rbid) < 0) {
			$state['state'] = '-3';
		} elseif($_SESSION['login_gid'] == '0') {
			$state['state'] = '-2';
		} elseif(empty($uid) || $uid == '0') {
			$state['state'] = '-5';
		} else {
			$query = $db->query("select id,money from {$tablepre}redbag_info where rbid='{$rbid}' and state = 0");
			while($row = $db->fetch_row($query)) {
				$redbags[] = $row;
			}
			
			if(empty($redbags) || count($redbags) < 1) {
				$state['state'] = '-1';
			} else {
			
				$rand_id = mt_rand(0, count($redbags) - 1);
				
				$db->query("update {$tablepre}redbag_info set `uid` = '$uid', `nickname` = '$username', `state` = '1', `rtime` = '" . time() . "' where id='{$redbags[$rand_id]['id']}'");
				
				$db->query("update {$tablepre}members set gold = gold+".floatval($redbags[$rand_id]['money'])." where uid='$uid'");//update user gold	
				
				$state['state'] = 'ok';
				$state['money'] = $redbags[$rand_id]['money'];
			}
		}
        $data = $json->encode($state);
        exit($data);
        break;
    case "get_redbag_info":
        $uid = $_SESSION['login_uid'];
		$username = $_SESSION['login_user'];
		$received = 0;
		
        $query = $db->query("select * from {$tablepre}redbag where id='{$rbid}'");
        $redbag = $db->fetch_row($query);
		
		$query = $db->query("select * from {$tablepre}redbag_info where rbid='{$rbid}' and state = '1'");
		while($row = $db->fetch_row($query)) {
			$row['rrtime'] = date('H:i:s', $row['rtime']);
			$redbag_infos[] = $row;
			if($row['state'] == '1') {
				$received += 1;
			}
		}
		
		if(!empty($redbag) && !empty($redbag_infos)) {
			$state['state'] = 'ok';
			
			$redbag['received'] = $received;
			$rd['redbag'] = $redbag;
			$rd['redbag_info'] = $redbag_infos;
			$state['rb'] = $rd;
		} else {
			$state['state'] = '';
		}
		//print_r($state);exit;
        $data = $json->encode($state);
        exit($data);
        break;
    case "sendflower":
        $uid = $_SESSION['login_uid'];
		$username = $_SESSION['login_user'];
        $query = $db->query("select gold from {$tablepre}members where uid='{$uid}'");
        $row = $db->fetch_row($query);
		if($_SESSION['login_gid'] == '0') {
			$state['state'] = '-2';
		} elseif(intval($row['gold']) < 1) {
			$state['state'] = '-1';
		} else {
			$gold = $total;
			$db->query("insert into {$tablepre}redbag(`uid`, `nickname`, `money`, `count`, `type`, `touid`, `ctime`) values('$uid', '$username', '1', '1', 'flower', '$touid', '" . time() . "')");//insert redbag
			
			$db->query("update {$tablepre}members set gold = gold-1 where uid='$uid'");//update user gold
			
			$state['state'] = 'ok';
		}
        $data = $json->encode($state);
        exit($data);
        break;
	case 'addmobile':
		if(!preg_match('/1[34758]{1}\d{9}$/i', $mobile) || $mobile != $_SESSION['phone']) {
			exit('invalidatemobile');
		}
		if($_SESSION['mcode'] != $validate) {
			exit('invalidatecode');
		}
		$query = $db->query("select state from {$tablepre}lottery where phone='{$mobile}'");
        $row = $db->fetch_row($query);
		if($row['state'] == '1') {
			exit('hasmobile');
		}
		$_SESSION['phone_validate'] = TRUE;
		exit('success');
		break;
	case 'rotate':
		$ret = array();
		$ret['code'] = -1;
		$uid = $_SESSION['login_uid'];
		$gid = $_SESSION['login_gid'];
		if($gid == '0') {
			$ret['msg'] = '请先登录';
			exit(json_encode($ret));
		}
		
		$query = $db->query("select * from {$tablepre}lottery where uid='{$uid}' and cdate='" . date('ymd') . "'");
        $row = $db->num_rows($query);
		if($row >= 3) {
			$ret['msg'] = '3次机会已用完';
			exit(json_encode($ret));
		}
	
		//奖项初始化
		$query = $db->query("select num as id,prize,prob as v,state from {$tablepre}prize order by id asc;");
		$prize_arr = array();
		while($row = $db->fetch_row($query)) {
			array_push($prize_arr, $row);
		}
		/*$prize_arr = array(
				'0' => array('id' => 1,'min' => 1,'max' => 29,'prize' => '特伦舒牛奶一箱','v' => 10),
				'1' => array('id' => 2,'min' => 302,'max' => 328,'prize' => '自行车一辆','v' => 2),
				'2' => array('id' => 3,'min' => 242,'max' => 268,'prize' => 'ipaid Aair2','v' => 1),
				'3' => array('id' => 4,'min' => 182,'max' => 208,'prize' => '再接再厉','v' => 1000),
				'4' => array('id' => 5,'min' => 122,'max' => 148,'prize' => '精品棉袜','v' => 90),
				'5' => array('id' => 6,'min' => 62,'max' => 88,'prize' => '电饭煲','v' => 25),
				'6' => array('id' => 7,'min' => 62,'max' => 88,'prize' => '洗衣液','v' => 50),
				'7' => array('id' => 8,'min' => 62,'max' => 88,'prize' => '再接再厉','v' => 1000),
				'8' => array('id' => 9,'min' => 62,'max' => 88,'prize' => 'iphone 6s','v' => 0),
				'9' => array('id' => 10,'min' => 62,'max' => 88,'prize' => '再接再厉','v' => 1000),
		);*/

		//抽奖开始
		foreach ($prize_arr as $key => $val) {
			$arr[$val['id']] = $val['v'];
		}

		$rid = getRand($arr); //根据概率获取奖项id
		
		$sql = "insert into {$tablepre}lottery(`uid`, `prizeid`, `prize`, `state`, `ctime`, `cdate`) values('".$uid."', '".$prize_arr[$rid-1]['id']."', '".$prize_arr[$rid-1]['prize']."', '".($prize_arr[$rid-1]['state'])."','" . time() . "', '" . date('ymd') . "')";
		$db->query($sql);//insert redbag
		
		$ret['code'] = $rid;
		$ret['prize'] = $prize_arr[$rid-1]['state'] . '_+_' . $prize_arr[$rid-1]['prize'];
//print_r($ret);exit;
		exit(json_encode($ret));
		break;
		
	case 'check_vcode':
		if(strtolower($vcode) == strtolower($_SESSION["vcode"])) {
			exit(json_encode(1));
		} else {
			exit(json_encode(-1));
			//$_SESSION['checkVcode'] = false;
		}
		break;
		
	case 'signin':
		$ret['code'] = 1;
		if($_SESSION['login_gid'] == '0') {
			$ret['code'] = '-1';
			$ret['msg'] = '请先登录';
		}
		$uid = $_SESSION['login_uid'];
		$username = $_SESSION['login_user'];
		$signdate = date('Ymd');
		
		$query = $db->query("select signdate from {$tablepre}signin where uid='{$uid}' and signdate='{$signdate}'");
        $row = $db->fetch_row($query);
		if(!empty($row)) {
			$ret['code'] = '-1';
			$ret['msg'] = '您今天已签到';
		}
		
		if($ret['code'] == 1) {
			$sql = "insert into {$tablepre}signin(`uid`, `uname`, `signtime`, `signdate`) values('".$uid."','".$username."', '" . time() . "', '". $signdate ."')";
			$db->query($sql);
		}
		
		exit(json_encode($ret));
		break;
	case 'load_msg':
		//用户组
		$query=$db->query("select * from {$tablepre}auth_group order by ov desc");
		while($row=$db->fetch_row($query)){
			$group["m".$row[id]] = $row;
		}
		$query=$db->query("select * from {$tablepre}msgs where rid='".$rid."' and p='false' and state!='1' and `type`='0' and id<'".$lastmsgid."' order by id desc limit 0,20 ");
		//echo "select * from {$tablepre}msgs where rid='".$rid."' and p='false' and state!='1' and `type`='0' and id<'".$lastmsgid."' order by id desc limit 0,20 ";exit;
		$omsg = '';
		while($row=$db->fetch_row($query)){
			$row['msg']=str_replace(array('&amp;', '','&quot;', '&lt;', '&gt;'), array('&', "\'",'"', '<', '>'),$row['msg']);
			if($row[tuid]!="ALL"){
				$omsg = "<div class='msg' id='{$row[msgid]}'><font class='date'>".date('H:i',$row[mtime])."</font><div class='msg_head'><img title='".$group["m".$row[ugid]][title]."-".$group["m".$row[ugid]][sn]."' class='msg_group_ico' src='".$group["m".$row[ugid]][ico]."'></div><div class='msg_content'><div><font class='u' onclick='\"{$row[uid]}\",\"{$row[uname]}\")'>{$row[uname]}</font>&nbsp;&nbsp; <font class='dui'>    对</font> <font class='u' onclick='ToUser.set(\"{$row[tuid]}\",\"{$row[tname]}\")'>{$row[tname]}</font> </div></div><div class='layim_chatsay1'><font style='{$row[style]};'>{$row[msg]}</font></div></div><div style='clear:both;'></div>".$omsg;
			}elseif(preg_match('/^<img onclick=\"getRedbag\(this\)\" style=\"width:186px;float:left;cursor:pointer\" src="images\/redbag_open.png\" rel=\"[0-9]{0,12}\">$/i',$row[msg])) {
				$omsg = "<div class='msg' id='{$row[msgid]}'><font class='date'>".date('H:i',$row[mtime])."</font><div class='msg_head'><img src='".$group["m".$row[ugid]][ico]."' class='msg_group_ico' title='".$group["m".$row[ugid]][title]."-".$group["m".$row[ugid]][sn]."'></div><div class='msg_content'><div><font class='u'  onclick='ToUser.set(\"{$row[uid]}\",\"{$row[uname]}\")'>{$row[uname]}</font>&nbsp;&nbsp; </div></div><div class='layim_chatsay1' style='margin:5px 0px;padding: 0;background:#f00 none repeat scroll 0 0;'><font style='{$row[style]};'>{$row[msg]}</font></div></div><div style='clear:both;'></div>".$omsg;
			} else {
				$omsg = "<div class='msg' id='{$row[msgid]}'><font class='date'>".date('H:i',$row[mtime])."</font><div class='msg_head'><img src='".$group["m".$row[ugid]][ico]."' class='msg_group_ico' title='".$group["m".$row[ugid]][title]."-".$group["m".$row[ugid]][sn]."'></div><div class='msg_content'><div><font class='u' onclick='ToUser.set(\"{$row[uid]}\",\"{$row[uname]}\")'>{$row[uname]}</font>&nbsp;&nbsp; </div></div><div class='layim_chatsay1'><font style='{$row[style]};'>{$row[msg]}</font></div></div><div style='clear:both;'></div>" . $omsg;
			}
			$lastmsgid = $row['id'];			
		}
		$ret['msgs'] = '<div class="load_msg"><a class="" onclick="chatload()" href="javascript:void(0)">查看更多消息</a></div>' . $omsg;
		$ret['lastmsgid'] = $lastmsgid;
		exit(json_encode($ret));
		break;
	case 'auto_speak':
		$ret = '';
		$time = time();//1472030271;//
		$mintime = intval($time) - 5;
		$maxtime = intval($time);
        $query = $db->query("select * from {$tablepre}rebots where rid='$rid' and losttime>{$time}");
        if ($db->num_rows($query) > 0) {
			$robotmsg = $db->fetch_row($db->query("select * from {$tablepre}robotmsg where rid='$rid' and sendtime>={$mintime} and sendtime<{$maxtime}"));
			if(!empty($robotmsg)) {
				$robotmsg['msg'] = str_replace(array('&amp;', '','&quot;', '&lt;', '&gt;'), array('&', "\'",'"', '<', '>'), $robotmsg['msg']);
				//echo $robotmsg['senduser'];exit;
				$robotmsg['senduser'] = str_replace(array('&amp;', '', '&quot;', '&lt;', '&gt;'), array('&', "\'",'"', '<', '>'), $robotmsg['senduser']);
				if($robotmsg['state'] != '1') {
					$row = $db->fetch_row($query);
					$rlist = base64_decode($row['rebots']);
					$rlist = json_decode($rlist, TRUE);
					$randid = rand(0, count($rlist['roomListUser']) - 1);
					$ret['user'] = $user = $rlist['roomListUser'][$randid];
				} else {
					$user = json_decode($robotmsg['senduser'], TRUE);
					$ret['user'] = $user;
				}
				//	print_r($ret);
				$ret['msg'] = $robotmsg['msg'];
				$ret['msgdate'] = date('H:i:s', $robotmsg['sendtime']);
				//echo "::".json_encode($ret[user]);exit;
				if($robotmsg['state'] != '1') {
					$sql = "insert into {$tablepre}msgs(rid,uid,tuid,uname,tname,p,style,msg,mtime,ugid,msgid,ip,state)
					  values('$rid','{$user[chatid]}','ALL','{$user[nick]}','大家','false','','{$ret[msg]}','" . $robotmsg['sendtime'] . "','{$user[color]}','','{$user[ip]}','0')";
					$db->query($sql);
					$db->query("update {$tablepre}robotmsg set senduser='".(str_replace('\u', '\\\\u', json_encode($ret[user])))."',state='1' where id='{$robotmsg[id]}';");
				}
				//EXIT;
				exit(json_encode($ret));
			}
		}
		$query = $db->query("select * from {$tablepre}cpjh where state = 0 limit 1");
		$row = $db->fetch_row($query);
		if(!empty($row)){
			$ret['cpjh'] = 1;
			$ret['msg'] = tohtml($row['data']);
			$db->query("update {$tablepre}cpjh set state = 1 where id = {$row['id']}");
		}
		
		exit(json_encode($ret));
		break;
    case "login":
        $ret = -1;
        $msg = user_login($username, $password);
        if($msg === true) {
            $ret = 'success';
        } else {
            $ret = $msg;
        }
        exit($ret);
        break;
    case "reg":
        $ret['Error'] = '';
        $guestexp = '^Guest|'.$cfg['config']['regban']."Guest";
        if(preg_match("/\s+|{$guestexp}/is", $u)) {
            $ret['Error'] = '用户名禁用！';
            exit(json_encode($ret));
        }

        $query=$db->query("select uid from {$tablepre}members where username='{$u}' limit 1");
        if($db->num_rows($query)) {
            $ret['Error'] = "用户名已经被使用!换一个，如{$u}1985";
            exit(json_encode($ret));
        }

        $regtime = gdate();
        $p = md5($p);
        if(isset($_COOKIE['tg'])) {
            $tuser = $_COOKIE['tg'];
        } else {
            $tuser = rand_kefu();
        }
        if($cfg['config']['regaudit']=='1') {
            $state='0';
        } else {
            $state='1';
        }
        $db->query("insert into {$tablepre}members(username,password,sex,email,regdate,regip,lastvisit,lastactivity,gold,realname,gid,phone,fuser,tuser,state) values('$u','$p','2','$email','$regtime','$onlineip','$regtime','$regtime','0','$qq','1','$phone','$tuser','$tuser','$state')");
        $uid=$db->insert_id();
        $db->query("replace into {$tablepre}memberfields (uid,nickname) values('$uid','$nick')");
        $db->query("insert into {$tablepre}msgs(rid,ugid,uid,uname,tuid,tname,mtime,ip,msg,type) values('{$cfg[config][id]}','1','{$uid}','{$u}','{$cfg[config][defvideo]}','{$cfg[config][defvideonick]}','".gdate()."','{$onlineip}','用户注册','2')");

        $msg = user_login($u,$p2);
        if($msg===true) {
            $ret['Info'] = '注册成功！';
            exit(json_encode($ret));
        } else {
            $ret['Info'] = '注册成功！';//$msg;
            exit(json_encode($ret));
        }
        break;
	case 'editface':
		copy('face/p1/' . $face . '.gif', 'face/p1/' . $_SESSION[login_uid] . '.gif');
		exit('ok');
		break;
	case 'getZhiniuVid':
		/**
		调用方法:
		<video id="live" src="" controls="controls" autoplay width="100%" height="215" style="display: block">您的浏览器不支持 video 标签。</video>
		<script>
			$.get('/ajax.php?act=getZhiniuVid&videoUrl=http://www.zhiniu8.com/tougu/1341388379',function(ret) {
				$('#live').attr('src', ret);
			});
		</script>
		**/
		$url = file_get_contents("http://115.159.123.69/open/index.php?url=" . $videoUrl);
		exit($url);
		break;
	case 'closeLive':
		if(empty($roomid)) {
			exit(json_encode('-1'));
		}
		$row = $db->fetch_row($db->query("select state from {$tablepre}config where `id`='{$roomid}'"));
		if($row['state'] == '0') {
			exit(json_encode('0'));
		}
		$db->query("update {$tablepre}config set `state`='0' where `id`='{$roomid}'");
		if($db->affected_rows() > 0) {
			exit(json_encode('ok'));
		}
		exit(json_encode('-1'));
		break;
	case "sendgift":
		$gname=$db->totxt($gname);
		$msg=$db->totxt($msg);
		$v=sendgift($num,$gid,$sid,$msg);
		exit($v);
		break;
	case "setRemark":
		if(empty($muid)) {
			exit('muid is empty');
		}
		if (check_auth('room_admin')) {
			if(empty($mname)) {
				$db->query("delete from {$tablepre}member_remark where uid='{$myuid}' and muid='{$muid}'");
				exit('ok');
			}
			$row = $db->fetch_row($db->query("select * from {$tablepre}member_remark where uid='{$myuid}' and muid='{$muid}'"));
			if(empty($row)) {
				$db->query("insert into {$tablepre}member_remark set `uid`='{$myuid}',`muid`='{$muid}',`mname`='{$mname}'");
			} else {
				$db->query("update {$tablepre}member_remark set `muid`='{$muid}',`mname`='{$mname}' where `id`='{$row[id]}'");
			}
			exit('ok');
		} else {
			exit('no permission');
		}
		break;
	case "wohongbao":
		$ret = array();
		if($_SESSION['login_gid'] == '0') {
			$ret['code'] = '-1';
			$ret['msg'] = '请先登录';
			exit(json_encode($ret));
		}
		$uid = $_SESSION['login_uid'];
		$query = $db->query("select ri.money,ri.rtime,r.nickname,r.uid from {$tablepre}redbag_info as ri left join {$tablepre}redbag as r on ri.rbid = r.id where ri.uid='{$uid}' order by ri.id desc");
		$ret['code'] = '0';
		$data = array();
		$ret['count'] = 0;
		$ret['money'] = 0;
		while($row = $db->fetch_row($query)){
			$data[] = array('nickname' => $row['nickname'], 'money' => $row['money'], 'uid' => $row['uid'], 'time' => date('Y-m-d H:i:s', $row['rtime']));
			$ret['money'] += $row['money'];
			$ret['count'] += 1;
		}
		$ret['data'] = $data;
		exit(json_encode($ret));
	break;

}

?>