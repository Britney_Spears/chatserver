<?php
require_once '../include/common.inc.php';
$query=$db->query("select id,title,logo,state from {$tablepre}config where state != '0' order by id asc");
while($row = $db->fetch_row($query)) {
    $rooms[] = $row;
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<script src="../room/script/jquery.min.js"></script>
<title>房间列表</title>
<style type="text/css">
/* CSS Document */
body {font: normal 11px auto "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif; color: #4f6b72;background-color: #c2ddf3; }
a {color: #FFF; text-decoration: none;}
.list {float: left; }
.li {width: 228px; height:105px; float: left; margin: 5px 5px 0 0; padding: 5px; border: 1px solid #aec5d8; background-color: #fff;}
.li_img img{width: 221px; height: 54px; border: 1px #CCCCCC solid; padding: 1px; }
.li_qq {height: 26px; text-align: center; display: block; margin: 0 auto; color: #fff; font-size: 14px; margin-top: 3px; line-height: 26px; overflow:hidden;}
.li_qq a{background-color: #2A96E7; padding: 3px 6px;}
.li_qq:hover{opacity: 0.9;}
.li_phone {background: url(img/pop_btn2.png) no-repeat 1px 3px; display: block; height: 22px; line-height: 22px; padding-left: 12px; margin: 0 auto; overflow:hidden; color:#F00}
</style>
</head>

<body>
<div class='list'>
    <?php foreach($rooms as $room) { ?>
        <div class='li' id='<?php echo $room['id']; ?>'>
            <div class='li_img'>
                <img src='<?php echo $room['logo']; ?>'>
            </div>
            <div class='li_qq'>
                <a href='javascript:void(0);' onclick='top.location.href="/room/index.php?rid=<?php echo $room['id']; ?>"' title='<?php echo $room['title']; ?>'><?php echo $room['title']; ?></a>
            </div>
        </div>
    <?php } ?>
</div>
</body>
</html>