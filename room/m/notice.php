<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>温馨提示</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
<link rel="shortcut icon" type="image/x-icon" href="<?=$cfg['config']['ico']?>" />
<meta content="<?=$cfg['config']['keys']?>" name="keywords">
<meta content="<?=$cfg['config']['dc']?>" name="description">
<link href="images/base.css" rel="stylesheet" type="text/css"  />
<link href="images/login.css" rel="stylesheet" type="text/css"  />
<script src="room/script/jquery.min.js"></script>
<script src="room/script/layer.js"></script>
<script src="room/script/device.min.js"></script>
<script>
function setCookie_t(name,value,time) {
    var strsec = getsec_t(time);
    var exp = new Date();
    exp.setTime(exp.getTime() + strsec*1);
    document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
}
function getsec_t(str) {
//   alert(str);
   var str1=str.substring(1,str.length)*1;
   var str2=str.substring(0,1);
   if (str2 == "s") {
        return str1*1000;
   } else if (str2 == "h") {
       return str1*60*60*1000;
   } else if (str2 == "d") {
       return str1*24*60*60*1000;
   }
}
</script>
</head>

<body>
<!--<script>if (device.mobile()){window.location = './room/minilogin.php';}</script>-->

<div id="error" class="index" style="">
	<?php if(@$_GET['referer'] != 'login') { ?>
	<!--<div style="width: 150px; height: 80px; margin-left: 50%; margin-top: 50%; position: relative; left: -175px; top: -70px; background-color: #CCC; padding: 30px 100px; color: #FF7F50;">-->
	<div style="width: 150px; height: 80px; background-color: #CCC; padding: 30px 100px; color: #FF7F50; margin: 200px auto; line-height: 25px;">
		温馨提示：<br/>
		今日观看时间已到<br/>
		[<a href="logging.php" style="color: #FF7F50;">点击这里登录</a>]
		<script>
			setTimeout("timeUpdate()", 1000);
			function timeUpdate() {
				window.location.href = '/room/m/logging.php';
			}
		</script>
	</div>
	<?php } else { ?>		
	<script>
		setTimeout("timeUpdate()", 0);
		setCookie_t('times', '0', 's100');
		function timeUpdate() {
			window.location.href = '/room/m';
		}
	</script>
	<?php } ?>
</div>

</body>
</html>