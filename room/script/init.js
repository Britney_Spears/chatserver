//var e=getEvent();
//var jq = jQuery.noConflict();
//window.onbeforeunload=function(){ws.send("Logout");}
Number.prototype.Left = function(strlen){
	if(this.toString().length >= strlen) {
		return this.toString().substr(0, strlen);
	} else {
		var str = this.toString();
		for(var i=0; i < strlen - str.length; i++)
			str = "0" + str;
		return str;
	}
}
// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
// 例子： 
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "H+": this.getHours(), //小时 
        "i+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
String.prototype.str_replace = function(t) {
    var str = this;
    var reg = new RegExp(msg_unallowable, "ig");
    var pic_reg = new RegExp('<img([^>]*)>', "ig");
    if (reg.test(str) && !check_auth("room_admin") && !pic_reg.test(str))
        return "含敏感关键字，内容被屏蔽";
    str = str.replace(/<\/?(?!br|img|font|p|span|\/font|\/p|\/span)[^>]*>/ig, '').replace(/\r?\n/ig, ' ').replace(/(&nbsp;)+/ig, " ").replace(/(=M=)+/ig, "").replace(/(|)+/ig, "").replace(/(SendMsg)/ig, '');
    if (!check_auth("room_admin"))
        str = str.replace(reg, '**')
    return str;
}
function getId(id) {
    return document.getElementById(id);
}
function Datetime(tag) {
    return new Date().toTimeString().split(' ')[tag];
}
function SetChatValue(Variables, value) {
    ChatValue[Variables] = value;
}
function GetChatValue(Variables) {
    return ChatValue[Variables];
}

var ChatValue = new Array(10);
function SwfloadCompleted(tag) {
    if (tag == 'Video_main') {
        VideoLoaded = true;
        if (RoomInfo.type == '0') {
            getId('RoomMV').style.display = 'none';
        } else {
            if (RoomInfo.AutoPublicVideo == '1') {
                if (RoomInfo.PVideo == My.chatid)
                    thisMovie('pVideo').sConnect(RoomInfo.VServer, My.rid + '·' + My.chatid, '1');
                else
                    thisMovie('pVideo').pConnect(RoomInfo.VServer, My.rid + '·' + RoomInfo.PVideo, RoomInfo.PVideoNick);
            }
            if (RoomInfo.AutoSelfVideo == '1') {
                if (RoomInfo.PVideo == My.chatid)
                    thisMovie('pVideo').sConnect(RoomInfo.VServer, My.rid + '·' + My.chatid, '1');
                else
                    thisMovie('pVideo').sConnect(RoomInfo.VServer, My.rid + '·' + My.chatid, '2');
            }
        }
    }
}
function showLive() {
    //if (RoomInfo.OtherVideoAutoPlayer == '0') {
    //    location.reload();
    //}
    $('#OnLine_MV').html($('#OnLine_MV').html());
}
function showVideo() {
	if (RoomInfo.OtherVideoAutoPlayer == "0") {
        $('#OnLine_MV').html('<iframe height="100%" width="100%" id="video_box" allowTransparency="true" marginwidth="0" marginheight="0"  frameborder="0" scrolling="no" src="player.php?type=pc&rid='+My.rid+'"></iframe>');
    } else if (RoomInfo.OtherVideoAutoPlayer == "1") {
        /*var vFlash = new SWFObject("images/Video_main.swf?Q=" + RoomInfo.livefp, "pVideo", "520", "390", "9", "#FFF");
        vFlash.addParam("wmode", "transparent");
        vFlash.addParam("allowFullScreen", "true");
        vFlash.addParam("allowScriptAccess", "always");
        vFlash.write("OnLine_MV");*/
		/*var flashvars={
			f: RoomInfo.livefp, //'/upload/upfile/day_160518/video_1.flv',
			c: 0,
		};
		var params={bgcolor:'#FFF',allowFullScreen:true,allowScriptAccess:'always',wmode:'transparent'};
		var video=['http://movie.ks.js.cn/flv/other/1_0.mp4->video/mp4'];
		CKobject.embed('/ckplayer/ckplayer.swf', 'OnLine_MV', 'ckplayer_a1', '100%', '100%', true, flashvars, video, params);*/
		
		/*var flashvars={
			f: RoomInfo.livefp1,
			c: 0,
		};
		var params={bgcolor:'#FFF',allowFullScreen:true,allowScriptAccess:'always',wmode:'transparent'};
		var video=[RoomInfo.livefp1 + '->video/mp4'];
		CKobject.embed('/ckplayer/ckplayer.swf', 'OnLine_MV', 'ckplayer_a1', '100%', '100%', true, flashvars, video, params);
		
		$('#OnLine_MV').html('<iframe height="100%" width="100%" id="video_box" allowTransparency="true" marginwidth="0" marginheight="0"  frameborder="0" scrolling="no" src="'+RoomInfo.livefp1+'"></iframe>');*/
		$('#OnLine_MV').html('<embed src="'+RoomInfo.livefp1+'" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" wmode="opaque" width="100%" height="100%">');
    } else if (RoomInfo.OtherVideoAutoPlayer == "2") {
        $('#OnLine_MV').html("<img src='"+RoomInfo.livefp2+"' style='width: 100%; height: 100%;'>");
    }
	$('.liveYy').hide();
}
var socket;
var page_fire;
var timeid;
var RECONNECT_INTERNAL = 10000;
function OnSocket() {
	socket = io("ws://" + RoomInfo.TServer + ":" + RoomInfo.TSPort);
	//socket = io("http://" + RoomInfo.TServer + ":" + RoomInfo.TSPort + '/r' + My.rid);
	//socket = io('http://120.25.63.226:9000/r' + My.rid);
	//socket = io("ws://211.149.164.35:9000");
    socket.on('connect', function() {
		timeid && window.clearInterval(timeid);
        onConnect();
		//console.log('connecting');
    });
    socket.on('message', function(data) {
        //console.log(data);
        WriteMessage(data);
    });
	socket.on('close', function() {
		//console.log('server close');
		//setTimeout('location.reload()',3000);
		window.clearInterval(timeid);
        timeid = window.setInterval(init, RECONNECT_INTERNAL);
	});
	socket.on('error', function() {
		//console.log('server error');
		//setTimeout('location.reload()',3000);
		window.clearInterval(timeid);
        timeid = window.setInterval(init, RECONNECT_INTERNAL);
	});
	socket.on('disconnect', function() {
		//setTimeout('location.reload()',3000);
		setTimeout(init, RECONNECT_INTERNAL);
	});
}
function OnInit() {
    if ($.browser.msie && ($.browser.version == "6.0") && !$.support.style) {
        location.href = 'error.php?msg=' + encodeURIComponent('您使用的是不安全IE6.0浏览器,请升级到最新版本或<br>下载安装<a href=http://chrome.360.cn/ target=_blank>360浏览器</a>或<a href=http://www.baidu.com/s?wd=chrome target=_blank>Google浏览器</a>!');
        return false;
    }
    $.ajaxSetup({contentType: "application/x-www-form-urlencoded; charset=utf-8"});

    $("body").click(function() {
        MsgCAlert();
    });
    //auth
    if (check_auth("room_admin")) {
        $('#manage_div').show();
		$('#bt_quickReply').css({'display': 'inline-block'});
	}
    if (check_auth("rebots_msg")) {
		$('#chat_type').show();
	}
        
    if (check_auth("def_videosrc"))
        $('#bt_defvideosrc').show();
    
    OnSocket();
    
    //5分钟提示登录
    if (My.chatid.indexOf('x') > -1) {
		if(RoomInfo.loginTip == '1') {
			setInterval("loginTip()", 1000 * 60 * 5);
		}
		if(room_state == 3) {
			setInterval("viewTime()", 1000);
		}
	}	

    $('#Msg').html("连接中...");

    window.moveTo(0, 0);
    window.resizeTo(screen.availWidth, screen.availHeight);
    OnResize();
	
	if(My.chatid == 'x' || My.nick == '游客') {
		layer.msg('抱歉，您的账号过于简单，请刷新页面重新获取!', {shift: 6});
		return;
	}

    showVideo();

    interfaceInit();
    POPChat.Init();
    ToUser.set('ALL', '大家');
    dragMsgWinx(getId('drag_bar'));
	noticefram
	$('#noticefram').niceScroll({cursorcolor: "#FFF", cursorwidth: "2px", cursorborder: "0px;"});
    $('#MsgBox1').niceScroll({cursorcolor: "#FFF", cursorwidth: "2px", cursorborder: "0px;"});
    $('#MsgBox2').niceScroll({cursorcolor: "#FFF", cursorwidth: "2px", cursorborder: "0px;"});
    $('#OnLineUser_FindList').niceScroll({cursorcolor: "#FFF", cursorwidth: "2px", cursorborder: "0px;"});
    $('#OnLineUser_OhterList').niceScroll({cursorcolor: "#FFF", cursorwidth: "2px", cursorborder: "0px;"});
    $('#OnLineUser').niceScroll({cursorcolor: "#FFF", cursorwidth: "2px", cursorborder: "0px;"});
    $('#NoticeList').niceScroll({cursorcolor: "#FFF", cursorwidth: "2px", cursorborder: "0px;"});
    $("#Msg").keydown(function(e) {
        if (e.keyCode == 13) {
            ToUser.set($("#ToUser").val(), $("#ToUser").find("option:selected").text());
            SysSend.msg();
            HideMenu();
            MsgCAlert();
            return false
        }
    });
    $("#Send_bt").on("click", function() {
        ToUser.set($("#ToUser").val(), $("#ToUser").find("option:selected").text());
        HideMenu();
        MsgCAlert();
        SysSend.msg();
    });
    initFaceColobar();

    openWin(1, false, $("#tip_login_win").html(), 800, 350);
    //setTimeout("bt_SwitchListTab('Other')",1000)
    
	
	$("#navigation").click(function() {
		$("#navigation ul").toggle();
		return false;
	});
	$('body').click(function() {
		$("#navigation ul").hide();
	});
	$(".x_close").click(function() {
        $(".abundantface").hide();
		return false;
    });
	$(".GetFolowers").click(function() {
        $(".abundantface").toggle();
    });
	$(".broadcast").click(function () {
		$(".broadcastMain").slideToggle();
	});
	
	$('.closebroad').click(function() {
		$('.broadcastMain').hide();
	});
	
	$('.liveYy').click(function() {
		showVideo();
	});
	MsgAutoScroll();
	
	$('#MsgBox1').on('contextmenu', '.msg_content .u', function() {
		var temp_uid = $(this).attr('data-uid');
		console.log(temp_uid);
		var u = UserList.get(temp_uid);
		console.log(u);
		if(u) {
			//$(this).oncontextmenu = function() {
				UserList.menu(u);
				return false;
			//}
		} else {
			layer.msg("用户已离线！", {shift: 6});
		}
		return false;
	});
}
function choose_video(obj) {
	var video_url = $(obj).attr('data-url');
	/*var flashvars={
		f: video_url,
		c: 0,
	};
	var params={bgcolor:'#FFF',allowFullScreen:true,allowScriptAccess:'always',wmode:'transparent'};
	var video=[video_url + '->video/mp4'];
	CKobject.embed('/ckplayer/ckplayer.swf', 'OnLine_MV', 'ckplayer_a1', '100%', '100%', true, flashvars, video, params);*/
	$('#OnLine_MV').html('<embed src="'+video_url+'" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" wmode="opaque" width="100%" height="100%">');
	
	$('.liveYy').show();
}
function viewTime() {
	var view_time = viewtime;
    if(view_time < 0) {
        $('#colockbox').html('即将关闭');
        return;
    }
	var second = view_time % 60;
	var view_time = parseInt(view_time / 60);
	var minute = view_time % 60;
	var view_time = parseInt(view_time / 60);
	var hour = view_time % 24;
	var day = parseInt(view_time / 24);
	$('.second').html(second.Left(2));
	$('.minute').html(minute.Left(2));
	$('.hour').html(hour.Left(2));
	$('.day').html(day.Left(2));
	
    viewtime = viewtime - 1;
}
function OnResize() {
    var cw = $(window).width();
    var vw = cw - 980;
	var notice_bili = (267 / 1120);

    if (cw >= 1280) {
        if (cw >= 1280) {
            //vw = cw - 675;
			video_base_width = 775;
		}
        if (cw >= 1400) {
            //vw = cw - 750;
			video_base_width = 850;
		}
        if (cw >= 1600) {
            //vw = cw - 880;
			video_base_width = 980;
		}
        //if (vw > 520) {
			vw = cw - video_base_width;
            //$('#UI_Central').css("margin-left", 5);
			//$('#UI_Central').css("width", cw - ($('#UI_Left').width() ) - vw - 18);
            //$('#UI_Central').css("margin-right", 5);
            //$('#UI_Right').css("width", vw);
            //$('#OnLine_MV').css("height", vw / 1000 * 618);
			
			//$('#Notice_List').css('height', vw * notice_bili - 2);
			//$('#NoticeList').css('height', vw * notice_bili - 47);
        //}
		/*var c_w = 242 * 1000 / 378;
		var r_w = cw - (242 + c_w) - 10;
		$('#UI_Right').css("width", r_w);
		$('#OnLine_MV').css("height", r_w / 1000 * 618);
		$('#UI_Central').css("margin-left", 242 + 5);
		$('#UI_Central').css("margin-right", r_w + 10);
		//console.log(c_w);
		//console.log(r_w);*/
    } else {
        //$('#UI_Central').css("margin-left", "5px");
        //$('#UI_Central').css("margin-right", "5px");
        //$('#UI_Right').css("width", 520);
        //$('#OnLine_MV').css("height", 520 / 1000 * 618);
		
		//$('#Notice_List').css('height', 520 * notice_bili - 2);
		//$('#NoticeList').css('height', 520 * notice_bili - 47);
    }

    var cH = $(window).height() - 13;
    $('#UI_MainBox').height($(window).height());	
	
	$('#UI_Right').css('height', cH - 45);
	//$('#RoomMV').css('height', cH - (vw * notice_bili) - 48);
	//$('#OnLine_MV').css('height', cH - (vw * notice_bili) - 35 - 48);
	$('#noticefram').height($('#UI_Right').height() - 270);
	$('#noticevideo').height($('#UI_Right').height() - 270);
	$('#noticetext, #noticezxtz').height($('#UI_Right').height() - 35);
	
	$('#UI_Central').width(cw - $('#UI_Left').width() - $('#UI_Right').width() - 15);
	
    //$('#MsgBox1').height($('#MsgBox1').height() + cH - $('#UI_Central').height() - $('#UI_Head').height());
	$('#UI_Central').height(cH - $('#UI_Head').height());
	$('#data_news').height(cH - $('#UI_Head').height() - 35);
	if($('#manage_div').css('display') == 'none') {
		$('#MsgBox').height($('#UI_Central').height() - 195);
	} else {
		$('#MsgBox').height($('#UI_Central').height() - 195 - $('#manage_div').height());
	}
	$('#MsgBox1').height($('#MsgBox').height());

    $('#OnLineUser').height($('#OnLineUser').height() + $('#UI_Central').height() - $('#UI_Left2').height());
    $('#UI_Left1').height($('#UI_Left2').height());
    //$('#NoticeList').height($('#NoticeList').height() + $('#UI_Central').height() - $('#UI_Right').height());
    $('#gd_ad').height($('#NoticeList').height());
	$('#gd_ad img').height($('#NoticeList').height());

    $("#OnLineUser_OhterList").height($('#OnLineUser').height());
    $("#OnLineUser_FindList").height($('#OnLineUser').height());
	
	$('.tanmubt').click(function(){
		var alt = $(this).attr('alt');
		if(alt == 1){
			$(this).attr('alt', '0');
			$(this).attr('src', 'images/tanmubg-off.png');
		}else{
			$(this).attr('src', 'images/tanmubg.png');
			$(this).attr('alt', '1');
		}
	});
}
function addTanmu(str) {
	if($('.tanmubt').attr('alt') == 0)
		return;
	var cont = $('#OnLine_MV_wrap');
	var top = parseInt(Math.random()*90 + 1);
	var el = $('<span style="top: ' + top + '%; left: 100%">' + str + '</span>');
	cont.append(el);
	el.animate({right:"100%"}, parseInt(Math.random() * 20000 + 5000), function(){
		el.remove();
	});
	el.css({left: 'auto'});
}
var video_base_width = 880;
var video_step_width = 50;
function video_resize(type) {
	if(type == 'add') {
		video_base_width = video_base_width - video_step_width;
	} else {
		video_base_width = video_base_width + video_step_width;
	}
    var cw = $(window).width();
    var vw = cw - video_base_width;
	
	$('#UI_Central').css("width", cw - vw - 18 - $('#UI_Left').width());
	//$('#UI_Right').css("width", vw);
	$('#OnLine_MV').css("height", vw / 1000 * 618);
	
    //$('#NoticeList').height($('#NoticeList').height() + $('#UI_Central').height() - $('#UI_Right').height());
}
function OnUnload() {
    var str = "Logout=M=";
    socket.emit('message', str);
}
function tCam(tag) {
    My.cam = tag;
}
function tCamState(tag) {
    My.camState = tag;
    //alert(tag);
}
function onConnect() {
    setInterval("online('"+My.rst+"')",3000);
	auto_speak();
	setInterval("auto_speak()",5000);
    getId('Msg').innerHTML = "";
    var str = 'Login=M=' + My.roomid + '|' + My.chatid + '|' + My.nick + '|' + My.sex + '|' + My.age + '|' + My.qx + '|' + My.ip + '|' + My.vip + '|' + My.color + '|' + My.cam + '|' + My.state + '|' + My.mood;
    socket.emit('message', str);

    if (typeof (UserList) != 'undefined') {
        UserList.init();
    }
    //bt_fenping();
}
function auto_speak() {
	$.getJSON('../ajax.php?act=auto_speak&rid=' + My.rid + '&_' + Math.random() * 10000, function(data) {
		//console.log(data);
		if(data != '') {
			
			if(data.cpjh){
				var msgid = randStr() + randStr();
				var msg = 'SendMsg=M=ALL|false|color:;|' + 'COMMAND_+_rebotmsg_+_xr18_+_' + msgid + "_+_" + encodeURIComponent(data.msg);
				PutMessage(My.rid, 'xr18', 'ALL', '计划员', '大家', 'false', 'color:;', data.msg, msgid, '3', '127');
				socket.emit('message', msg);
				return;
			}
			
			var User = data.user;
			var msgdate = data.msgdate;
			var Txt = data.msg;
			var str = '<div style="clear:both;"></div><div class="msg"><font class="date">' + msgdate + '</font><div class="msg_head"><img src="' + grouparr[User.color].ico + '" class="msg_group_ico" title="' + grouparr[User.color].title + "-" + grouparr[User.color].sn + '"></div><div class="msg_content"><div><font class="u" style="color:' + aColor[User.sex] + '" onclick="ToUser.set(\'' + User.chatid + '\',\'' + User.nick + '\')">' + User.nick + '</font>&nbsp;&nbsp; </div></div><div class="layim_chatsay1"><font>' + Txt + '</font></div></div><div style="clear:both;"></div>';
			
			MsgShow(str, 0);
		}
	});
}
function h_l(e) {
    if (getId('UI_Left').style.display == '') {
        getId('UI_Left').style.display = 'none';
        getId('UI_Central').style.marginLeft = '2px';
        e.src = "images/h_r.gif";
    } else {
        getId('UI_Left').style.display = '';
        getId('UI_Central').style.marginLeft = '157px';
        e.src = "images/h_l.gif";
    }
    getId('FontBar').style.display = 'none';
}
function h_r(e) {
    if (getId('UI_Right').style.display == '') {
        getId('UI_Right').style.display = 'none';
        getId('UI_Central').style.marginRight = '2px';
        e.src = "images/h_l.gif";
    } else {
        getId('UI_Right').style.display = '';
        getId('UI_Central').style.marginRight = '248px';
        e.src = "images/h_r.gif";
    }
    getId('FontBar').style.display = 'none';
}
function getXY(obj) {
    var a = new Array();
    var t = obj.offsetTop;
    var l = obj.offsetLeft;
    var w = obj.offsetWidth;
    var h = obj.offsetHeight;
    while (obj = obj.offsetParent) {
        t += obj.offsetTop;
        l += obj.offsetLeft;
    }
    a[0] = t;
    a[1] = l;
    a[2] = w;
    a[3] = h;
    return a;
}


function CloseColorPicker() {
    getId('ColorTable').style.display = 'none'
}

function ck_Font(e, act) {
    if (e != null) {
        e.value == 'true' ? e.value = 'false' : e.value = 'true';
    }
    switch (act) {
        case 'FontBold':
            if (e.value == 'true')
                getId('Msg').style.fontWeight = 'bold';
            else
                getId('Msg').style.fontWeight = '';
            break;
        case "FontItalic":
            if (e.value == 'true')
                getId('Msg').style.fontStyle = 'italic';
            else
                getId('Msg').style.fontStyle = '';
            break;
        case 'Fontunderline':
            if (e.value == 'true')
                getId('Msg').style.textDecoration = 'underline';
            else
                getId('Msg').style.textDecoration = '';
            break;
        case 'FontColor':
            getId('Msg').style.color = getId('ColorShow').style.backgroundColor;
            break;
        case 'ShowColorPicker':
            bt_ColorPicker();
            break;
    }
}
function ColorPicker() {
    var baseColor = new Array(6);
    baseColor[0] = "00";
    baseColor[1] = "33";
    baseColor[2] = "66";
    baseColor[3] = "99";
    baseColor[4] = "CC";
    baseColor[5] = "FF";
    var myColor;
    myColor = "";
    var myHTML = "";
    myHTML = myHTML + "<div style='WIDTH:180px;HEIGHT:120px;' onclick='ck_Font(null,\"FontColor\");CloseColorPicker()'>";
    for (i = 0; i < 6; i++) {
        for (j = 0; j < 3; j++) {
            for (k = 0; k < 6; k++) {
                myColor = baseColor[j] + baseColor[k] + baseColor[i];
                myHTML = myHTML + "<li data=" + myColor + " onmousemove=\"document.getElementById('ColorShow').style.backgroundColor=this.style.backgroundColor\" style='background-color:#" + myColor + "'></li>";
            }
        }

    }
    for (i = 0; i < 6; i++) {
        for (j = 3; j < 6; j++) {
            for (k = 0; k < 6; k++) {
                myColor = baseColor[j] + baseColor[k] + baseColor[i];//get   Color   
                myHTML = myHTML + "<li data=" + myColor + " onmousemove=\"document.getElementById('ColorShow').style.backgroundColor=this.style.backgroundColor\" style='background-color:#" + myColor + "'></li>";
            }
        }
    }

    myHTML = myHTML + "</div><div style='border:0px; width:180px; height:10px; background:#333333' id='ColorShow'></div>";
    document.getElementById("ColorTable").innerHTML = myHTML;
}
var ColorInit = false;
function bt_ColorPicker() {
    var t = getXY(getId('FontColor'));
    getId('ColorTable').style.top = t[0] - 145 + 'px';
    getId('ColorTable').style.left = t[1] + 1 + 'px';
    if (!ColorInit) {
        ColorPicker();
        ColorInit = true;
    }
    getId('ColorTable').style.display = '';
    getId('ColorTable').focus();
    return true;

}

function bt_Personal(e) {
    if (e.value == 'false') {
        e.value = 'true';
        e.src = "images/Personal_true.gif";
        e.title = '私聊中...[公聊/私聊]';
    } else {
        e.value = 'false';
        e.src = "images/Personal_false.gif";
        e.title = '公聊中...[公聊/私聊]';
    }
}
function bt_FontBar(e) {
    if (getId('FontBar').style.display == 'none') {
        var t = getXY(getId('UI_Control'));
        getId('FontBar').style.display = '';
        //getId('FontBar').style.top = t[0] - 29 + 'px';
        //getId('FontBar').style.left = isIE ? t[1] + 1 : t[1] + 'px';
        getId('FontBar').style.top = '0px';
        getId('FontBar').style.left = '0px';
        getId('FontBar').style.width = t[2] - 8 + 'px';
    } else {
        getId('FontBar').style.display = 'none';
    }
}
function bt_Send_key_option(e) {
    var t = getXY(e);
    getId('Send_key_option').style.top = t[0] - 50 + 'px';
    getId('Send_key_option').style.left = t[1] + 2 - 165 + 'px';
    getId('Send_key_option').style.display = '';
    getId('Send_key_option').focus();
}

function InsertImg(id, src) {
    $(id).append('<img src=\"' + src + '\">');
}
function bt_insertImg(id) {
    $('#imgUptag').val(id);
    $('#filedata').click();
}
function bt_gifts(e) {
    layer_open('送礼物', '../glist_room.php', 302, 405, '', true)
}
function bt_MsgClear() {
    getId('MsgBox1').innerHTML = '';
    getId('MsgBox2').innerHTML = '';
}
function bt_SendEmote(obj) {
    getId('Msg').innerHTML = obj.innerHTML;
    SysSend.msg();
    getId('Emote').style.display = 'none';
}
function bt_SwitchListTab(tag) {
    if (tag == 'User') {
        $("#OnLineUser_OhterList").hide();
        $("#OnLineUser_FindList").hide();
        $("#OnLineUser").show();
        $("#listTab1")[0].className = 'bg_png2';
        $("#listTab2")[0].className = '';
    } else if (tag == "Other") {
        $("#OnLineUser_OhterList").show();
        $("#OnLineUser_FindList").hide();
        $("#OnLineUser").hide();
        $("#listTab1")[0].className = '';
        $("#listTab2")[0].className = 'bg_png2';
        UserList.getmylist(My.name);
    }
    return false;
}
function bt_defvideosrc() {
    if (check_auth('def_videosrc')) {
        SysSend.command('setVideoSrc', My.chatid + '_+_' + My.nick);
        $.ajax({type: 'get', url: '../ajax.php?act=setdefvideosrc&vid=' + encodeURIComponent(My.chatid) + '&nick=' + encodeURIComponent(My.nick) + '&rid=' + My.rid});
    }
}
function bt_msgBlock(id) {
    SysSend.command('msgBlock', id);
}
function bt_msgAudit(id, a) {
    SysSend.command('msgAudit', id);
    $(a).hide();
}
function bt_FindUser() {
    getId("OnLineUser_OhterList").style.display = "none";
    var username = getId('finduser').value;
    getId("OnLineUser_FindList").style.display = "none";
    getId("OnLineUser").style.display = "";
    //alert(username);
    if (username == "") {
        getId("OnLineUser_FindList").style.display = "none";

        getId("OnLineUser").style.display = "";
    } else {
        getId("OnLineUser_FindList").style.display = "";
        getId("OnLineUser_FindList").innerHTML = "";
        getId("OnLineUser_FindList").style.height = getId("OnLineUser").offsetHeight + 'px';
        getId("OnLineUser").style.display = "none";

        var ulist = UserList.List();
        var li = "";
        for (c in ulist) {
            if (ulist[c].nick.toLowerCase().indexOf(username.toLowerCase()) >= 0) {
                //alert(ulist[c].nick);
                li = getId(ulist[c].chatid);
                var t_li = CreateElm(getId("OnLineUser_FindList"), 'li', false, 'fn' + ulist[c].chatid);
                t_li.innerHTML = li.innerHTML;
                t_li.oncontextmenu = li.oncontextmenu;
                t_li.onclick = li.onclick;
                t_li.ondblclick = li.ondblclick;
            }
        }
    }
}
var fenping = true;
function bt_fenping() {
    if (fenping == true) {
        fenping = false;
        //getId('btfp').src = 'images/FP_true.gif';
        getId('drag_bar').style.display = "";
        getId('MsgBox2').style.display = "";
        getId('MsgBox2').style.height = 100 + 'px';
        getId('MsgBox1').style.height = getId('MsgBox1').offsetHeight - 100 - getId('drag_bar').offsetHeight + "px";
    } else {
        fenping = true;
        //getId('btfp').src = 'images/FP_false.gif';
        getId('MsgBox1').style.height = getId('MsgBox').offsetHeight + "px";
        getId('drag_bar').style.display = "none";
        getId('MsgBox2').style.display = "none";

    }
    MsgAutoScroll();
}
var audioNotify = true;
function bt_toggleAudio() {
    if (audioNotify == true) {
        audioNotify = false;
        getId('toggleaudio').src = 'images/Sc.gif';
    } else {
        audioNotify = true;
        getId('toggleaudio').src = 'images/So.gif';
    }
}
var toggleScroll = true;
function bt_toggleScroll() {
    if ($("#bt_gundong").attr("select") == "true") {
        $("#bt_gundong").attr("select", "false");
        toggleScroll = false;
    } else {
        $("#bt_gundong").attr("select", "true");
        toggleScroll = true;
    }
}
function showReplyPanel(e, toinput) {
    var offset = $(e).offset();
    var t = offset.top;
    var l = offset.left;
    $('#reply_panel').css({"top": t - $('#reply_panel').outerHeight(), "left": l - 214});
    $('#reply_panel').toggle();
    $('#reply_panel').attr("toinput", toinput);
}
function sign_in() {
	if(check_login()) {
		$.ajax({type: 'get', dataType: 'json', url: '../ajax.php?act=signin',
			success: function(data) {
				if (data.code == "1") {
					layer.msg("签到成功", {shift: 0});
				} else {
					layer.msg(data.msg, {shift: 6});
				}
			},
			error: function() {
				layer.msg("签到失败，请重试", {shift: 6});
			}
		});
	}
}
function check_login() {
	if(My.chatid.indexOf('x') > -1) {
		layer.msg("请先登录", {shift: 6});
		return false;
	}
	return true;
}
function icon4() {
    var a = $("head  title").text();
    postToQzone(a)
}
function icon2() {
    var a = $("head  title").text();
    postToXinLang(a)
}
function icon3() {
    var a = $("head  title").text();
    postToWb(a)
}
function icon5() {
    var b, c, a = '<div class="sharefrends">';
    a += '<div class="closeMessBtn"></div><input type="text"><span class="copy" >复制</span></div>',
    $('body').append(a),
    b = ($(window).height() - 266) / 2,
    c = ($(window).width() - 266) / 2,
    $(".sharefrends").css({
        left: c,
        top: b
    }),
    $(".sharefrends input").val(window.location.href),
    $(".closeMessBtn").click(function() {
        $(".sharefrends").remove()
    }),
    $(".sharefrends .copy").zclip({
        path: "script/ZeroClipboard.swf",
        copy: $(".sharefrends input").val(),
        afterCopy: function() {
            $(".sharefrends").remove(),
            waitting("网址复制成功,粘贴分享给您的QQ好友！", 3500)
        }
    })
}
function postToWb(a) {
    var b = encodeURI(a),
    c = encodeURI(document.location),
    d = encodeURI("appkey"),
    e = encodeURI(""),
    f = "",
    g = "http://v.t.qq.com/share/share.php?title=" + b + "&url=" + c + "&appkey=" + d + "&site=" + f + "&pic=" + e;
    window.open(g, "转播到腾讯微博", "width=700, height=680, top=0, left=0, toolbar=no, menubar=no, scrollbars=no, location=yes, resizable=no, status=no")
}
function postToQzone(a) {
    var b = encodeURI(a),
    c = encodeURI(a),
    d = encodeURI(document.location);
    return window.open("http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?title=" + b + "&url=" + d + "&summary=" + c),
    !1
}
function postToXinLang(a) {
    window.open("http://v.t.sina.com.cn/share/share.php?title=" + encodeURIComponent(a) + "&url=" + encodeURIComponent(location.href) + "&rcontent=", "_blank", "scrollbars=no,width=600,height=450,left=75,top=20,status=no,resizable=yes")
}
function newsflash() {
	var $newsflash = $('#data_news');
	if ($newsflash.children().length > 0)
		return;
	$newsflash.append('<iframe src="http://www.jin10.com/example/jin10.com.html" style="border: 0px; height: 100%; width: 100%;" id="news_list_frame"></iframe>');
}
function wscnInfo() {
	var $wscn = $('#js-wscn-sidebar-wrapper');
	if ($wscn.children().length > 0)
		return;
	var parentId = 'js-wscn-sidebar-wrapper';
	var iframeId = 'js-wscn-sidebar-iframe';
	var options = /*options*/{
		"chart": {
			"interval": "5"
		},
		"theme": "dark",
		"height": 396,
		"width": $("#UI_Central").width(),
		"active": 0,
		"utmSource": "unknown",
		"tabs": [
			{
				"name": "股指",
				"symbols": [
					"SPX500",
					"000001",
					"US10Year",
					"NAS100",
					"US30",
					"JPN225INDEX",
					"hkg33index",
					"UK100",
					"eustx50index"
				]
			},
		]
	}
	var sidebar = new WallstreetCN.embed.Sidebar(parentId, options, iframeId);
	sidebar.render();
}
var nomore = false;
function chatload() {
	if(nomore) {
		layer.msg('没有更多了', {shift: 6});
		return;
	}
	$('.load_msg').remove();
	$.getJSON('../ajax.php?act=load_msg&lastmsgid=' + lastmsgid + '&rid=' + My.rid, function(data) {
		//console.log(data);
		if(lastmsgid == data.lastmsgid) {
			nomore = true;
			layer.msg('没有更多了', {shift: 6});
			return;
		} else {
			lastmsgid = data.lastmsgid;
		}
		$('#MsgBox1').prepend(data.msgs);
	});
}
function closeLive() {
	$.getJSON('/ajax.php?act=closeLive&roomid=' + My.rid, function(data) {
		if(data == 'ok') {
			//layer.msg('直播室即将关闭');
			//setTimeout(function() {
				SysSend.command("closeLive", "");
			//}, 3e3);
		} else if(data == '0') {
			layer.msg('直播室已是关闭状态', {shift: 6});
		} else {
			layer.msg('关闭直播室失败', {shift: 6});
		}
	});
}
$(document).ready(function() {
    $('#replys li').click(function() {
        $('#Msg').html($(this).html());
        $('#reply_panel').hide();
    });
	
	$('.giftList .gift_close').click(function() {
		$('.giftList').hide();
	});
	
	$('.main-content-menu li').click(function() {
		var targetTabId = $(this).attr('data-type');
		$('.main-content').hide();
		if (targetTabId == 'hall') {
			$('#data_hall').show();
		}
		if (targetTabId == 'news') {
			//newsflash();
			//$('#data_news').show();
		}
		if (targetTabId == 'market') {
			//wscnInfo();
			//$('#data_market').show();
		}
	});
	
    $('#retract').click(function() {
        $('#UI_Left').hide();
        $('#unretract').show();
        $('#UI_Central').width($('#UI_Central').width() + 249);
    });
    $('#unretract').click(function() {
        $('#UI_Left').show();
        $('#unretract').hide();
        $('#UI_Central').width($('#UI_Central').width() - 249);
    });
});