$(function(){
	var msg = '';
	var draw_in = false;
	var $hand = $('.hand');
	$hand.click(function(){
		if(draw_in) {
			layer.msg(msg, {shift: 6});
			return;
		}
		draw_in = true;
		if(My.color == '0') {
			layer.msg('请先登录', {shift: 6});
			return false;
		}
		$.getJSON("../ajax.php?act=rotate",function(data, status){
			if(data.code == -1){
				msg = data.msg;
				layer.msg(data.msg, {shift: 6});
				return false;
			}
			data.code = parseInt(data.code);
			switch(data.code){
				case 1:
					rotateFunc(1, 16, data.prize);
					break;
				case 2:
					rotateFunc(2, 47, data.prize);
					break;
				case 3:
					rotateFunc(3, 76, data.prize);
					break;
				case 4:
					rotateFunc(4, 106, data.prize);
					break;
				case 5:
					rotateFunc(5, 135, data.prize);
					break;
				case 6:
					rotateFunc(6, 164, data.prize);
					break;
				case 7:
					rotateFunc(7, 200, data.prize);
					break;
				case 8:
					rotateFunc(8, 241, data.prize);
					break;
				case 9:
					rotateFunc(9, 278, data.prize);
					break;
				case 10:
					rotateFunc(10, 326, data.prize);
					break;				
			}
		});
	});

	var rotateFunc = function(awards, angle, data){
		$hand.stopRotate();
		$hand.rotate({
			angle: 0,
			duration: 8000,
			animateTo: angle + 1800,
			callback: function(){
				arr = data.split('_+_');
				alert(arr[1]);
				if(arr[0] == '1') {
					SysSend.command('lottery', My.chatid + '_+_' + arr[1]);
				}
				draw_in = false;
			}
		});
	};
});

var vcode_bool = false;
function change_vcode() {
	vcode_bool = false;
	$('#imgCode').attr('src', '/include/code.php?_' + new Date().getTime());
}
function check_vcode() {
	$.ajax({
		type: "GET",
		url: '../../ajax.php?act=check_vcode',
		dataType: "json",
		data: {"vcode": $('#vcode').val()},
		success: function(d) {
			console.log(d);
			if (d == '1') {
				layer.msg('验证码正确');
				vcode_bool = true;
				setTimeout('$(".rotateMobile2").hide()', 1000);
				setTimeout('$(".btn-send").click()', 2000);
			} else {
				layer.msg('验证码错误');
				vcode_bool = false;
			}
		}
	});
}

function rotateSendMsg(e){
	var tel = $("#rmobile");
	var v = tel.val();
	var isTelephone=/1[34758]{1}\d{9}$/.test(v);
	if(!v){
		alert("手机号不能为空");
		return false;
	} else if(!isTelephone) {
		alert('手机号不正确');
		return false;	
	}
	if(!vcode_bool) {
		change_vcode();
		$('.rotateMobile2').show();
		return false;
	}
	$.ajax({
         url: "../sms.php",
         type: "get",
         data:{phone:v},
		 dataType: "json",
         error: function(){  
             alert('Error loading XML document');  
         },  
         success: function(data,status){ 
			if(parseInt(data.code) == 0){
				timeCount = setInterval("waitRotateMsg()",1000);
				alert('短信验证码发送成功');
			} else {
				alert(data.msg);
			}
         }
     }); 

}

rotateWAIT = 60;

function waitRotateMsg() {

	$('.rotateMobile .btn-send').html(rotateWAIT+'秒后重新发送');
	$('.rotateMobile .btn-send').attr('onclick','');
	rotateWAIT--;
	if(rotateWAIT == 0){
		clearInterval(timeCount);
		rotateWAIT=60;
		$('.rotateMobile .btn-send').html('获取短信验证码');
		$('.rotateMobile .btn-send').attr('onclick','');
	}
}

function rotateMobile(){
	var mobile = $("#rmobile").val();
	var validate = $("#rcode").val();
	var isTelephone = /1[34758]{1}\d{9}$/.test(mobile);
	if(!mobile){
		alert("手机号不能为空");
		return false;
	}else if(!isTelephone){
		alert('手机号不正确');
		return false;	
	}
	if(!validate) {
		alert('请输入验证码');
		return false;
	}
	$.ajax({
         url: "../ajax.php?act=addmobile",  
         type: "POST",
         data:{mobile:mobile, validate:validate},
         error: function(){  
             alert('Error loading XML document');  
         },  
         success: function(data,status){ 
			if(data=='success'){
				vcode_bool = false;
				$("#rmobile").val('');
				$("#rcode").val('');
				$('.rotateMobile').hide();
				 $('.hand').click();
			}else if(data=='invalidatemobile'){
				alert("手机号不正确");
			}else if(data=='invalidatecode'){
				alert("验证码不正确");
			}else if(data=='hasmobile'){
				alert("对不起，该手机已经抽过奖了");
			}
         }
     }); 
}