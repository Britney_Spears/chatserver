var current = 0;
var max;
var interval;
$(document).ready(function() {
	max = $('#gd_ad a').length;
	var Ele = $('.ck-slidebox .dot-wrap').html();
	for(var i = 1; i < $('#gd_ad a').length; i++) {
        $('.ck-slidebox .dot-wrap').append(Ele);
	}
	$('.ck-slidebox .dot-wrap li:first').addClass('current');
	$('.tab a:first').addClass('active');
	$('.notice_div:first').css('display','');
	$('#gd_ad a').hide();
	$('#gd_ad a:eq('+current+')').show();
	$('.NoticeList .tab a').on('click',function(){
		$('.NoticeList .tab a').removeClass('active');
		$(this).addClass('active');
		$('.notice_div').css('display','none');
		$('#'+$(this)[0].id+'_div').css('display','');
	});
	SpreadSccroll();
	$('#gd_ad .prev').on('click', function() {
		clearInterval(interval);
		var $myul = $("#gd_ad");
		if($myul.find("a").length < 2) {
			return;
		}
		--current;
		if(current < 0) {
			current = max - 1;
		}
		change();
		SpreadSccroll();		
	});
	$('#gd_ad .next').on('click', function() {
		clearInterval(interval);
		var $myul = $("#gd_ad");
		if($myul.find("a").length < 2) {
			return;
		}
		++current;
		if(current >= max) {
			current = 0;
		}
		change();
		SpreadSccroll();
	});
	$('.ck-slidebox .dot-wrap li').mouseover(function() {
		clearInterval(interval);
		current = $(this).index();
		change();
		SpreadSccroll();
	});
	$(document).on('mouseover', '#gd_ad', function() {
		$(this).find('span').css('opacity', '0.5');
	}).on('mouseout', '#gd_ad', function() {
		$(this).find('span').css('opacity', '0.15');
	});
});

function SpreadSccroll() {
	interval = setInterval(function() {
		var $myul = $("#gd_ad");
		if($myul.find("a").length < 2) {
			return;
		}
		++current;
		if(current >= max) {
			current = 0;
		}
		change();
	}, 5000);
}
function change() {
	$('#gd_ad a').hide();
	$('#gd_ad a:eq('+current+')').fadeToggle();
	$('.ck-slidebox .dot-wrap li').removeClass('current');
	$('.ck-slidebox .dot-wrap li:eq('+current+')').addClass('current');
}