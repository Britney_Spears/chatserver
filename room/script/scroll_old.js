var current = 0;
var max;
$(document).ready(function() {
	max = $('#gd_ad a').length;
	var Ele = $('.ck-slidebox .dot-wrap').html();
	for(var i = 1; i < $('#gd_ad a').length; i++) {
        $('.ck-slidebox .dot-wrap').append(Ele);
	}
	$('.ck-slidebox .dot-wrap li:first').addClass('current');
	$('.tab a:first').addClass('active');
	$('.notice_div:first').css('display','');
	$('.tab a').on('click',function(){
		$('.tab a').removeClass('active');
		$(this).addClass('active');
		$('.notice_div').css('display','none');
		$('#'+$(this)[0].id+'_div').css('display','');
	});
	SpreadSccroll();
	$('#gd_ad .prev').on('click', function() {
		var $myul = $("#gd_ad");
		if($myul.find("a").length < 2) {
			return;
		}
		$actli = $myul.find("a:first");
		$actli.fadeToggle(function(){
			$myul.find("a:last").prependTo($myul);
		});
		$actli.fadeToggle();
	});
	$('#gd_ad .next').on('click', function() {
		var $myul = $("#gd_ad");
		if($myul.find("a").length < 2)
		{
			return;
		}
		$actli = $myul.find("a:first");
		$actli.fadeToggle(function(){
			$myul.find("a:first").appendTo($myul);
		});
		$actli.fadeToggle();
	});
});

function SpreadSccroll() {
	setTimeout(function() {
		var $myul = $("#gd_ad");
		if($myul.find("a").length < 2) {
			return;
		}
		$actli = $myul.find("a:first");
		$actli.fadeToggle(function(){
			$myul.find("a:first").appendTo($myul);
			SpreadSccroll();
		});
		$actli.fadeToggle();
	}, 5000);
}