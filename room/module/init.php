<?php

    require_once '../include/common.inc.php';
	$rid = $cfg['config']['id'];

	if(stripos(auth_group($_SESSION['login_gid']), 'room_' . $rid)===false)
		exit("没有权限！");
	
    if(!isset($_SESSION['login_uid']) && $cfg['config']['loginguest'] == '0') {
        header('location:../logging.php?rid=' . $cfg['config'][id]);
        //echo '<script>openWin(2,false,"minilogin.php",390,310)</script>';
    }
    require_once PPCHAT_ROOT.'./include/json.php';
    $json=new JSON_obj;
    //游客登录
    if(empty($_SESSION['login_gid']) && $cfg['config']['loginguest']=="1"){
        guestLogin();
    }

    //房间状态
    if($cfg['config']['state']=='2' && $_SESSION['room_'.$rid] != true){
        header("location:login.php?rid=" . $cfg['config'][id]);exit();
    }
    if($cfg['config']['state']=='0'){
        exit("<script>location.href='error.php?msg=系统处于关闭状态！请稍候……'</script>");exit();
    }
    //是否登录
    if(!isset($_SESSION['login_uid'])){
        //header("location:../logging.php?rid=" . $cfg['config'][id]);exit();
        //echo '<script>openWin(2,false,"minilogin.php",390,310)</script>';
    }

    //用户信息
    $uid = $_SESSION['login_uid'];
    $db->query("update {$tablepre}members set regip='$onlineip' where uid='{$uid}'");
    $userinfo = $db->fetch_row($db->query("select m.*,ms.* from {$tablepre}members m,{$tablepre}memberfields ms  where m.uid=ms.uid and m.uid='{$uid}'"));
	$user_class = $db->fetch_row($db->query("select * from {$tablepre}members where rid = '{$rid}' and uid='{$uid}'"));
	if($userinfo['gid'] == '3' && empty($user_class)) {
		$userinfo['gid'] = 1;
	}
    $_SESSION['login_gid'] = $userinfo['gid'];

	$onlinetime = $db->fetch_row($db->query("select onlinetime from {$tablepre}onlinetime where rid='{$rid}' and lastactivityip='{$onlineip}'"));	
    if($_SESSION['login_gid'] == 0 && $cfg['config']['state'] == '3' && $onlinetime['onlinetime'] > ($cfg['config']['limit'] * 60)) {
        header("location:../logging.php?rid=" . $cfg['config'][id]);exit();
    }
	
    //游客
    if($_SESSION['login_gid'] == 0) {
        $userinfo['username'] = $userinfo['nickname']=$_SESSION['login_nick'];
        $userinfo['sex'] = $_SESSION['login_sex'];
        $userinfo['uid'] = $_SESSION['login_guest_uid'];
    }

    //黑名单
    $query=$db->query("select * from {$tablepre}ban where (username='{$userinfo[username]}' or ip='{$onlineip}') and losttime>".gdate()." limit 1");
    while($row=$db->fetch_row($query)){
        exit("<script>location.href='error.php?msg=用户名或IP受限！过期时间".date("Y-m-d H:i:s",$row['losttime'])."'</script>");exit();
    }

    //用户组
    $query=$db->query("select * from {$tablepre}auth_group order by ov desc");
    while($row=$db->fetch_row($query)){
        $groupli .= "<div id='group_{$row[id]}'></div>";
        $grouparr .= "grouparr['{$row[id]}']=".json_encode($row).";\n";
        $group["m".$row[id]] = $row;
    }
    //聊天历史记录
    //$query=$db->query("select * from {$tablepre}msgs where rid='".$rid."' and p='false' and (state!='1' or state!='7') and `type`='0' order by id desc limit 0,20 ");
    $query=$db->query("select * from {$tablepre}msgs where rid='".$rid."' and p='false' and (state='0' || state='7') and `type`='0' order by id desc limit 0,20 ");
    //$query=$db->query("select * from {$tablepre}msgs where rid='".$rid."' and p='false' and `type`='0' order by id desc limit 0,20 ");
    while($row=$db->fetch_row($query)){
		if(($row['state'] == '1' || $row['state'] == '7') && $row['uid'] != $userinfo['uid'] && !check_user_auth($userinfo[uid], 'room_admin')) {
			continue;
		}
        $row['msg']=str_replace(array('&amp;', '','&quot;', '&lt;', '&gt;'), array('&', "\'",'"', '<', '>'),$row['msg']);
		$class = '';
		if('xr18' == $row[uid] || check_user_auth($row[uid], 'room_admin')) {
			$class = 'red';
		}
		$address = '';
		if(check_user_auth($userinfo[uid], 'room_admin') && !empty($row[address])) {
			//$address = "<div class='msg_content'><div><font class='u'>{$row[address]}</font>&nbsp;&nbsp; </div></div>";
		}
        if($row[tuid]!="ALL"){
            //$omsg = "<div style='clear:both;'></div><div class='msg'  id='{$row[msgid]}'><div class='msg_head'><img src='../face/img.php?t=p1&u={$row[uid]}'></div><div class='msg_content'><div><font class='u'  onclick='ToUser.set(\"{$row[uid]}\",\"{$row[uname]}\")'>{$row[uname]}</font> <img src='".$group["m".$row[ugid]][ico]."' class='msg_group_ico' title='".$group["m".$row[ugid]][title]."-".$group["m".$row[ugid]][sn]."'>   <font class='dui'>对</font> <font class='u' onclick='ToUser.set(\"{$row[tuid]}\",\"{$row[tname]}\")'>{$row[tname]}</font> 说 <font class='date'>".date('H:i:s',$row[mtime])."</font></div><div class='layim_chatsay' style='margin:5px 0px;'><font  style='{$row[style]};'>{$row[msg]}</font><em class='layim_zero'></em></div></div></div><div style='clear:both;'></div>".$omsg;
			$omsg = "<div class='msg' id='{$row[msgid]}'><div class='msg_head'><img title='".$group["m".$row[ugid]][title]."-".$group["m".$row[ugid]][sn]."' class='msg_group_ico' src='".$group["m".$row[ugid]][ico]."'></div><div class='msg_content'><div><font class='u {$class}' data-uid='{$row[uid]}' onclick='\"{$row[uid]}\",\"{$row[uname]}\")'>{$row[uname]}</font>&nbsp;&nbsp; <font class='date'>".date('H:i',$row[mtime])."</font><font class='dui'>    对</font> <font class='u' onclick='ToUser.set(\"{$row[tuid]}\",\"{$row[tname]}\")'>{$row[tname]}</font> ".$address."<div class='layim_chatsay1'><font style='{$row[style]};'>{$row[msg]}</font></div></div></div></div><div style='clear:both;'></div>".$omsg;
		}elseif(preg_match('/^.*?onclick=\"getRedbag\(this\)\".*?$/i',$row[msg])) {
			//$omsg = "<div class='msg' id='{$row[msgid]}'><font class='date'>".date('H:i',$row[mtime])."</font><div class='msg_head'><img src='".$group["m".$row[ugid]][ico]."' class='msg_group_ico' title='".$group["m".$row[ugid]][title]."-".$group["m".$row[ugid]][sn]."'></div><div class='msg_content'><div><font class='u {$class}' data-uid='{$row[uid]}' onclick='ToUser.set(\"{$row[uid]}\",\"{$row[uname]}\")'>{$row[uname]}</font>&nbsp;&nbsp; </div></div>".$address."<div class='layim_chatsay1' style='margin:5px 0px;padding: 0;'><font style='{$row[style]};'>{$row[msg]}</font></div></div><div style='clear:both;'></div>".$omsg;
			$omsg = "<div class='msg' id='{$row[msgid]}'><div class='msg_head'><img src='".$group["m".$row[ugid]][ico]."' class='msg_group_ico' title='".$group["m".$row[ugid]][title]."-".$group["m".$row[ugid]][sn]."'></div><div class='msg_content'><div><font class='u {$class}' data-uid='{$row[uid]}' onclick='ToUser.set(\"{$row[uid]}\",\"{$row[uname]}\")'>{$row[uname]}</font>&nbsp;&nbsp; <font class='date'>".date('H:i',$row[mtime])."</font></div>".$address."<div class='layim_chatsay1' style='padding:0'><font style='{$row[style]};'>{$row[msg]}</font></div></div></div><div style='clear:both;'></div>" . $omsg;
        } else {
            //$omsg = "<div style='clear:both;'></div><div class='msg' id='{$row[msgid]}'><div class='msg_head'><img src='../face/img.php?t=p1&u={$row[uid]}'></div><div class='msg_content'><div><font class='u'  onclick='ToUser.set(\"{$row[uid]}\",\"{$row[uname]}\")'>{$row[uname]}</font> <img src='".$group["m".$row[ugid]][ico]."' class='msg_group_ico' title='".$group["m".$row[ugid]][title]."-".$group["m".$row[ugid]][sn]."'> <font class='date'>".date('H:i:s',$row[mtime])."</font></div><div class='layim_chatsay' style='margin:5px 0px;'><font  style='{$row[style]};'>{$row[msg]}</font><em class='layim_zero'></em></div></div></div><div style='clear:both;'></div>".$omsg;
			$omsg = "<div class='msg' id='{$row[msgid]}'><div class='msg_head'><img src='".$group["m".$row[ugid]][ico]."' class='msg_group_ico' title='".$group["m".$row[ugid]][title]."-".$group["m".$row[ugid]][sn]."'></div><div class='msg_content'><div><font class='u {$class}' data-uid='{$row[uid]}' onclick='ToUser.set(\"{$row[uid]}\",\"{$row[uname]}\")'>{$row[uname]}</font>&nbsp;&nbsp; <font class='date'>".date('H:i',$row[mtime])."</font></div>".$address."<div class='layim_chatsay1'><font style='{$row[style]};'>{$row[msg]}</font></div></div></div><div style='clear:both;'></div>" . $omsg;
        }
		$lastmsgid = $row['id'];
    }
	
	$omsg = '<div class="load_msg"><a class="" onclick="chatload()" href="javascript:void(0)">查看更多消息</a></div>' . $omsg . "<div class='tips'>以上是历史消息</div><div style='clear:both;'></div>";
	
	//用户备注
	if($_SESSION['login_gid'] == '2' || $_SESSION['login_gid'] == '3' || $_SESSION['login_gid'] == '4') {
		$query=$db->query("select * from {$tablepre}member_remark where uid={$uid}");
		while($row=$db->fetch_row($query)){
			$remark .= "remark['{$row[muid]}']='".$row['mname']."';\n";
		}
	}
	
    //其他处理
    $ts=explode(':',$cfg['config']['tserver']);
    if(!isset($_SESSION['room_'.$uid.'_'.$cfg['config'][id]])){
        $db->query("insert into  {$tablepre}msgs(rid,ugid,uid,uname,tuid,tname,mtime,ip,msg,`type`)values('{$cfg[config][id]}','{$userinfo[gid]}','{$userinfo[uid]}','{$userinfo[username]}','','','".gdate()."','{$onlineip}','登陆直播间','3')");
        //$db->query("update {$tablepre}memberfields set logins=logins+1 where uid='{$uid}'");	
        $_SESSION['room_'.$uid.'_'.$cfg['config'][id]] = 1;
    }
?>