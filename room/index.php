<?php include_once('module/init.php'); ?>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
	<meta name="renderer" content="webkit">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title><?=$cfg['config']['title']?></title>
	<meta content="<?=$cfg['config']['keys']?>" name="keywords">
	<meta content="<?=$cfg['config']['dc']?>" name="description">
	<meta property="qc:admins" content="310104337127367555100637572775" />
	<meta name="360-site-verification" content="e9f57ba7ed296be93ddcb05e3a1eabab" />
	<link rel="shortcut icon" type="image/x-icon" href="<?=$cfg['config']['ico']?>" />
	<link href="https://cdn.bootcss.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
	<link href="skins/qqxiaoyou/css.css" rel="stylesheet" type="text/css"/>
	<link href="images/layim.css" rel="stylesheet" type="text/css"  />
	<script src="script/jquery.min.js"></script>
	<script src="script/jquery.zclip.min.js"></script>
	<script src="script/jquery.cookie.js"></script>
	<script src="script/json3.min.js"></script>
	<script src="script/socket.io.js"></script>
	<script src="script/MSClass.js"></script>
	<script src="script/layer.js"></script>
	<script src="script/jquery.nicescroll.min.js"></script>
	<script src="script/pastepicture.js"></script>
	<script src="script/swfobject.js" type="text/javascript"></script>
	<script src="script/function.js?<?=time()?>" type="text/javascript"></script>
	<script src="script/init.js?<?=time()?>" type="text/javascript"></script>
	<script src="script/scroll.js" type="text/javascript"></script>
	<script src="script/jquery.rotate.min.js" type="text/javascript"></script>
	<script src="script/rotate.js?<?=time()?>" type="text/javascript"></script>
	<script src="script/sidebar.js" type="text/javascript"></script>
	<script src="script/device.min.js" type="text/javascript"></script>
	<script src="http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js" type="text/javascript"></script>
	<script>
		if (device.mobile()) {
			window.location = 'm/index.php?rid=<?=$rid?>';
		}
		var UserList;
		var ToUser;
		var ToTeacher;
		var ToTeacherName;
		var VideoLoaded=false;
		var My={dm:'<?=$_SERVER['HTTP_HOST']?>',rid:'<?=$rid?>',roomid:'<?=$_SERVER['HTTP_HOST']?>/<?=$rid?>',chatid:'<?=$userinfo['uid']?>',name:'<?=$userinfo['username']?>',nick:'<?=$userinfo['nickname']?>',sex:'<?=$userinfo['sex']?>',age:'0',qx:'<?=check_auth('room_admin')?'1':'0'?>',ip:'<?=$onlineip?>',vip:'<?=$userinfo['gid']?>',color:'<?=$userinfo['gid']?>',cam:'',state:'<?=$userinfo['state']?>',mood:'<?=$userinfo['mood']?>',rst:'<?=time()?>',camState:'1',key:'<?=connectkey()?>',gold:'<?=$userinfo['gold']?>'}
		var RoomInfo={loginTip:'<?=$cfg['config']['logintip']?>',Msglog:'<?=$cfg['config']['msglog']?>',msgBlock:'<?=$cfg['config']['msgblock']?>',msgAudit:'<?=$cfg['config']['msgaudit']?>',defaultTitle:document.title,MaxVideo:'10',VServer:'<?=$cfg['config']['vserver']?>',VideoQ:'',TServer:'<?=$ts[0]?>',TSPort:'<?=$ts[1]?>',PVideo:'<?=$cfg['config']['defvideo']?>',AutoPublicVideo:'0',AutoSelfVideo:'0',type:'1',PVideoNick:'<?=$cfg['config']['defvide0nick']?>',OtherVideoAutoPlayer:'<?=$cfg['config']['livetype']?>',livefp1:'<?=$cfg['config']['livefp1']?>',livefp2:'<?=$cfg['config']['livefp2']?>',r:'<?=$cfg['config']['rebots']?>'}
		var grouparr=new Array();
		<?=$grouparr?>
		var remark = new Array();
		<?=$remark?>
		var ReLoad;
		var isIE=document.all;
		var aSex=['<span class="sex-womon"></span>','<span class="sex-man"></span>',''];
		var aColor=['#FFF','#FFF','#FFF'];
		var bg_img = '<?=$cfg['config']['bg']?>';
		var msg_unallowable="<?=$cfg['config']['msgban']?>";
		var room_state = parseInt("<?=$cfg['config']['state']?>");
		var lastmsgid = '<?=$lastmsgid?>';
	</script>
	<?php
		//禁言
		$query=$db->query("select * from {$tablepre}send where (username='{$userinfo[username]}' or ip='{$onlineip}') limit 1");
		while($row=$db->fetch_row($query)){
			echo "<script>remove_auth('msg_send');</script>";
		}
	?>
</head>
<body onresize="OnResize()" onUnload="OnUnload()">

	<div id="UI_MainBox" style="background: #408080 url(<?php echo empty($_COOKIE['bg_img']) ? $cfg['config']['bg'] : $_COOKIE['bg_img']; ?>) no-repeat scroll 0 0/100% 100%;">
	
		<?php include_once('module/header.php'); ?>
		
		<?php include_once('module/left.php'); ?>
		
		<?php include_once('module/center.php'); ?>
	  
		<?php include_once('module/right.php'); ?>
		
		<!--<div id="footer" class="price">
			<div class="price-t">
				<i class="ico ico16 ico16-price"></i>行情</div>
			<ul id="ulForignProduct">
				<li id="Price_XAUUSD">
					<span>现货黄金</span>
					<span class="red">1283.78</span>
					<span class="red">+0.2%
						<i class="ico ico16 ico16-ar-u"></i></span>
				</li>
				<li id="Price_XAGUSD">
					<span>现货白银</span>
					<span class="red">18.298</span>
					<span class="red">+0.47%
						<i class="ico ico16 ico16-ar-u"></i></span>
				</li>
				<li id="Price_UKOil">
					<span>布伦特油</span>
					<span class="red">46.37</span>
					<span class="red">+0.24%
						<i class="ico ico16 ico16-ar-u"></i></span>
				</li>
				<li id="Price_USOil">
					<span>美国原油</span>
					<span class="red">45.034</span>
					<span class="red">+0.24%
						<i class="ico ico16 ico16-ar-u"></i></span>
				</li>
				<li id="Price_USDX">
					<span>美元指数</span>
					<span class="gre">97.673</span>
					<span class="gre">-0.07%
						<i class="ico ico16 ico16-ar-d"></i></span>
				</li>
			</ul>
			<div class="price-t price-r"><?php echo $cfg['config']['copyright']; ?></div>
		</div>-->

	</div>

	<?php include_once('module/footer.php'); ?>
	
	<style>
		#loginnotice{position: absolute;bottom:20px;left:5px;}
		#loginnotice .sitem{width:215px;height:32px;background: url(/images/fa.png) center no-repeat;background-size: 275px 32px;color:#ffd800;line-height:36px;padding-left:60px;font-size:14px;margin-left:-280px}
	</style>
	<div id="loginnotice">
	</div>
	
	
	
	
<style>
						#hongBaoClick{    position: absolute;width: 415px;height: 448px;cursor: default;z-index: 9199;background: transparent url(/room/images/hongBaoClick.png) no-repeat 0 0;left:50%;margin-left:-207px;top:50%;margin-top:-224px;display:none}
						#hongBaoClick .bagbtn {position: absolute;bottom: 35px;left: 79px;}
						#hongBaoClick .sethongBao, #hongBaoClick .lookmainbag {
    width: 112px;
    height: 40px;
    line-height: 40px;
    float: left;
    color: #ef412f;
    text-align: center;
    font-size: 18px;
    cursor: pointer;
}
						#hongBaoClick .lookmainbag {
							margin-left: 38px;
						}
						#hongBaoClick .redbagclose {
							top: 305px;
							right: 50px;
						}
						.redbagclose {
							position: absolute;
							top: 10px;
							right: 10px;
							z-index: 1;
							background: url(/room/images/redbagclose.png) no-repeat;
							height: 14px;
							width: 14px;
							cursor: pointer;
						}
						#lookredbag{height: 485px;position: absolute;text-align: center;
    width: 347px;
    box-shadow: 0 0 10px 0 rgba(0,0,0,0.5);
    border-radius: 8px;
    cursor: default;
	background: url(/room/images/successredbag.png) no-repeat 0 0;}
	#lookredbag .img, #lookthisbag .img {
    margin: 0 auto;
    border-radius: 50%;
    height: 65px;
    width: 65px;
    margin-top: 45px;
    overflow: hidden;
    box-shadow: 0 2px 3px 0 rgba(0,0,0,0.5);
}
	#lookredbag .money, #lookthisbag .money {
    font-size: 24px;
    height: 42px;
    line-height: 42px;
	color: #fc4c4c;
}
#lookredbag .HBlist{
    background-color: #fff;
    border-bottom: 1px solid #dbdbdb;
    border-top: 1px solid #dbdbdb;
    overflow-y: auto;
    margin-top: 0;
    padding: 5px 20px;
	height: 275px
}
 .HBlist .list-info{    height: 35px;
    padding: 10px 0;
    border-bottom: 1px solid #dbdbdb;}
	 .HBlist .list-info img{
		float: left;
    margin-right: 5px;
    border-radius: 50%;
	 }
 .HBlist .list-info .list-right{
float: left;
    height: 35px;
    font-size: 14px;
    color: #333;
    text-align: left;
    width: 250px;
 }
  .HBlist .list-info span{float:right}
  .HBlist .list-info .time{    font-size: 12px;
    color: #999;}
						</style>
						<div id="hongBaoClick" style=""><div class="bagbtn"><div class="sethongBao">发红包</div><div class="lookmainbag">我的红包</div></div><div class="redbagclose" onClick="closehongbao()"></div></div>
<script>
function showhongbao(){
	$('#hongBaoClick').show();
}
function closehongbao(){
	$('#hongBaoClick').hide();
}
$('#hongBaoClick .lookmainbag').click(function(){
	closehongbao();
	
	$.get('/ajax.php?act=wohongbao&_t=' + new Date().getTime(), function(res){
		if(res['code'] != 0){
			alert(res['msg']);
			return;
		}
		
		
	var html = '<div id="lookredbag">' +
					'<div class="img"><img src="/face/img.php?t=p1&u='+My.chatid+'" width="65" height="65"></div>' +
					'<p style="margin-top:10px;font-size: 14px;color:#000">'+My.name+'共收到<span style="color:red;">'+res['count']+'</span>个红包</p>' +
					'<div class="money bagred">'+res['money']+'<span style="color:#333;font-size:14px;">元</span></div>' +
					'<div class="HBlist">';
					for(var inx in res['data']){
						html += '<div class="list-info">' +
							'<img src="/face/img.php?t=p1&u='+res['data'][inx]['uid']+'" width="35" height="35">' +
							'<div class="list-right">' +
								'<div>'+res['data'][inx]['nickname']+'<span>'+res['data'][inx]['money']+'元</span></div>' +
								'<div class="time">'+res['data'][inx]['time']+'</div>' +
							'</div>' +
						'</div>';
					}
					html += '</div>' +
				'</div>';
	openWin(1, false, html, 347, 485);
		
		
	}, 'json');

});
$('#hongBaoClick .sethongBao').click(function(){
	closehongbao();
	redbag_choice();
});
</script>
</body>
</html>