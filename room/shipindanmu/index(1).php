<?php include_once('module/init.php'); ?>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
	<meta name="renderer" content="webkit">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<title><?=$cfg['config']['title']?></title>
	<meta content="<?=$cfg['config']['keys']?>" name="keywords">
	<meta content="<?=$cfg['config']['dc']?>" name="description">
	<meta property="qc:admins" content="310104337127367555100637572775" />
	<meta name="360-site-verification" content="e9f57ba7ed296be93ddcb05e3a1eabab" />
	<link rel="shortcut icon" type="image/x-icon" href="<?=$cfg['config']['ico']?>" />
	<link href="skins/qqxiaoyou/css.css" rel="stylesheet" type="text/css"/>
	<link href="images/layim.css" rel="stylesheet" type="text/css"  />
	<script src="script/jquery.min.js"></script>
	<script src="script/jquery.zclip.min.js"></script>
	<script src="script/jquery.cookie.js"></script>
	<script src="script/json3.min.js"></script>
	<script src="script/socket.io.js"></script>
	<script src="script/MSClass.js"></script>
	<script src="script/layer.js"></script>
	<script src="script/jquery.nicescroll.min.js"></script>
	<script src="script/pastepicture.js"></script>
	<script src="script/swfobject.js" type="text/javascript"></script>
	<script src="script/function.js?<?=time()?>" type="text/javascript"></script>
	<script src="script/init.js?<?=time()?>" type="text/javascript"></script>
	<script src="script/scroll.js" type="text/javascript"></script>
	<script src="script/jquery.rotate.min.js" type="text/javascript"></script>
	<script src="script/rotate.js?<?=time()?>" type="text/javascript"></script>
	<script src="script/sidebar.js" type="text/javascript"></script>
	<script src="script/device.min.js" type="text/javascript"></script>
	<script>
		if (device.mobile()) {
			window.location = 'm/index.php?rid=<?=$rid?>';
		}
		var UserList;
		var ToUser;
		var ToTeacher;
		var ToTeacherName;
		var VideoLoaded=false;
		var My={dm:'<?=$_SERVER['HTTP_HOST']?>',rid:'<?=$rid?>',roomid:'<?=$_SERVER['HTTP_HOST']?>/<?=$rid?>',chatid:'<?=$userinfo['uid']?>',name:'<?=$userinfo['username']?>',nick:'<?=$userinfo['nickname']?>',sex:'<?=$userinfo['sex']?>',age:'0',qx:'<?=check_auth('room_admin')?'1':'0'?>',ip:'<?=$onlineip?>',vip:'<?=$userinfo['gid']?>',color:'<?=$userinfo['gid']?>',cam:'0',state:'<?=$userinfo['state']?>',mood:'<?=$userinfo['mood']?>',rst:'<?=time()?>',camState:'1',key:'<?=connectkey()?>'}
		var RoomInfo={loginTip:'<?=$cfg['config']['logintip']?>',Msglog:'<?=$cfg['config']['msglog']?>',msgBlock:'<?=$cfg['config']['msgblock']?>',msgAudit:'<?=$cfg['config']['msgaudit']?>',defaultTitle:document.title,MaxVideo:'10',VServer:'<?=$cfg['config']['vserver']?>',VideoQ:'',TServer:'<?=$ts[0]?>',TSPort:'<?=$ts[1]?>',PVideo:'<?=$cfg['config']['defvideo']?>',AutoPublicVideo:'0',AutoSelfVideo:'0',type:'1',PVideoNick:'<?=$cfg['config']['defvide0nick']?>',OtherVideoAutoPlayer:'<?=$cfg['config']['livetype']?>',livefp1:'<?=$cfg['config']['livefp1']?>',livefp2:'<?=$cfg['config']['livefp2']?>',r:'<?=$cfg['config']['rebots']?>'}
		var grouparr=new Array();
		<?=$grouparr?>
		var remark = new Array();
		<?=$remark?>
		var ReLoad;
		var isIE=document.all;
		var aSex=['<span class="sex-womon"></span>','<span class="sex-man"></span>',''];
		var aColor=['#FFF','#FFF','#FFF'];
		var bg_img = '<?=$cfg['config']['bg']?>';
		var msg_unallowable="<?=$cfg['config']['msgban']?>";
		var room_state = parseInt("<?=$cfg['config']['state']?>");
		var lastmsgid = '<?=$lastmsgid?>';
	</script>
	<?php
		//禁言
		$query=$db->query("select * from {$tablepre}send where (username='{$userinfo[username]}' or ip='{$onlineip}') limit 1");
		while($row=$db->fetch_row($query)){
			echo "<script>remove_auth('msg_send');</script>";
		}
	?>
</head>
<body onresize="OnResize()" onUnload="OnUnload()">

	<div id="UI_MainBox" style="background: #408080 url(<?php echo empty($_COOKIE['bg_img']) ? $cfg['config']['bg'] : $_COOKIE['bg_img']; ?>) no-repeat scroll 0 0/100% 100%;">
	
		<?php include_once('module/header.php'); ?>
		
		<?php include_once('module/left.php'); ?>
	  
		<?php
			if($cfg['config']['showvideo'] == '1') {
				include_once('module/right.php');
			}		
		?>
		
		<?php include_once('module/center.php'); ?>

	</div>

	<?php include_once('module/footer.php'); ?>

</body>
</html>