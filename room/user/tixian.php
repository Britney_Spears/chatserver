<?php

require_once 'header.php';

$query = $db->query("select phone,gold from {$tablepre}members where uid=" . $_SESSION['login_uid']);
$row = $db->fetch_row($query);
$phone = $row['phone'];
$gold = !empty($row['gold']) && floatval($row['gold']) > 0 ? $row['gold'] : 0;

switch($type){
    case "tixian":
        $db->query("insert into {$tablepre}alipay(`uid`, `alipay_name`, `buyer_email`, `total_fee`, `pay_time`, `type`) value('".$_SESSION['login_uid']."', '".$username."', '".$account."', '".$jine."', '".time()."', '1')");
        echo "<script>alert('提交成功');location.href='tixian.php';</script>";
    break;
}

?>
<div id="content">
    <div class="container">
        <div class="crumbs">
            <a href="#">首页</a>><a href="#">提现</a>
        </div>
        <div class="biaoge">
            <div class="biaoge_inner">
                <div class="width980 align_l">
                    <div class="czHomeBox">
                        <div class="wrap_ml44">
                            <div class="wrap745">
                                <div class="nav">
                                    <div class="tit">
                                        提现流程：
                                    </div>
                                    <div class="number">
                                        <span class="img">
                                            <img src="public/1.png" alt="">
                                        </span>
                                        <span class="on">
                                            填写提现金额
                                        </span>
                                        <span class="arrow">
                                            <img src="public/arrow_on.gif" alt="">
                                        </span>
                                    </div>
                                    <!--<div class="number">
                                        <span class="img">
                                            <img src="public/2.png" alt="">
                                        </span>
                                        <span class="off">
                                            确认提现信息
                                        </span>
                                        <span class="arrow">
                                            <img src="public/arrow_off.gif" alt="">
                                        </span>
                                    </div>-->
                                    <div class="number">
                                        <span class="img">
                                            <img src="public/2.png" alt="">
                                        </span>
                                        <span class="off">
                                            提现申请成功，请耐心等候到账
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear" style="height: 4px;"></div>
                    <div class="m_tlr">
                        <h1 class="tixian_title">
                            申请提现
                        </h1>
                        <div class="tixian_infor">
                            <p>
                                <strong class="f_14 f_666">
                                    账户余额：
                                </strong>
                                <span id="UserWithDrawalNew1_spanAccountRemainMoney" class="f_family22">
                                    <?php echo $gold; ?>
                                </span>
                                元
                                <!--<a href="#" target="_blank" class="f_grey_line">
                                    资金明细查询
                                </a>-->
                            </p>
                            <p class="m_t">
                                <strong class="f_14 f_666">
                                    可提现额：
                                </strong>
                                <span id="UserWithDrawalNew1_spanCanWithDrawalRemainMoney" class="f_family22 f_ff6600">
                                    <?php echo $gold; ?>
                                </span>
                                元
                                <span class="f_999">
                                    充值金额及退回押金需48小时后方能提现（卡类充值金额不能提现）。
                                </span>
                            </p>
                            <div class="tixian_block">
                                <form onsubmit="return check_form()" method="post" action="?type=tixian">
                                    <ul>
                                        <li>
                                            <span class="f_14">
                                                提现方式：
                                            </span>
                                            <!--<input type="radio" name="type" value="1" class="tixian_type" checked="checked" />-->
                                            支付宝
                                            <!--<input type="radio" name="type" value="2" class="tixian_type" />
                                            银行卡-->
                                        </li>
                                        <li id="bank_input" style="display:none">
                                            <span class="f_14">
                                                银行卡开户行：
                                            </span>
                                            <input type="text" name="bank" id="bank" value="" class="input_bor">
                                        </li>
                                        <li>
                                            <span class="f_14">
                                                收款人：
                                            </span>
                                            <input type="text" name="username" id="username" value="" class="input_bor" />
                                        </li>
                                        <li>
                                            <span class="f_14">
                                                收款账号：
                                            </span>
                                            <input type="text" name="account" id="account" value="" class="input_bor" />
                                        </li>
                                        <li class="m_b_5">
                                            <span class="f_14">
                                                提现金额：
                                            </span>
                                            <input type="text" name="jine" id="jine" value="1" class="input_bor" />
                                        </li>
                                        <li>
                                            <span class="f_14">
                                                &nbsp;
                                            </span>
                                            <span class="fed_formtips_error" id="spanMoneyTips" style="">
                                                <s class="ico_error_1"></s>
                                                请输入提现金额！
                                            </span>
                                        </li>
                                        <li>
                                            <span class="f_14">
                                                &nbsp;
                                            </span>
                                            <span class="mt10">
                                                <input type="submit" value="下一步" class="input_button82" />
                                                &nbsp;&nbsp;
                                            </span>
                                        </li>
                                    </ul>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/js/jquery-1.7.2.js"></script>
<script language='JavaScript'>
    $(".tixian_type").click(function() {
        if ($(this).val() == '2') {
            $("#bank_input").show();
        } else {
            $("#bank_input").hide();
        }
    });
    function check_form() {
        if($('input[name="username"]').val() == '') {
            alert('请输入收款人');
            return false;
        }
        if($('input[name="account"]').val() == '') {
            alert('请输入收款帐号');
            return false;
        }
        if($('input[name="jine"]').val() == '') {
            alert('请输入提现金额');
            return false;
        }
        if($('input[name="jine"]').val() > parseInt($('.f_ff6600').html())) {
            alert('余额不足');
            return false;
        }
    }
</script>

<?php include 'footer.php'; ?>