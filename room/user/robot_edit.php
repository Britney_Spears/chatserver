<?php

	require_once '../../include/common.inc.php';

	$uid = $_SESSION['login_uid'];
	$uname = $_SESSION['login_user'];
	if($_SESSION['login_gid'] == '2' && $_SESSION['login_gid'] == '3' && $_SESSION['login_gid'] == '4') {
		echo "<script>alert('没有权限');</script>";
		header("location:../../logging.php?rid=" . $rid);exit();
	}

	if($act=="robot_edit"){
		list($up_hour, $up_minute, $up_second) = explode(':', $online);
		list($down_hour, $down_minute, $down_second) = explode(':', $downline);
		$tuser = $uname;
		//robot_edit($id,$username,$nickname,$gid,$tuser,$week,$up_hour, $up_minute, $up_second,$down_hour, $down_minute, $down_second);
		$db->query("update {$tablepre}robots set username='$username',nickname='$nickname',gid='$gid',tuser='$tuser',week='$week',up_hour='$up_hour',up_minute='$up_minute',up_second='$up_second',down_hour='$down_hour',down_minute='$down_minute',down_second='$down_second' where uid='$robotid'");
		header('location:robots.php');
	} else if($act=="robot_add") {
		list($up_hour, $up_minute, $up_second) = explode(':', $online);
		list($down_hour, $down_minute, $down_second) = explode(':', $downline);
		$tuser = $uname;
		$db->query("insert into {$tablepre}robots (username, nickname, gid, tuser, week, up_hour, up_minute, up_second, down_hour, down_minute, down_second) values('".$username."', '".$nickname."','".$gid."', '".$tuser."','".$week."','".$up_hour."','".$up_minute."','".$up_second."','".$down_hour."','".$down_minute."','".$down_second."')");
		header('location:robots.php');
	}
	
	if($robotid) {
		$robot = $db->fetch_row($db->query("select * from {$tablepre}robots where uid='{$robotid}'"));
	}

	include 'header.php';

?>
	<link href="/admin/assets/css/dpl-min.css" rel="stylesheet" type="text/css" />
	<link href="/admin/assets/css/bui-min.css" rel="stylesheet" type="text/css" />
	<link href="/admin/assets/css/page-min.css" rel="stylesheet" type="text/css" />
	<style>
		.result-tab .inputTd{text-align: left; padding-left: 10px;}
		.inputTd input{heigth: 26px; line-height: 26px; padding-left: 5px; border: 1px solid #CCC;}
		.bui-calendar-header,.bui-calendar-panel{display: none;}
		.bui-calendar-footer{padding-top: 10px;}
	</style>
	<div>
		<?php
			$query = $db->query("select * from {$tablepre}auth_group order by id desc");
			while($row = $db->fetch_row($query)){
				$group.='<option value="'.$row[id].'">GID:'.$row[id].'-'.$row[title].'</option>';
			}
			if(stripos(auth_group($_SESSION['login_gid']),'users_group')===false){
				$group='';
			}
		?>
		<div id="content">
			<div class="container">
				<div class="biaoge">
					<div class="result-wrap">
						<form name="myform" id="myform" action="" method="post">                
							<div class="result-content">
								<table class="result-tab" width="100%" style="text-align: center;">
									<tr>
										<td width="120" class="tableleft" style="width:70px;">用户名：</td>
										<td class="inputTd"><input name="username" type="text" id="username" style="width:260px;" value="<?php echo $robot['username']; ?>"/></td>
									</tr>
									<tr>
										<td width="80" class="tableleft" style="width:70px;">昵称：</td>
										<td class="inputTd"><input name="nickname" type="text" id="nickname" style="width:350px;" value="<?php echo $robot['nickname']; ?>"/></td>
									</tr>
									<tr>
										<td width="80" class="tableleft">用 户 组：</td>
										<td class="inputTd">
											<select name="gid" id="gid" >
												<?php echo $group; ?>
											</select>
										</td>
									</tr>
									<!--<tr>
										<td width="80" class="tableleft">推广用户：</td>
										<td><input name="tuser" type="text" id="tuser" style="width:350px;" value="{}"/></td>
									</tr>-->
									<tr>
										<td width="80" class="tableleft">在线时段：</td>
										<td class="inputTd">
											<input name="week" type="text" id="week" style="width:150px;" value="<?php echo $robot['week']; ?>"/>&nbsp;&nbsp;周(一,二,三,四,五,六,日)  填1,2,3,4,5,6,7逗号隔开<br/>
											<input name="online" type="text" id="online" class="calendar" style="width:150px;" value="<?php echo $robot['up_hour'] ? ($robot['up_hour'] . ':' . $robot['up_minute'] . ':' . $robot['up_second']) : ''; ?>"/>上线时间<br/>
											<input name="downline" type="text" id="downline" class="calendar" style="width:150px;" value="<?php echo $robot['down_hour'] ? ($robot['down_hour'] . ':' . $robot['down_minute'] . ':' . $robot['down_second']) : ''; ?>"/>下线时间
										</td>
									</tr>
									<tr>
										<td></td>
										<td class="inputTd">
											<button type="submit" style="width: 80px; height: 30px; font-size: 14px; background: #4d7496; border-radius: 5px; cursor: pointer;">确定</button>
											<button type="button" style="width: 80px; height: 30px; font-size: 14px; background: #4d7496; border-radius: 5px; cursor: pointer;" onclick="history.go(-1);">取消</button>
											<input type="hidden" name="act" value="<?php echo $robotid ? 'robot_edit' : 'robot_add'; ?>">
											<input type="hidden" name="robotid" value="<?php echo $robotid; ?>">
										</td>
									</tr>
								</table>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="/admin/assets/js/jquery-1.8.1.min.js"></script> 
	<script type="text/javascript" src="/admin/assets/js/bui.js"></script> 
	<script type="text/javascript" src="/admin/assets/js/config.js"></script> 
	<script>
		BUI.use('bui/calendar',function(Calendar){
		  var datepicker1 = new Calendar.DatePicker({
			trigger:'#online',
			showTime : true,
			dateMask : 'HH:MM:ss',
			autoRender : true
		  });
		  var datepicker1 = new Calendar.DatePicker({
			trigger:'#downline',
			showTime : true,
			dateMask : 'HH:MM:ss',
			autoRender : true
		  });
		});
	</script>

<?php include 'footer.php'; ?>
