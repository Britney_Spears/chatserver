<?php

	require_once '../../include/common.inc.php';
	//是否登录
	if($_SESSION['login_gid'] == 0){
		header("location:../../logging.php?rid=" . $cfg['config']['id']);exit();
	}

	$current_url = $_SERVER['REQUEST_URI'];
	$url_arr = explode('/', $current_url);
	$url = $url_arr[3];
	$title = '';
	switch($url) {
		case 'am_tixian.php':
			$title = '我的提现';
			break;
		case 'dingdan.php':
			$title = '我的充值';
			break;
		case 'pay.php':
			$title = '充值';
			break;
		case 'tixian.php':
			$title = '提现';
			break;
		case 'consume.php':
			$title = '我的消费';
			break;
		case 'robots.php':
		case 'robot_edit.php':
			$title = '机器人';
			break;
		default:
			$title = '我的资料';
			break;
	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="shortcut icon" type="image/x-icon" href="<?=$cfg['config']['ico']?>" />
	<title><?php echo $title . ' - ' . $cfg['config']['title']; ?></title>
	<link href="public/css.css" rel="stylesheet" type="text/css">
	<script src="../script/jquery.min.js" type="text/javascript" charset="utf-8"></script>
</head>
<body style="background:#ededed">	
	<header class="header navbar navbar-fixed-top" role="banner">
    	<div class="touxiang fr">
        	<div class="wenben fl"><?=$_SESSION['login_user']?><p><span><a href="../../logging.php?act=logout&rid=<?=$_SESSION['uid']?>">退出</a></span></p></div>
        	<div class="tupian fl"><img src="../../face/img.php?t=p1&u=<?=$_SESSION['uid']?>" width="100"></div>            
        </div>
		<div class="container">
        	<div class="logo fl"><img src="<?=$cfg['config']['logo']?>" width="250"></div>
			<ul class="nav navbar-nav navbar-left hidden-xs hidden-sm">
				<li>
					<a href="#">您好，<?=$_SESSION['login_user']?></a>
				</li>
				<li class="dropdown">
					<a href="/room/index.php?rid=<?=$rid?>" class="dropdown-toggle" data-toggle="dropdown">
						网站首页
					</a>
				</li>
			</ul>
        </div>        
    </header>
	<div id="sidebar" class="sidebar-fixed">
		<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;">
			<div id="sidebar-content" style="overflow: hidden; width: auto; height: 100%;">          
				<ul id="nav">		
					<li <?php if($url == 'index.php' || empty($url)) echo ' class="current"'; ?>>
						<a href="index.php">
							<i><img src="public/ziliao.png" width="20"></i>
							我的资料
						</a>
					</li>            
					<!--<li>
						<a href="editpwd.php">
							<i><img src="public/mima.png" width="20"></i>
							修改密码<i class="arrow icon-angle-left"></i>
						</a>              
					</li>-->
					<li <?php if($url == 'consume.php') echo ' class="current"'; ?>>
						<a href="consume.php">
							<i><img src="public/xiaofei.png" width="20"></i>
							我的消费<i class="arrow icon-angle-left"></i>
						</a>              
					</li>
					<li <?php if($url == 'am_tixian.php') echo ' class="current"'; ?>>
						<a href="am_tixian.php">
							<i><img src="public/xiaofei.png" width="20"></i>
							我的提现<i class="arrow icon-angle-left"></i>
						</a>              
					</li>
					<li <?php if($url == 'dingdan.php') echo ' class="current"'; ?>>
						<a href="dingdan.php">
							<i><img src="public/xiaofei.png" width="20"></i>
							我的充值<i class="arrow icon-angle-left"></i>
						</a>              
					</li>
					<li <?php if($url == 'pay.php') echo ' class="current"'; ?>>
						<a href="pay.php">
							<i><img src="public/chongzhi.png" width="20"></i>
							充值<i class="arrow icon-angle-left"></i>
						</a>              
					</li>            
					<li <?php if($url == 'tixian.php') echo ' class="current"'; ?>>
						<a href="tixian.php">
							<i><img src="public/tixian.png" width="20"></i>
							提现<i class="arrow icon-angle-left"></i>
						</a>             
					</li>
					<?php
						if($_SESSION['login_gid'] == '2' || $_SESSION['login_gid'] == '3' || $_SESSION['login_gid'] == '4') {
					?>
					<li <?php if(stripos($url, 'robot') !== FALSE) echo ' class="current"'; ?>>
						<a href="robots.php">
							<i><img src="public/tixian.png" width="20"></i>
							机器人<i class="arrow icon-angle-left"></i>
						</a>             
					</li>
					<?php } ?>
				</ul>          
			</div>
		 </div>      
     </div>