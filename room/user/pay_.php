<?php

include 'header.php';

?>

<!--
https://doc.open.alipay.com/doc2/detail?treeId=62&articleId=103738&docType=1
https://b.alipay.com/order/productDetail.htm?productId=2015110218012942&tabId=4#ps-tabinfo-hash
-->

	<div id="content">
     	<div class="container">
        	<div class="crumbs">
            	<a href="#">首页</a>&gt;<a href="#">充值</a>
			</div>
			<div class="biaoge">
				<div class="biaoge_inner">
					<div class="wrap745" style="width: 740px;">
						<!--topbar-->
						<div class="nav">
							<div class="tit">
								充值流程：</div>
							<div class="number">
								<span class="img">
									<img src="public/1.png"></span> <span class="on">填写金额</span>
								<span class="arrow">
									<img src="public/arrow_on.gif"></span>
							</div>
							<div class="number">
								<span class="img">
									<img src="public/2.png"></span> <span class="off">充值结果
								</span>
								<!--<span class="arrow">
									<img src="public/arrow_off.gif">
								</span>-->
							</div>
							<!--<div class="number">
								<span class="img">
									<img src="public/3.png"></span><span class="off">进入网上银行充值
								</span>
							</div>-->
						</div>    
						<!--end-->
						<div id="ChargeEBank1_divBankMsg" style="display: none;" class="paycue"></div>
						<div class="box clearfix"></div>
						
						<div class="box mt10 clearfix clear">
							<div class="left mt10" style="margin-left: 4px;">
								<span class="red">*</span>充值金额：&nbsp;<br>
								<br>
								<!--<span id="spanUse1" style="display: none;"><span class="red">*</span>充值用途：&nbsp;</span>-->
							</div>
							<div class="right mt10" id="debug_0">		
								<form action="alipay/alipayapi.php" class="alipayform" method="post">
									<input type="hidden" name="WIDout_trade_no" id="out_trade_no" value="<?php echo date('YmdHis') . time(); ?>">
									<input type="hidden" name="WIDsubject" value="充值">
									<input type="text" name="WIDtotal_fee" id="WIDtotal_fee" value="1" class="input"> &nbsp;元&nbsp;  
									<input type="submit" value="" id="check_submit" style="display: none;">
								</form>
							</div>
							<!--<div class="clear"></div>
							<div class="left mt10">            
								<span id="spanUse" style="display: none;"><span class="red">*</span>充值用途：&nbsp;</span>
							</div>-->
							<div id="debug_1" class="right mt10">          
								<span class="mt10">
									<input name="ChargeEBank1$btnNext" value="下一步" class="input_button82" type="submit" onclick="$('#check_submit').click();">&nbsp;&nbsp;
									<a href="#" target="_blank"><img src="" id="imgebank" style="display: none;" alt=""></a><br>
								</span>
								<br>
							</div>
						</div>
					</div>                	
				</div>
			</div>
        </div>
	</div>
<?php include 'footer.php'; ?>