<?php

    include 'header.php';
    $uid = $_SESSION['login_uid'];
    $query = $db->query("select * from {$tablepre}alipay where uid='{$uid}' and type = '1'");
    
?>

<div id="content">
    <div class="container">
        <div class="crumbs">
            <a href="#">首页</a>><a href="#">我的提现</a>
        </div>
        <div class="biaoge">
            <div class="result-wrap">
                <form name="myform" id="myform" method="post">                
                    <div class="result-content">
                        <table class="result-tab" width="100%" style="text-align: center;">
                            <tbody>
                                <tr>                            
                                    <th width="25">序号</th>
                                    <th>提现金额</th>
                                    <th>账号</th>
                                    <th>收款人</th>
                                    <th>提现时间</th>
                                    <th>状态</th>                            
                                </tr>
                                <?php
                                    while($row = $db->fetch_row($query)) {
                                ?>
                                <tr>
                                    <td><?php echo $row['id']; ?></td>
                                    <td><?php echo $row['total_fee']; ?></td>
                                    <td><?php echo $row['buyer_email']; ?></td>
                                    <td><?php echo $row['alipay_name']; ?></td>
                                    <td><?php echo date('Y-m-d H:i:s', $row['pay_time']); ?></td>
                                    <td>
                                        <?php
                                    
                                            switch($row['state']) {
                                                case "0":
                                                    echo '等待审核中';
                                                    break;
                                                case "0":
                                                    echo '已通过';
                                                    break;
                                                case "2":
                                                    echo '未通过';
                                                    break;
                                            }

                                        ?>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <div class="list-page">
                            <style>
                                .page { list-style:none; margin:10px;}
                                .page li { float:left; margin-right:10px}
                            </style>
                            <!--<ul class="page">
                                    <li>
                                            <a href="dingdan.php?page=1">首页</a> 
                                    </li>
                                    <li>
                                            <a  href="dingdan.php?page=1">尾页</a> 
                                    </li>
                            </ul>-->
                        </div>
                    </div>
                </form>
            </div>                
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>