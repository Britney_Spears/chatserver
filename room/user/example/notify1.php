<?php
ini_set('date.timezone','Asia/Shanghai');
error_reporting(E_ERROR);

require_once "../lib/WxPay.Api.php";
require_once '../lib/WxPay.Notify.php';
require_once 'log.php';

//初始化日志
$logHandler= new CLogFileHandler("../logs/".date('Y-m-d').'.log');
$log = Log::Init($logHandler, 15);

class PayNotifyCallBack extends WxPayNotify
{
	//查询订单
	public function Queryorder($transaction_id)
	{
		$input = new WxPayOrderQuery();
		$input->SetTransaction_id($transaction_id);
		$result = WxPayApi::orderQuery($input);
		Log::DEBUG("query:" . json_encode($result));
		if(array_key_exists("return_code", $result)
			&& array_key_exists("result_code", $result)
			&& $result["return_code"] == "SUCCESS"
			&& $result["result_code"] == "SUCCESS")
		{
			return true;
		}
		return false;
	}
	
	//重写回调处理函数
	public function NotifyProcess($data, &$msg)
	{
		require_once "../../../../include/common.inc.php";
		Log::DEBUG("call back:" . json_encode($data));
		$notfiyOutput = array();
		
		if(!array_key_exists("transaction_id", $data)){
			$msg = "输入参数不正确";
			return false;
		}
		//查询订单，判断订单真实性
		if(!$this->Queryorder($data["transaction_id"])){
			$msg = "订单查询失败";
			return false;
		}
		
		
		Log::DEBUG($data['result_code']);
		if($data['result_code'] == 'SUCCESS') {
			$total_fee = $data['total_fee'];
			$total_fee = $total_fee / 100;
			$payTime = strtotime($data['time_end']);
			$isSuccess = $data['return_code'] == 'SUCCESS' ? 'T' : 'F';
			$sql = "insert into {$tablepre}alipay(`uid`,`is_success`,`out_trade_no`,`total_fee`,`trade_status`,`pay_time`,`type`) values('{$_SESSION['login_uid']}', '{$isSuccess}', '{$data['"out_trade_no']}', '{$total_fee}', '{$data['return_code']}', '{$payTime}', '1')";
			$db->query($sql);
			
			Log::DEBUG($sql);
		}
		
		return true;
	}
}

Log::DEBUG("begin notify");
$notify = new PayNotifyCallBack();
$notify->Handle(false);


$data = array("appid"=> "wxa923173fc767d43a",
    "attach"=> "直播室充值",
    "bank_type"=> "CFT",
    "cash_fee"=> "1",
    "fee_type"=> "CNY",
    "is_subscribe"=> "N",
    "mch_id"=> "1388580302",
    "nonce_str"=> "9wves37e906ff3y0ec22dpo1jot2cas8",
    "openid"=> "onMJUw92-47W-Du2sgdzJF1n-CCA",
    "out_trade_no"=> "138858030220161115141103",
    "result_code"=> "SUCCESS",
    "return_code"=> "SUCCESS",
    "sign"=> "185EB7E2EBBDFECFD4936D1A2C9A13E0",
    "time_end"=> "20161115141114",
    "total_fee"=> "1",
    "trade_type"=> "NATIVE",
    "transaction_id"=> "4005042001201611159809509282"
);
$notify->NotifyProcess($data);