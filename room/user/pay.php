<?php

include 'header.php';

//error_reporting(E_ALL);
//ini_set('display_errors', '1');

if(!empty($act) && $act == 'wxPay') {
	$setting = array();
	$query = $db->query("select * from {$tablepre}setting;");
	while($row = $db->fetch_row($query)) {
		$setting[$row['idx']] = $row['item'];
	}	
	define("WXPAY_APPID", $setting['APPID']);
	define("WXPAY_MCHID", $setting['MCHID']);
	define("WXPAY_KEY", $setting['KEY']);
	require_once "lib/WxPay.Api.php";
	require_once "example/WxPay.NativePay.php";
	require_once "example/log.php";

	$notify = new NativePay();
	//$url1 = $notify->GetPrePayUrl("123456789");

	$input = new WxPayUnifiedOrder();
	$input->SetBody("直播室充值");
	$input->SetAttach($_SESSION['login_uid']);
	$input->SetOut_trade_no(WxPayConfig::MCHID.date("YmdHis"));
	$input->SetTotal_fee($total_fee*100);
	$input->SetTime_start(date("YmdHis"));
	$input->SetTime_expire(date("YmdHis", time() + 600));
	$input->SetGoods_tag("直播室充值");
	$input->SetNotify_url("http://" . $_SERVER['HTTP_HOST'] . "/room/user/notify.php");
	$input->SetTrade_type("NATIVE");
	$input->SetProduct_id("123456789");

	$result = $notify->GetPayUrl($input);
	$url2 = $result["code_url"];
}

?>

<!--
https://doc.open.alipay.com/doc2/detail?treeId=62&articleId=103738&docType=1
https://b.alipay.com/order/productDetail.htm?productId=2015110218012942&tabId=4#ps-tabinfo-hash
-->

	<div id="content">
     	<div class="container">
        	<div class="crumbs">
            	<a href="#">首页</a>&gt;<a href="#">充值</a>
			</div>
			<div class="biaoge">
				<div class="biaoge_inner">
					<!--方式一：支付宝支付-->
					<div class="wrap745" style="width: 740px;">
						<!--topbar-->
						<div class="nav">
							<div class="tit" style="color: red;">方式一：支付宝支付</div>
							<!--<div class="tit">充值流程：</div>
							<div class="number">
								<span class="img">
									<img src="public/1.png"></span> <span class="on">填写金额</span>
								<span class="arrow">
									<img src="public/arrow_on.gif"></span>
							</div>
							<div class="number">
								<span class="img">
									<img src="public/2.png"></span> <span class="off">充值结果
								</span>
							</div>-->
							<!--<div class="number">
								<span class="img">
									<img src="public/3.png"></span><span class="off">进入网上银行充值
								</span>
							</div>-->
						</div>    
						<!--end-->
						<div class="box clearfix"></div>
						
						<div class="box mt10 clearfix clear">
							<div class="left mt10" style="margin-left: 4px;">
								<span class="red">*</span>充值金额：&nbsp;<br>
								<br>
								<!--<span id="spanUse1" style="display: none;"><span class="red">*</span>充值用途：&nbsp;</span>-->
							</div>
							<div class="right mt10" id="debug_0">		
								<form action="alipay/alipayapi.php" class="alipayform" method="post">
									<input type="hidden" name="WIDout_trade_no" id="out_trade_no" value="<?php echo date('YmdHis') . time(); ?>">
									<input type="hidden" name="WIDsubject" value="充值">
									<input type="text" name="WIDtotal_fee" id="WIDtotal_fee" value="1" class="input"> &nbsp;元&nbsp;  
									<input type="submit" value="" id="check_submit" style="display: none;">
								</form>
							</div>
							<!--<div class="clear"></div>
							<div class="left mt10">            
								<span id="spanUse" style="display: none;"><span class="red">*</span>充值用途：&nbsp;</span>
							</div>-->
							<div id="debug_1" class="right mt10">          
								<span class="mt10">
									<input name="ChargeEBank1$btnNext" value="下一步" class="input_button82" type="submit" onclick="$('#check_submit').click();">&nbsp;&nbsp;
									<a href="#" target="_blank"><img src="" id="imgebank" style="display: none;" alt=""></a><br>
								</span>
								<br>
							</div>
						</div>
					</div> 


					<!--方式二：微信支付-->
					<div class="wrap745" style="width: 740px; margin-top: 150px;">
						<!--topbar-->
						<div class="nav">
							<div class="tit" style="color: red;">方式二：微信支付</div>
						</div>    
						<!--end-->
						<div class="box clearfix"></div>
						
						<div class="box mt10 clearfix clear">
							<div id="chongzhi" style="<?php echo !empty($act) ? 'display: none;' : ''; ?>">
								<div class="left mt10" style="margin-left: 4px;">
									<span class="red">*</span>充值金额：&nbsp;<br>
									<br>
									<!--<span id="spanUse1" style="display: none;"><span class="red">*</span>充值用途：&nbsp;</span>-->
								</div>
								<div class="right mt10" id="debug_2">		
									<form action="?" class="alipayform" method="get">
										<input type="hidden" name="act" value="wxPay">
										<input type="text" name="total_fee" id="WXtotal_fee" value="0.01" class="input"> &nbsp;元&nbsp; 
										<input type="submit" value="" id="wx_check_submit" style="display: none;">
									</form>
								</div>
								<div id="debug_3" class="right mt10">          
									<span class="mt10">
										<input id="wxBtn" value="下一步" class="input_button82" type="submit" onclick="$('#wx_check_submit').click();">&nbsp;&nbsp;
									</span>
									<br>
								</div>
							</div>
							<div id="saoma" class="right" style="margin-left: 135px; <?php echo empty($act) ? 'display: none;' : ''; ?>"">			
								<img alt="模式二扫码支付" id="wxQrcode" src="http://paysdk.weixin.qq.com/example/qrcode.php?data=<?php echo urlencode($url2); ?>" style="width:150px;height:150px;"/>
							</div>
							<!--<div class="clear"></div>
							<div class="left mt10">            
								<span id="spanUse" style="display: none;"><span class="red">*</span>充值用途：&nbsp;</span>
							</div>-->
						</div>
					</div> 
					
				</div>
			</div>
        </div>
	</div>
	<script>
		/*$('#wxBtn').click(function() {
			$.get('wxpay/example/native.php', {'Total_fee': $('#WXtotal_fee').val()}, function(ret) {
				if(ret) {
					$('#chongzhi').hide();
					$('#saoma').show();
					$('#wxQrcode').attr('src', ret);
				} else {
					alert('验证码获取失败');
				}
			});
		});*/
	</script>
<?php include 'footer.php'; ?>