<?php
require_once '../../include/common.inc.php';
$echo="";
switch($act){
	case "login1":
		$msg=user_login($username,$password);
		//exit($msg);
		if($msg===true){
			setcookie("username", $username, gdate()+315360000);
			header("location:/room/m/notice.php?referer=login");
		} else {
			$echo ="<script>layer.msg('{$msg}',{shift: 6});</script>";
		}
	break;
	case "login2":
		$msg=user_login($username,$password);
		if($msg===true){
			setcookie("username", $username, gdate()+315360000);
			header("location:http://chat2.oumei88.com/room");
		} else {
			$echo ="<script>layer.msg('{$msg}',{shift: 6});</script>";
		}
	break;
	//case "login":
	//	$msg=user_login($username,$password);
	//	//exit($msg);
	//	if($msg===true && $_SESSION['login_gid'] > 1){
	//		setcookie("username", $username, gdate()+315360000);
	//		header("location:http://chat2.oumei88.com/room");
	//	} elseif($msg===true && $_SESSION['login_gid'] < 2) {
	//		setcookie("username", $username, gdate()+315360000);
	//		header("location:./room");
	//	} else {
	//		$echo ="<script>layer.msg('{$msg}',{shift: 6});</script>";
	//	}
	//break;
	case "logout":
		unset($_SESSION['login_uid']);
		unset($_SESSION['login_user']);
		session_destroy(); 
		header("location:index.php");
	break;
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>登录 <?=$cfg['config']['title']?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
<link rel="shortcut icon" type="image/x-icon" href="<?=$cfg['config']['ico']?>" />
<meta content="<?=$cfg['config']['keys']?>" name="keywords">
<meta content="<?=$cfg['config']['dc']?>" name="description">
<link href="../../images/base.css" rel="stylesheet" type="text/css"  />
<link href="../../images/login.css" rel="stylesheet" type="text/css"  />
<script src="../../room/script/jquery.min.js"></script>
<script src="../../room/script/layer.js"></script>
<script src="../../room/script/device.min.js"></script>
</head>

<body>
<!--<script>if (device.mobile()){window.location = './room/minilogin.php';}</script>-->
<div class="mainBg">
<div class="logoBar w1000 m0 cf">
		<div class="logo fl">
			<a href="javascript:;"><img src='<?=$cfg['config']['logo']?>' border=0></a>
		</div>
		<p class="fr" style="height:50px;">
            
            <a href="javascript://"  class="regBtn trans03" style="margin-top:10px;background: #ee6229;color:#fff;" >客服中心</a>
            
		</p>
	</div>
    <div class="loginBox f14">
		<div class="loginMain cf">
            
    <div class="loginLeft fl h330">
        <div class="loginTitle">
            <p class="userLogin"></p>
        </div>
        <form action="?act=login1" method="post" enctype="application/x-www-form-urlencoded"  name="loginform"  id="login_form" class="loginForm" >
        <div class="loginForm" style="padding-top:0; border: 1px solid #CCC;">
            <ul style="display: inline-block; margin-bottom: 15px;" class="login_op">
                <li style="padding: 10px 20px; cursor: pointer;">普通直播室登录</li>
                <li style="padding: 10px 20px; border: 1px solid #CCC; cursor: pointer; background-color: #CCC;">高级直播室登录</li>
            </ul>
            <script>
                $(document).ready(function() {
                    $(document).on('click', '.login_op li', function() {
                        $('.login_op li').each(function() {
                            $(this).css('background-color', '#CCC');
                            $(this).css('border', '1px solid #CCC');
                        });
                        $(this).css('background-color', '');
                        $(this).css('border', '');
                        if($(this).html() == '普通直播室登录') {
                            $('#login_form').attr('action', '?act=login1');
                            $('.tiyan.f14').show();
                        } else {
                            $('#login_form').attr('action', '?act=login2');
                            $('.tiyan.f14').hide();
                        }
                    });
                });
            </script>
            <div class="oneLine cf">
                <span class="itemName">用户名</span>
                <span class="star">&nbsp;</span>
                <span>
                    <input name="username" type="text" value="<?=$_COOKIE['username']?>"></span>
                <span class="tishi" style="display: none"><i class="dui"></i></span>
            </div>
            <div class="oneLine cf">
                <span class="itemName">密码</span>
                <span class="star">&nbsp;</span>
                <span>
                    <input name="password" type="password" ></span>
                <span class="tishi" style="display: none"><i class="cuo">密码错误</i></span>
            </div>
            
            
            <div class="oneLine cf">
                <span class="itemName">&nbsp;</span>
                <span class="star">&nbsp;</span>
                <div class="ie7LoginWidth dib cf">
                    <p class="pr">
                        <button id="lnkLogin" class="loginBtn trans03" style="border:0px;" type="submit">登录</button>
                      <a class="tiyan f14" href="/room/m">游客体验</a>
                    </p>
                    <p class="pt20 cf w">
                        <span class="fl"><a href="javascript:layer.msg('忘记密码？请联系客服！');" class="forgot">忘记密码？</a></span>
                        <span class="fr"></span>
                    </p>
                </div>
            </div>
        </div>
        </form>
        
    </div>
    <div class="loginRight fl">
        <div class="loginTitle">
            <p class="toReg"></p>
        </div>
        <a href="register.php" class="regBtn mt40 trans03">立即注册</a>
        <!--
        <p class="c999 pt30 f12">使用社交账号登录</p>
        <p>
            <a  class="qq_login" href=""></a>
        </p>
        <p>

            <a  class="weibo_login" href=""></a>
        </p>
		-->
    </div>


		</div>
		<div class="loginBt"></div>
	</div>
    <div class="footer w" >
    <div class="fLinks cf">
        <div class="w1000 m0">
            <span class="fl">友情链接：</span>
            <ul class="fl">
                <li><a href="http://www.95599.cn/cn/" target="_blank">农业银行</a></li>
                <li><a href="http://www.icbc.com.cn/icbc/" target="_blank">工商银行</a></li>
                <li><a href="http://www.ccb.com/" target="_blank">建设银行</a></li>
                <li><a href="http://www.boc.cn" target="_blank">中国银行</a></li>
            </ul>
        </div>
        <div>
            
            
        </div>
    </div>
    <div class="copy">
        <div id="MainContent_footer_divFooterLog" class="w1000 m0 cf">
            
<div class="fl">
				<p class="cfff">投资有风险，入市须谨慎</p>
				<p><span ><?=tohtml($cfg['config']['copyright'])?></span>   </p>
				<p>
                    </p>
			</div>
            <p id="MainContent_footer_pLogo4RJ" class="fr pt10">&nbsp;</p>
        </div>
    </div>
</div>
</div>

<?=$echo?>
</body>
</html>
