//http://layer.layui.com/mobile/
Number.prototype.Left = function(strlen){
	if(this.toString().length >= strlen) {
		return this.toString().substr(0, strlen);
	} else {
		var str = this.toString();
		for(var i=0; i < strlen - str.length; i++)
			str = "0" + str;
		return str;
	}
}
var socket;
var page_fire
//window.onbeforeunload=function(){ws.send("Logout");}
function OnSocket(){    
	socket = io("ws://" + RoomInfo.TServer + ":" + RoomInfo.TSPort);
	//socket = io("http://" + RoomInfo.TServer + ":" + RoomInfo.TSPort + '/r' + My.rid);
    socket.on('connect', function() {
        onConnect();
    });
    socket.on('message', function(data) {
		//console.log(data);
        WriteMessage(data);
    });
}
function Datetime(tag)
{
	return new Date().toTimeString().split(' ')[tag];	
}
function MsgAutoScroll(){
	if($('#publicChat').find(".msg_li").length>30){$('#publicChat').find(".msg_li:first").remove();}
	//$('#publicChat').animate({scrollTop:$('#publicChat')[0].scrollHeight}, 1000);
	$('#msgcontent').scrollTop($('#msgcontent').height() + $('#publicChat').height())
}
UserList=(function(){
	var list=[];
	list['ALL']={sex:2,chatid:'ALL',nick:'大家'}
	return{
		List:function(){return list},
		init:function(){
			list['ALL']={sex:2,chatid:'ALL',nick:'大家'}
			var request_url='../../ajax.php?act=getrlist&rid='+My.rid+'&r='+RoomInfo.r+'&'+Math.random() * 10000;
			$.ajax({type: 'get',dataType:'text',url: request_url,
				success:function(data){
					WriteMessage(data);
			}});
		},
		get:function(id){return list[id];},
		add:function(id,u){
			if($("#"+id).length >0)return;
			list[id]=u;
		},
		del:function(id,u){
			if(id==My.chatid)return;
			delete(list[id]);
		}
	}
})();
function MsgShow(str,type){
	//alert(str);
	str="<div class='msg_li'><div style='clear:both;'></div>"+str+"</div>";
	$("#publicChat").append(str);

	MsgAutoScroll();
}
function MsgSend(){
	if (!check_auth("msg_send")) {
		layer.open({
			content: '没有发言权限！',
			style: 'background-color:#CCC; color:#000; border:none;',
			time: 2
		});
		return false;
	}
	var msgid=randStr()+randStr();
	var str=encodeURIComponent($("#messageEditor").html());
	if(str.length < 1){
		return;
	}
	/*
	if(device.iphone())str+=" [IPhone用户]";
	else if(device.ios())str+=" [IOS用户]"
	else if(device.ipad())str+=" [IPad用户]";
	else if(device.mobile())str+=" [手机用户]";		*/
	
	//ws.send('SendMsg=M=ALL|false|color:#000|'+msgid+'_+_'+str);
	socket.emit('message', 'SendMsg=M=ALL|false|color:#000|'+msgid+'_+_'+str);
	
	//console.log(str);
	PutMessage(My.rid,My.chatid,'ALL',My.nick,'大家','false','',str,msgid);
	$("#messageEditor").html("");
	//$("#messageEditor").focus();
}
function InsertImg(err, url){
	if (!check_auth("msg_send")) {
		layer.open({
			content: '没有发言权限！',
			style: 'background-color:#CCC; color:#000; border:none;',
			time: 2
		});
		return false;
	}
	var msgid=randStr()+randStr();
	var str = encodeURIComponent('<img src="'+url+'" style="max-width:100%" />');
	
	socket.emit('message', 'SendMsg=M=ALL|false|color:#000|'+msgid+'_+_'+str);
	
	PutMessage(My.rid,My.chatid,'ALL',My.nick,'大家','false','',str,msgid);
}
function randStr(){
	return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
}
function PutMessage(rid,uid,tid,uname,tname,privacy,style,str,msgid){
    var msgtip = "";
	var reg = new RegExp(msg_unallowable, "ig");
    if (RoomInfo.Msglog == '0' || reg.test(decodeURIComponent(str))) {
        return;
	}
	if(My.state == '3') {
		msgtip = "msgtip=7&";
	}
	var request_url='../../ajax.php?act=putmsg';
	var postdata=msgtip+'msgid='+msgid+'&uname='+uname+'&tname='+tname+'&muid='+uid+'&rid='+rid+'&tid='+tid+'&privacy='+privacy+'&style='+style+'&msg='+str+'&'+Math.random() * 10000;
	
	$.ajax({type: 'POST',url:request_url,data:postdata});
}
function Mkick(adminid,rid,ktime,cause) {
	$.ajax({type: 'get',dataType:'json',url: '../../ajax.php?act=kick&aid='+adminid+'&rid='+rid+'&ktime='+ktime+'&cause='+cause+'&u='+My.name+'&'+Math.random() * 10000,
		success:function(data){
			//alert(data);
			if(data.state=="yes"){
			location.href="../error.php?msg="+encodeURI('你被踢出！并禁止'+ktime+'分钟内登陆该房间！<br>原因是 '+cause+'');
			}
		}
	});
}
String.prototype.str_replace=function(t){
	var str=this;
	var reg = new RegExp(msg_unallowable, "ig");
	if(reg.test(str)&&!check_auth("room_admin"))
		return "含敏感关键字，内容被屏蔽";
	str= str.replace(/<\/?(?!br|img|font|p|span|\/font|\/p|\/span)[^>]*>/ig,'').replace(/\r?\n/ig,' ').replace(/(&nbsp;)+/ig," ").replace(/(=M=)+/ig,"").replace(/(|)+/ig,"").replace(/(SendMsg)/ig,'');
	if(!check_auth("room_admin"))str=str.replace(reg,'**')
	return str;
	};

function check_auth(auth) {
	var auth_rules = grouparr[My.color].rules;
	if(auth_rules.indexOf(auth)>-1)return true;
	else false;
}
function remove_auth(auth) {
    grouparr[My.color].rules = grouparr[My.color].rules.replace(auth, "");
}
function kick_send(adminid) {
    $.ajax({type: 'get', dataType: 'json', url: '../../ajax.php?act=kick_send&aid='+adminid+'&u=' + encodeURIComponent(My.name) + '&' + Math.random() * 10000,
        
    });
}
function FormatMsg(Msg) {
	var str="";
	var User=UserList.get(Msg.ChatId);
	var toUser=UserList.get(Msg.ToChatId);
	var date= Datetime(0);
	var IsPersonal='';
	if(typeof(User)=='undefined'||typeof(toUser)=='undefined')return false;
	if(Msg.IsPersonal=='true' && toUser.chatid!='ALL') IsPersonal='[私]';

	if(Msg.Txt.indexOf('COMMAND')!=-1) {
		var Txt=decodeURIComponent(Msg.Txt.str_replace());
		var command=Txt.split('_+_');
		switch(command[1]) {
            case 'send_Msgblock':
                if (My.chatid == toUser.chatid) {
                    remove_auth('msg_send');
					layer.open({
						content: '你已被禁言！',
						style: 'background-color:#CCC; color:#000; border:none;',
						time: 2
					});
					kick_send(Msg.ChatId);
                }
                return 'yyc';
			case 'msgAudit':
				$('#'+command[2]).show();
				MsgAutoScroll();
				$.ajax({type: 'get',url: '../../ajax.php?act=msgblock&s=0&msgid='+command[2]});
			break;
			case 'msgBlock':
				$('#'+command[2]).remove();
				MsgAutoScroll();
				$.ajax({type: 'get',url: '../../ajax.php?act=msgblock&s=1&msgid='+command[2]});
			break;
			case 'rebotmsg':
				var msg={};
				msg.ChatId=command[2];
				msg.ToChatId='ALL';
				msg.IsPersonal='false';
				msg.Txt=command[3]+"_+_"+command[4];
				msg.Style=Msg.Style;
				MsgShow(FormatMsg(msg),0);
			break;		
			
			case 'kick':
				if(My.chatid==toUser.chatid){				
					Mkick(Msg.ChatId,My.roomid,command[2],command[3]);				
				}
			break;
			case 'sendredbag'://红包
				var User = UserList.get(command[3]);
				var str = "";
					str+="<div class='msg'>";
					str+="	<div class='msg_head'><img src='../../face/img.php?t=p1&u="+User.chatid+"'><span class='u'> "+User.nick+"：</spa></div>";
					str+=" <div class='msg_content' style='background: none;'><img onclick='getRedbag(this)' style='width:186px;float:left;cursor:pointer' src='images/redbag_open.png' rel='" + command[2] + "'></div>";
					str+="</div>";
				break;
            case 'getredbag'://红包
                var u = UserList.get(command[2]);
                var str = '<p class="tip_msg"><font class="u" color="' + aColor[u.sex] + '" onclick="ToUser.set(\'' + u.chatid + '\',\'' + u.nick + '\')">' + u.nick + '</font><font color="' + aColor[u.sex] + '">领取红包</font></p>';
                break;
            case 'sleepwalk':
				var user = UserList.get(command[2]);
				user.state = '3';
                break;
			case "sendgift":
				var u = UserList.get(command[2]);
				//var s = UserList.get(command[3]);
				var sid = command[3];
				var sname = command[8];
				var str = '<p><font class="u" color="' + aColor[u.sex] + '" onclick="ToUser.set(\'' + u.chatid + '\',\'' + u.nick + '\')">' + u.nick + '</font> 向 <font class="u" color="" onclick="ToUser.set(\'' + sid + '\',\'' + sname + '\')">' + sname + '</font> 送了' + command[6] + ' <img src="' + command[4] + '" height="50" width="50"/> (<span style="color:red"><b>' + command[5] + '</b></span>份) <font color="' + aColor[u.sex] + '">赠言： ' + command[7] + '</font> <font class="date">【' + date + '】</font></p>';
				break;
		}
	} else {		
		var reg = new RegExp(msg_unallowable, "ig");
		if (reg.test(decodeURIComponent(Msg.Txt))) {
			var Txt = decodeURIComponent(Msg.Txt);// + '[屏蔽]';
		} else {
			var Txt = decodeURIComponent(Msg.Txt.str_replace());
		}
		
		var msgid="";
		if(Txt.indexOf('_+_')>-1){
			var t=Txt.split('_+_');
			msgid= t[0];
			Txt=t[1];
		}	
	
		var msgAuditShow = '';
		if (RoomInfo.msgAudit == "1") {
			msgAuditShow = 'style="display:none"';
			if (User.chatid == My.chatid)
				msgAuditShow = "";

		}		
		
		var html="";
			html+="<div class='msg_li'><div class='"+(User.chatid == My.chatid ? 'msg my' : 'msg')+"' id='[msgid]'  " + msgAuditShow + ">";
			html+="	<div class='msg_head'><img src='../../face/img.php?t=p1&u=[uid]'></div>";
			html+=" <div class='contentwrap'><span class='u'> [nick]</spa><div class='msg_content'>[txt]</div></div>";
			html+="</div></div>";
		var html2="";
			html2+="<div class='msg_li'><div class='"+(User.chatid == My.chatid ? 'msg my' : 'msg')+"' id='[msgid]' " + msgAuditShow + ">";
			html2+="	<div class='msg_head'><img src='../../face/img.php?t=p1&u=[uid]'></div>";
			html2+=" <div class='contentwrap'><span class='u'>[nick]</span><span class='dui'> 对</span><span>[tnick]</span><span class='shuo'> 说</span><div class='msg_content'>[txt]</div></div>";
			html2+="</div></div>";
			
		if(toUser.chatid!="ALL"){
			str=html2.replace("[txt]",Txt).replace("[tnick]",toUser.nick).replace("[nick]",User.nick).replace("[tuid]",toUser.chatid).replace("[uid]",User.chatid).replace("[msgid]",msgid);
		}else{
			str=html.replace("[txt]",Txt).replace("[nick]",User.nick).replace("[uid]",User.chatid).replace("[msgid]",msgid);
		}
	}
	return str;
	
}
function WriteMessage(txt){
	//if(txt.indexOf('SendMsg')!=-1)
	//console.log(txt);
	var Msg;
	try{
		Msg=eval("("+txt+")");
	} catch(e) {return;}
	if(Msg.stat!='OK') {
		if(Msg.stat=="MaxOnline"){
			document.body.innerHTML='<div  style="font-size:12px; text-align:center; top:100px; position:absolute;width:100%">O.O 对不起，服务端并发数已满！请您联系管理员对系统扩容升级！<br><br></div>';
			return;
		}
		return ;
	}
	switch(Msg.type) {
		case "Ulogin":
			var U=Msg.Ulogin;
			var vip_msg="到来";
			var date= Datetime(0);
			var str='<span class="info">欢迎：<font class="u" onclick="ToUser.set(\''+U.chatid+'\',\''+U.nick+'\')">'+U.nick+'</font>  <font class="date">'+date+'</font></span>';
			if(My.chatid!=U.chatid){
				UserList.add(U.chatid,U);
			}
			var type=0;
			if(U.chatid==My.chatid) type=2;
			//MsgShow(str,type);
			
			break;
		case "UMsg":
			var sendUid = Msg.UMsg.ChatId;
			var sendU = UserList.get(sendUid);
			if(sendU.state == 3 && sendU.chatid != My.chatid) {
				return;
			}
			var str=FormatMsg(Msg.UMsg);
			var reg = new RegExp(msg_unallowable, "ig");
			//console.log(reg.test(decodeURIComponent(Msg.UMsg.Txt)));
			if (reg.test(decodeURIComponent(Msg.UMsg.Txt))) {
				if(check_auth("room_admin") || My.chatid == Msg.UMsg.ChatId) {
					MsgShow(str, type);
				}
			} else {
				if(str != 'yyc') {
					MsgShow(str,1);
				}	
			}		
			break;
		case "UonlineUser":
			
			var onlineNum=Msg.roomListUser.length;
			for(i=0;i<onlineNum;i++) {
				var U=Msg.roomListUser[i];				
				UserList.add(U.chatid,U);
			}
			break;
		case "Ulogout":
			var U=Msg.Ulogout;
			var date= Datetime(0);
			var str='<span class="info">用户：'+U.nick+'   离开！ <font class="date">'+date+'</font></span>';
			//MsgShow(str,0);
			UserList.del(U.chatid,U);
			break;
		case "SPing":
			//alert(Msg.SPing.time);
			break;
		case "Sysmsg":
			alert(Msg.Sysmsg.txt);
			break;
	}
	
}
function OnInit(){
	//alert($(window).height());
	Init();
	if(RoomInfo.showvideo == '1') {
		showLive();
	}
	OnSocket();
	$("#sendBtn").click(function(){MsgSend();});
	//$('#publicChat').height($(window).height()-45-$('#head_1').height());
	//$('#publicChat').niceScroll({cursorcolor:"#FFF",cursorwidth:"2px",cursorborder:"0px;"});
	
	setInterval("auto_speak()", 5000);
	if(RoomInfo.showvideo == '1' && RoomInfo.state == 3 && My.chatid.indexOf('x') > -1) {
		setInterval("viewTime()", 1000);
	}
	$(".GetFolowers").click(function() {
        $(".abundantface").toggle();
    });
	$(".gift_close").click(function() {
        $(".giftList").toggle();
    });
	MsgAutoScroll();
}
function viewTime() {
	var view_time = viewtime;
    if(view_time < 0) {
        $('#colockbox').html('即将关闭');
        return;
    }
	var second = view_time % 60;
	var view_time = parseInt(view_time / 60);
	var minute = view_time % 60;
	var view_time = parseInt(view_time / 60);
	var hour = view_time % 24;
	var day = parseInt(view_time / 24);
	$('.second').html(second.Left(2));
	$('.minute').html(minute.Left(2));
	$('.hour').html(hour.Left(2));
	$('.day').html(day.Left(2));
	
    viewtime = viewtime - 1;
}
function onConnect() {
	setInterval("online('<?=$time?>')",3000);
	var str='Login=M='+My.roomid+'|'+My.chatid+'|'+My.nick+'|'+My.sex+'|'+My.age+'|'+My.qx+'|'+My.ip+'|'+My.vip+'|'+My.color+'|'+My.cam+'|'+My.state+'|'+My.mood + '|' + My.sleepwalk;
	//ws.send(str);
    socket.emit('message', str);
		
	if(typeof(UserList)!='undefined'){
		UserList.init();
	}
	//bt_fenping();
}

function auto_speak() {
	$.getJSON('../../ajax.php?act=auto_speak&rid=' + My.rid + '&_' + Math.random() * 10000, function(data) {
		if(data != '') {
			var User = data.user;
			var msgdate = data.msgdate;
			var Txt = data.msg;
			//str = '<div style="clear:both;"></div><div class="msg"><font class="date">' + msgdate + '</font><div class="msg_head"><img src="' + grouparr[User.color].ico + '" class="msg_group_ico" title="' + grouparr[User.color].title + "-" + grouparr[User.color].sn + '"></div><div class="msg_content"><div><font class="u" style="color:' + aColor[User.sex] + '" onclick="ToUser.set(\'' + User.chatid + '\',\'' + User.nick + '\')">' + User.nick + '</font>&nbsp;&nbsp; </div></div><div class="layim_chatsay1"><font>' + Txt + '</font></div></div><div style="clear:both;"></div>';
			
			var str = "";
				str+="<div class='msg'>";
				str+="	<div class='msg_head'><img src='../../face/img.php?t=p1&u="+User.chatid+"'><span class='u'> "+User.nick+"：</spa></div>";
				str+=" <div class='msg_content'>"+Txt+"</div>";
				str+="</div>";
			
			MsgShow(str, 0);
		}
	});
}
function XHConn() {
    var xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {
            try {
                xmlhttp = new XMLHttpRequest();
            } catch (e) {
                xmlhttp = false;
            }
        }
    }

    return xmlhttp;
}
function online(rst) {
    var xmlhttp = XHConn();
    var request_url = "../../ajax.php?act=online&rst=" + rst + "&num=" + (Object.getOwnPropertyNames(UserList.List()).length - 1) + '&rid=' + My.rid + "&" + Math.random() * 10000;
    try {
        xmlhttp.open('GET', request_url, true);
        xmlhttp.send(null);
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var re = eval("(" + xmlhttp.responseText + ")");
                if (re.state == "logout") {
                    layerMsg('你还没有登陆！');
                    location.href = '../minilogin.php'
                } else if (re.state == 'ologin') {
                    var str = '<div><img src="images/true.png" width="16" height="16"  style="vertical-align:text-bottom" /> ' + Datetime(0) + ' <br /> &nbsp;&nbsp;&nbsp;帐号其他地方登录或网络异常！统一帐号不能两个地方（浏览器登录）！ <a href="javascript:location.reload()" style="color:#0033FF;cursor:pointer">点击重新连接</a> </div>';
                    //MsgShow(str, 1);
                } else if (re.state == 'timeout') {
                    layerMsg('您的观看时间已到！', {shift: 6});
                    setTimeout(function () {
                        location.href = '../minilogin.php';
                    }, 3000);
                } else if (re.state == 'nogold') {
                    layerMsg('余额不足，请及时充值！', {shift: 6});
                    setTimeout(function () {
                        location.href = '../minilogin.php';
                    }, 3000);
                } else if(re.state == 'reload') {
					alert('re.state reload');
					location.reload();
				}
				
				$('#ssbscont h5').text(re.ssbs['txt']);
				$('#ssbscont div span').text(re.ssbs['url']);
            }
            return true;
        }
    } catch (e) {
        return true;
    }
}
function icon3() {
    $("#icon3").click(function() {
        var a = $("head  title").text();
		
        postToWb(a)
    })
}
function icon2() {
    $("#icon2").click(function() {
        var a = $("head  title").text();
        postToXinLang(a)
    })
}
function icon4() {
    $("#icon4").click(function() {
        var a = $("head  title").text();
        postToQzone(a)
    })
}
function postToXinLang(a) {
    window.open("http://v.t.sina.com.cn/share/share.php?title=" + encodeURIComponent(a) + "&url=" + encodeURIComponent(location.href) + "&rcontent=", "_blank", "scrollbars=no,width=600,height=450,left=75,top=20,status=no,resizable=yes")
}
function postToQzone(a) {
    var b = encodeURI(a),
    c = encodeURI(a),
    d = encodeURI(document.location);
    return window.open("http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?title=" + b + "&url=" + d + "&summary=" + c),
    !1
}
function postToWb(a) {//document.getElementById('video_box').contentWindow.document.getElementById('huya_video').contentWindow.document.getElementById('live_online').style.display='none';
    var b = encodeURI(a),
    c = encodeURI(document.location),
    d = encodeURI("appkey"),
    e = encodeURI(""),
    f = "",
    g = "http://v.t.qq.com/share/share.php?title=" + b + "&url=" + c + "&appkey=" + d + "&site=" + f + "&pic=" + e;
    window.open(g, "转播到腾讯微博", "width=700, height=680, top=0, left=0, toolbar=no, menubar=no, scrollbars=no, location=yes, resizable=no, status=no")
}
function showLive(){
	$("#video-win").html('<iframe height="215" width="100%" id="video_box" allowTransparency="true" marginwidth="0" marginheight="0"  frameborder="0" scrolling="no" src="../player.php?type=m&rid='+My.rid+'"></iframe>');
}
function Init(){
$("#video-flash").click(function(){showLive()});
$("body").click(function() { $(".setting-expression-layer").hide() });
$("#footer .smile img").click(function(a) {
    if (a.stopPropagation(), 0 == $("#expressions").find(".expr-tab").find("tr").length) {
        var b = '<tr><td><img src="/room/face/pic/m/kx.gif" alt="狂笑" title="狂笑" width="28" height="28" /></td>';
        b += ' <td><img src="/room/face/pic/m/jx.gif" alt="贱笑" title="贱笑" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/tx.gif" alt="偷笑" title="偷笑" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/qx.gif" alt="窃笑" title="窃笑" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/ka.gif" alt="可爱" title="可爱" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/kiss.gif" alt="kiss" title="kiss" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/up.gif" alt="up" title="up" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/bq.gif" alt="抱歉" title="抱歉" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/bx.gif" alt="鼻血" title="鼻血" width="28" height="28" /></td></tr>',
        b += '<tr><td><img src="/room/face/pic/m/bs.gif" alt="鄙视" title="鄙视" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/dy.gif" alt="得意" title="得意" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/fd.gif" alt="发呆" title="发呆" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/gd.gif" alt="感动" title="感动" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/glian.gif" alt="鬼脸" title="鬼脸" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/hx.gif" alt="害羞" title="害羞" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/jxia.gif" alt="惊吓" title="惊吓" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/zong.gif" alt="囧" title="囧" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/kl.gif" alt="可怜" title="可怜" width="28" height="28" /></td></tr>',
        b += '<tr><td><img src="/room/face/pic/m/kle.gif" alt="困了" title="困了" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/ld.gif" alt="来电" title="来电" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/lh.gif" alt="流汗" title="流汗" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/qf.gif" alt="气愤" title="气愤" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/qs.gif" alt="潜水" title="潜水" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/qiang.gif" alt="强" title="强" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/sx.gif" alt="伤心" title="伤心" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/suai.gif" alt="衰" title="衰" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/sj.gif" alt="睡觉" title="睡觉" width="28" height="28" /></td></tr>',
        b += '<tr><td><img src="/room/face/pic/m/tz.gif" alt="陶醉" title="陶醉" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/wbk.gif" alt="挖鼻孔" title="挖鼻孔" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/wq.gif" alt="委屈" title="委屈" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/xf.gif" alt="兴奋" title="兴奋" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/yw.gif" alt="疑问" title="疑问" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/yuan.gif" alt="晕" title="晕" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/zj.gif" alt="再见" title="再见" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/zan.gif" alt="赞" title="赞" width="28" height="28" /></td>',
        b += '<td><img src="/room/face/pic/m/zb.gif" alt="装逼" title="装逼" width="28" height="28" /></td></tr>',
        b += '<tr><td><img src="/room/face/pic/m/bd.gif" alt="被电" title="被电" width="22" height="22" /></td>',
        b += '<td><img src="/room/face/pic/m/gl.gif" alt="给力" title="给力" width="22" height="22" /></td>',
        b += '<td><img src="/room/face/pic/m/hjd.gif" alt="好激动" title="好激动" width="22" height="22" /></td>',
        b += '<td><img src="/room/face/pic/m/jyl.gif" alt="加油啦" title="加油啦" width="22" height="22" /></td>',
        b += '<td><img src="/room/face/pic/m/jjdx.gif" alt="贱贱地笑" title="贱贱地笑" width="22" height="22" /></td>',
        b += '<td><img src="/room/face/pic/m/lll.gif" alt="啦啦啦" title="啦啦啦" width="22" height="22" /></td>',
        b += '<td><img src="/room/face/pic/m/lm.gif" alt="来嘛" title="来嘛" width="22" height="22" /></td>',
        b += '<td><img src="/room/face/pic/m/lx.gif" alt="流血" title="流血" width="22" height="22" /></td>',
        b += '<td><img src="/room/face/pic/m/lgze.gif" alt="路过这儿" title="路过这儿" width="22" height="22" /></td></tr>',
        b += '<tr><td><img src="/room/face/pic/m/qkn.gif" alt="切克闹" title="切克闹" width="22" height="22" /></td>',
        b += '<td><img src="/room/face/pic/m/qgz.gif" alt="求关注" title="求关注" width="22" height="22" /></td>',
        b += '<td><img src="/room/face/pic/m/tzuang.gif" alt="推撞" title="推撞" width="22" height="22" /></td>',
        b += '<td><img src="/room/face/pic/m/ww.gif" alt="威武" title="威武" width="22" height="22" /></td>',
        b += '<td><img src="/room/face/pic/m/wg.gif" alt="围观" title="围观" width="22" height="22" /></td>',
        b += '<td><img src="/room/face/pic/m/xhh.gif" alt="笑哈哈" title="笑哈哈" width="22" height="22" /></td>',
        b += '<td><img src="/room/face/pic/m/zc.gif" alt="招财" title="招财" width="22" height="22" /></td>',
        b += '<td><img src="/room/face/pic/m/zf.gif" alt="转发" title="转发" width="22" height="22" /></td>',
        b += '<td><img src="/room/face/pic/m/zz.gif" alt="转转" title="转转" width="22" height="22" /></td></tr>',
        $("#expressions").find(".expr-tab").append(b),
        $("#expressions").find(".expr-tab").find("img").click(function() {
            var a = $(this).attr("src");
            $("#messageEditor").focus(),
            document.execCommand("insertImage", null, a),
            $(this).closest(".setting-expression-layer").hide()
        })
    } else $(".setting-expression-layer").toggle()
});
    $("#sharedBtn").click(function() {
        var a = '<div class="header">';
        a += '<div class="sharedClose" style=""></div>',
        a += "</div>",
        a += '<h2 style="margin-bottom: 12px;color: #000;font-weight: 400;">分享到:</h2>',
        a += "<ul>",
        a += '<li onclick="icon3()" id="icon3"><img src="images/tx.png" width="44px" height="44px">腾讯微博</li>',
        a += '<li onclick="icon2()" id="icon2"><img src="images/sina.png" width="44px" height="44px">新浪微博</li>',
        a += '<li onclick="icon4()" id="icon4"><img src="images/qz.png" width="44px" height="44px">QQ空间</li>',
        a += "</ul>",
        $("#shared").empty().append(a),
        $("#sharedWrap").show(),
        $("#shared").show(),
        $("#shared .sharedClose").click(function() {
            $("#shared,#sharedWrap").hide()
        })
    });
    /*$("nav li").click(function() {
        var e, a = !0,
        b = !0,
        c = !0,
        d = $(this).index();
        0 == d && ($(this).addClass("active").siblings().removeClass("active1").removeClass("active2"), $(this).find("span").addClass("activeCart"), $("nav li span").removeClass("activeCart1").removeClass("activeCart2").removeClass("activeCart")),
        1 == d && ($(this).addClass("active1").siblings().removeClass("active").removeClass("active2"), $(this).find("span").addClass("activeCart1"), $("nav li span").removeClass("activeCart").removeClass("activeCart2")),
        2 == d && ($(this).addClass("active2").siblings().removeClass("active").removeClass("active1"), $(this).find("span").addClass("activeCart2"), $("nav li span").removeClass("activeCart").removeClass("activeCart1")),
        1 == d && ($(this).addClass("active3").siblings().removeClass("active").removeClass("active2"), $(this).find("span").addClass("activeCart1"), $("nav li span").removeClass("activeCart").removeClass("activeCart2")),
        $(this).find("span").addClass("activeCart"),
        0 == d && a && (a = !1, e = $(window).height() - 310, $(".publicChat,#footer").show(), $('#msgcontent').css({'bottom':43, 'overflow-y': 'scroll'}), $(".myCustomer,#qqOnline,.kuaiXun,.tuiGuang").hide(), $(".publicChat").height() > e),
        1 == d && b && (b = !1, e = $(window).height() - 260,$('#msgcontent').css({'bottom':0, 'overflow-y': 'hidden'}) , $("#qqOnline").addClass("white"), $("#qqOnline").show(), $(".publicChat,.kuaiXun,#footer,.tuiGuang").hide(), $("#qqOnline ul").height() > e ),
        2 == d && c && (c = !1, $(".kuaiXun").show(),$('#msgcontent').css({'bottom':0, 'overflow-y': 'hidden'}) , $(".publicChat,#qqOnline,#footer,.tuiGuang").hide()),
        3 == d && c && (c = !1, e = $(window).height() - 250, $(".tuiGuang").height(e).show(), $(".kuaiXun,.publicChat,#qqOnline,#footer").hide())
    });*/
	$("nav li").click(function() {
		var d = $(this).index();
		$("nav li.active").removeClass('active');
		$(this).addClass('active');
		if(d == 0){
			$('#msgcontent').css({'bottom':43, 'overflow-y': 'scroll'});
			$(".publicChat,#footer").show();
			$("#qqOnline,.kuaiXun").hide();
		}else{
			$('#msgcontent').css({'bottom':0, 'overflow-y': 'hidden'});
			$(".publicChat,#qqOnline,.kuaiXun,#footer").hide();
			if(1 == d)
				$(".kuaiXun").show();
			if(2 == d)
				$("#qqOnline").show();
		}
	});
}
function layerMsg(msg) {
    layer.open({
        type: 0,
        content: msg,
        time: 2,
        style: 'background-color: rgba(0, 0, 0, 0.6); color: #ddd;',
    });
}
function getmsg(){
   $.ajax({type: 'get',dataType:'text',url: 'ajax.php?act=getmsg', success:function(data){                                 
		$("#publicChat").append(data);
		MsgAutoScroll();
	}});    
}
function redbag_choice() {
	var html = '<div id="redbag_choice">' +
					'<h1>发送红包</h1>' +
					'<ul>' +
					   '<li><Span>金 额：</span><input id="rendbag_total" placeholder="输入金额" />元</li>' +
					   '<li><Span>个 数：</span><input id="rendbag_num"  placeholder="输入个数"/>个</li>' +
					   '<li><a href="javascript:void(0)" class="send_redbag" onclick="sendRedbag()">立即塞钱</a></li>' +
					'</ul>' +
					'<span class="closeBtn" onclick="layer.closeAll();"><img src="../images/fancy_close.png"></span>' +
				'</div>';
	layer.open({
        type: 0,
		closeBtn: 1,
        content: html,
		offset: 'rb',
        time: 0,
        style: 'background-color: #FFF; color: #ddd;',
    });
}

function sendRedbag() {	
	var _num=$("#rendbag_num").val();
	var _total=$('#rendbag_total').val();
	if (_num < 1){
		layerMsg("红包个数至少为1", {shift: 0});
		return;
	}
	if(_total < 1) {
		layerMsg("单个红包金额最少为1", {shift: 0});
		return;
	}
	if (_total / _num < 0.01){
		layerMsg("红包个数与红包金额不合理，请核对后再发", {shift: 0});
		return;
	}
	$.ajax({
		url: "../../ajax.php?act=send_redbag&total=" + _total + '&num=' + _num,  
		type: "get",
		dataType: "json",
		error: function() {
		
		},  
		success: function(data){//如果调用php成功
			if(data.state == "ok"){
				var Msg = 'COMMAND_+_sendredbag_+_' + data.rbid + '_+_' + My.chatid;
				var str = 'SendMsg=M=ALL|false|color:#000|' + Msg;
				socket.emit('message', str);
				
				//SysSend.command('sendredbag', data.rbid + '_+_' + My.chatid);
				var msg = '<img onclick="getRedbag(this)" style="width:186px;float:left;cursor:pointer" src="images/redbag_open.png" rel="' + data.rbid + '">';
				var msgid = randStr() + randStr();
				//var str = 'SendMsg=M=ALL|false|color:#000|' + encodeURIComponent(msg.str_replace());
				PutMessage(My.rid, My.chatid, 'ALL', My.nick, 'ALL', 'false', '', msg.str_replace(), msgid);
				
				$("#rendbag_num").val('');
				$('#rendbag_total').val('');
				layer.closeAll();
				layerMsg("红包已发送", {shift: 0});
				
				//$('.bar_redbag span').html('￥'+data.yue);
			} else if(data.state == '-2') {
				layerMsg("请先登录", {shift: 0});
			} else if(data.state == '-3') {
				layerMsg("参数错误", {shift: 0});
			} else {
				layerMsg("对不起，您的账户余额不足", {shift: 0});
				//$("#redbag_pay_btn").click();
			}
		}
	});
}
function getRedbag(e){
	var rbid = $(e).attr('rel');
	$.ajax({
		url: "../../ajax.php?act=get_redbag&rbid=" + rbid,  
		type: "get",
		dataType: "json",
		success: function(data){//如果调用php成功
			layer.closeAll(); 
			if(data.state == "ok") {
				layerMsg("恭喜你，领取了红包￥"+data.money, {shift: 0});
				//SysSend.command('getredbag', My.chatid);
				var str = 'SendMsg=M=ALL|false|color:#000|COMMAND_+_getredbag_+_' + My.chatid;
				socket.emit('message', str);
				
				//$('.bar_redbag span').html('￥'+data.yue);
				setTimeout('getRedbagInfo(' + rbid + ')', 1000);
			} else if(data.state == "-2") {
				setTimeout('getRedbagInfo(' + rbid + ')', 1000);
				layerMsg("请登录之后再抢红包", {shift: 0});
			} else if(data.state == "-4") {
				setTimeout('getRedbagInfo(' + rbid + ')', 1000);
				layerMsg("您已经领过该红包领了，请下次再领吧", {shift: 0});
			} else if(data.state == "-5") {
				setTimeout('getRedbagInfo(' + rbid + ')', 1000);
				layerMsg("领取红包失败，请重试", {shift: 0});
			} else if(data.state == '-1') {
				setTimeout('getRedbagInfo(' + rbid + ')', 1000);
				layerMsg("红包领取完了，请下次再领吧", {shift: 0});
			}
		}
	});  
}
function getRedbagInfo(rbid){
	$.ajax({
		url: "../../ajax.php?act=get_redbag_info&rbid=" + rbid,  
		type: "get",
		dataType: "json",
		error: function(){
			
		},  
		success: function(data){//如果调用php成功
			if(data.state == "ok"){				
				var redbag = data.rb.redbag;
				var redbag_info = data.rb.redbag_info;
				
				var rb_list = '';
				for(i in redbag_info) {
					rb_list +=	'<tr>' +
										'<td style="width:15%"><img src="../../face/img.php?t=p1&u=' + redbag_info[i].uid + '"></td>' +
										'<td><span>'+redbag_info[i].nickname+'</span><p>'+redbag_info[i].rrtime+'</p></td>' +
										'<td style="width:30%">￥'+redbag_info[i].money+'</td>' +
									'</tr>';
				}
				var html = '<div id="redbag_info">' +
								'<div class="redbag_sender">' +
									'<div class="_avatar"><img src="../../face/img.php?t=p1&u=' + redbag.uid + '"/></div>' +
									'<div class="_info">' +
										'<span class="_info_total">共 ￥' + redbag.money + '</span>' +
										'<span class="_info_from">来自 ' + redbag.nickname + '</span>' +
									'</div>' +
								'</div>' +
								'<h1>已领取' + redbag.received + '/' + redbag.count + '个' + (redbag.count == redbag.received ? '，已领完' : '') + '</h1>' +
								'<table >' + rb_list + '</table>' +
								'<div class="redbag_get_bottom">收到的红包已存入<a href="../user" target="_blank" style="color:red">余额</a></div>' +
							'</div><span style="position: absolute;right: -15px;top: -15px;" class="closeBtn" onclick="layer.closeAll();"><img src="../images/fancy_close.png"></span>';
				
				//layer_open('', null, 400, 500, html);
				layer.open({
					type: 0,
					content: html,
					closeBtn: 1,
					time: 0,
					style: 'background-color: #FFF; color: #ddd;',
				});
			}
		}
	});	
}
function bt_sendflower(touid, touname) {
	ToTeacher = touid;
	ToTeacherName = touname;
	$('.giftList').show();
}
function sendgift(g,gname,gico,gmsg) {
    //var loadstr='<font color="red">服务器连接失败</font><br>';
	if(!check_login()){
		return;
	}
	var s = ToTeacher;
	var n = ToTeacherName;
    if (s == null || s == "") {
        alert("没有选择收礼人！");
        return false;
    }
    //layer_open_pc('赠送礼物', '../../sendgift.php?froom=froom&gid=' + g + '&sid=' + s + '&snick=' + n, 300, 200, null, false);
	$('.giftList').hide();
	var request_url='../../ajax.php?act=sendgift&sid='+s+'&gid='+g+'&num=1';
	$.ajax({type: 'get', dataType:'text', url: request_url,
		success:function(data){
			if(data == '1') {
				//SysSend.command('sendgift', My.chatid+'_+_'+s+'_+_{$ico}_+_1_+_{$gname}_+_{$msg}_+_{$snick}');
				
				var Msg = 'COMMAND_+_sendgift_+_' + (My.chatid+'_+_'+s+'_+_'+gico+'_+_1_+_'+gname+'_+_'+gmsg+'_+_'+n);
				var str = 'SendMsg=M=ALL|false||' + Msg;
				socket.emit('message', str);
			} else if(data == '0') {
				layerMsg("没有选择礼物！");
			} else if(data == '-1') {				
				layerMsg("金币不足，请先充值！");
			}
		}
	});
}
function check_login() {
	if(My.chatid.indexOf('x') > -1) {
		layerMsg("请先登录", {shift: 6});
		return false;
	}
	return true;
}
function layer_open() {
	layer.open({
		type: 0,
		content: html,
		time: 0,
		style: 'background-color: #FFF; color: #ddd;',
	});
}
function layer_open_pc(tit, url, w, h, html) {
    if (url == null) {
        layer.open({
            type: 1,
            title: tit != "" ? tit : false,
            shadeClose: true,
            shade: false,
            area: [w + 'px', h + 'px'],
            content: html, //iframe的url
            cancel: function (index) {
                layer.close(index);
            }
        });
    } else {
        layer.open({
            type: 2,
            title: tit != "" ? tit : false,
            maxmin: false,
            shadeClose: true, //开启点击遮罩关闭层
            area: [w + 'px', h + 'px'],
            offset: ['100px', ''],
            content: url,
            cancel: function (index) {
                layer.close(index);
            }
        });
    }
}

//http://layer.layui.com/mobile/