<?php

	require_once '../../include/common.inc.php';
	if($cfg['config']['loginguest'] == '0' && !isset($_SESSION['login_uid']))
		header('location:../../logging.php');

	require_once PPCHAT_ROOT.'./include/json.php';
	$json=new JSON_obj;
	
	if($act == 'auth'){
		if(empty($code))
			exit('error');
		
		$sccesstokenurl = "https://api.weixin.qq.com/sns/oauth2/access_token?appid={$app_id}&secret={$app_secret}&code={$code}&grant_type=authorization_code";
		$acctok = file_get_contents($sccesstokenurl);
		if($acctok)
			$acctok = json_decode($acctok);
		else
			exit('err');
		if($acctok->errcode)
			exit('get token err');
		
		$openid = $acctok->openid;
		$access_token = $acctok->access_token;
		$_SESSION['openid'] = $openid;
		
		$userinfourl = "https://api.weixin.qq.com/sns/userinfo?access_token={$access_token}&openid={$openid}";
		$userinfo = file_get_contents($userinfourl);
		
		if(!$userinfo)
			exit('get userinfo err');
		$userinfo = json_decode($userinfo);

		if($acctok->errcode)
			exit('err');
		
		$query = $db->query("select * from {$tablepre}members where openid = '{$openid}'");
		$row = $db->fetch_row($query);
		
		if(empty($row)){
			
			$db->query("update {$tablepre}members set openid = '{$openid}' where uid = '{$_SESSION['login_uid']}'");
			$db->query("update {$tablepre}memberfields set nickname = '{$userinfo->nickname}' where uid = '{$_SESSION['login_uid']}'");
		}else{
			
			$db->query("update {$tablepre}memberfields set nickname = '{$userinfo->nickname}' where uid = '{$row['uid']}'");
		}
		guestLogin();
		//if(!isset($_SESSION['login_uid'])){
		//	gusetLogin();
		//}
		
		
	}

	//游客登录
	if((!isset($_SESSION['login_uid']) || !isset($_SESSION['openid'])) && $cfg['config']['loginguest']=="1"){
		if(is_weixin() && !empty($app_id) && !empty($app_secret)){
			$redirecturi = 'http://' . $_SERVER['SERVER_NAME'] . '/room/m/index.php?act=auth';
			$redirecturi = urlencode($redirecturi);
			$authwx = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=' . $app_id . '&redirect_uri=' . $redirecturi . '&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect';
			header("Location: {$authwx}");
			exit;
		}
		if(!isset($_SESSION['login_uid']))
			guestLogin();
	}
	//房间状态
	if($cfg['config']['state']=='2' and $_SESSION['room_'.$cfg['config']['id']]!=true){
		header("location:login.php?1");exit();
	}
	if($cfg['config']['state']=='0'){
		exit("<script>location.href='../error.php?msg=系统处于关闭状态！请稍候……'</script>");
	}
	//是否登录
	if(!isset($_SESSION['login_uid'])){header("location:../../logging.php");exit();}

	//用户信息
	$uid=$_SESSION['login_uid'];
	$db->query("update {$tablepre}members set regip='$onlineip' where uid='{$uid}'");
	$userinfo=$db->fetch_row($db->query("select m.*,ms.* from {$tablepre}members m,{$tablepre}memberfields ms  where m.uid=ms.uid and m.uid='{$uid}'"));
	$_SESSION['login_gid'] = $userinfo['gid'];
	//游客
	if($_SESSION['login_gid'] == 0){
		$userinfo['username'] = $userinfo['nickname']=$_SESSION['login_nick'];
		$userinfo['sex'] = $_SESSION['login_sex'];
		$userinfo['uid'] = $_SESSION['login_guest_uid'];
	}

	//黑名单
	$query=$db->query("select * from {$tablepre}ban where (username='{$userinfo[username]}' or ip='{$onlineip}') and losttime>".gdate()." limit 1");
	while($row=$db->fetch_row($query)){
		exit("<script>location.href='error.php?msg=用户名或IP受限！过期时间".date("Y-m-d H:i:s",$row['losttime'])."'</script>");exit();
	}

	//用户组
	$query=$db->query("select * from {$tablepre}auth_group order by ov desc");
	while($row=$db->fetch_row($query)){
		$groupli.="<div id='group_{$row[id]}'></div>";
		$grouparr.="grouparr['{$row[id]}']=".json_encode($row).";\n";
		$group["m".$row[id]]=$row;
	}	

	$onlinetime = $db->fetch_row($db->query("select onlinetime from {$tablepre}onlinetime where rid='{$rid}' and lastactivityip='{$onlineip}'"));	
    if($_SESSION['login_gid'] == 0 && $cfg['config']['state'] == '3' && $onlinetime['onlinetime'] > ($cfg['config']['limit'] * 60)) {
        header("location:../logging.php?rid=" . $cfg['config'][id]);exit();
    }
	
	//聊天历史记录
	$query=$db->query("select * from {$tablepre}msgs where rid='".$cfg['config']['id']."' and p='false' and (state!='1' or state!='7') and `type`='0' order by id desc limit 0,20 ");

	while($row=$db->fetch_row($query)){
		if($row['state'] == '7' && $row['uid'] != $userinfo['uid']) {
			continue;
		}
		$row['msg']=str_replace(array('&amp;', '','&quot;', '&lt;', '&gt;'), array('&', "\'",'"', '<', '>'),$row['msg']);
		$class = $row[uid] == $userinfo['uid'] ? 'msg my' : 'msg';
		if($row[tuid]!="ALL"){
			$omsg="<div class='msg_li'><div style='clear:both;'></div><div class='{$class}' id='{$row[msgid]}'><div class='msg_head'><img src='../../face/img.php?t=p1&u={$row[uid]}'></div><div class='contentwrap'><span class='u'>{$row[uname]}<span class='dui'> 对 </span><span>{$row[tname]}</span><span class='shuo'> 说</span></span>
			 <div class='msg_content'>{$row[msg]}</div></div></div></div>".$omsg;
		} elseif(preg_match('/^<img onclick=\"getRedbag\(this\)\" style=\"width:186px;float:left;cursor:pointer\" src="images\/redbag_open.png\" rel=\"[0-9]{0,12}\">$/i',$row[msg])) {
			$omsg="<div class='msg_li'><div style='clear:both;'></div><div class='{$class}' id='{$row[msgid]}'><div class='msg_head'><img src='../../face/img.php?t=p1&u={$row[uid]}'></div><div class='contentwrap'><span class='u'> {$row[uname]}</span><div class='msg_content' style='background: none;'>{$row[msg]}</div></div></div></div>".$omsg;
		} else {
			$omsg="<div class='msg_li'><div style='clear:both;'></div><div class='{$class}' id='{$row[msgid]}'><div class='msg_head'><img src='../../face/img.php?t=p1&u={$row[uid]}'></div><div class='contentwrap'><span class='u'> {$row[uname]}</span><div class='msg_content'>{$row[msg]}</div></div></div></div>".$omsg;
		}
	}
	//获取最大ID
	$query=$db->query("select max(id+0) max_id from {$tablepre}msgs");
	$row=$db->fetch_row($query);

	$_SESSION['max_msg_id']=$row['max_id'];
	//其他处理
	$ts=explode(':',$cfg['config']['tserver']);


	if(!isset($_SESSION['room_'.$uid.'_'.$cfg['config'][id]])){
		$db->query("insert into  {$tablepre}msgs(rid,ugid,uid,uname,tuid,tname,mtime,ip,msg,`type`)values('{$cfg[config][id]}','{$userinfo[gid]}','{$userinfo[uid]}','{$userinfo[username]}','{$cfg[config][defvideo]}','{$cfg[config][defvideonick]}','".gdate()."','{$onlineip}','登陆直播间','3')");
		$db->query("update {$tablepre}memberfields set logins=logins+1 where uid='{$uid}'");	
		$_SESSION['room_'.$uid.'_'.$cfg['config'][id]]=1;
	}
	
	
	function is_weixin() { 
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) { 
        return true; 
    } return false; 
}

?>