<?php
	session_start();

	require_once './include/common.inc.php';
	$echo="";
	switch($act){
		case "reg":
			if($_SESSION['mcode'] != $mcode)
				exit("<script>alert('验证码错误！');location.href='?'</script>");
			$guestexp = '^Guest|'.$cfg['config']['regban']."Guest";
			if(preg_match("/\s+|{$guestexp}/is", $u))
				exit("<script>alert('用户名禁用！');</script>");
			
			$query=$db->query("select uid from {$tablepre}members where username='{$u}' limit 1");
			if($db->num_rows($query))exit("<script>alert('用户名已经被使用!换一个，如{$u}1985');location.href='?'</script>");
			
			$regtime=gdate();
			$p=md5($p);
			if(isset($_COOKIE['tg'])) {
				$tuser = $_COOKIE['tg'];
			} else {
				$tuser = rand_kefu();
			}
			if($cfg['config']['regaudit']=='1')$state='0';
			else $state='1';
			
			$db->query("insert into {$tablepre}members(rid,username,password,sex,email,regdate,regip,lastvisit,lastactivity,gold,realname,gid,phone,fuser,tuser,state)	values('$rid','$u','$p','2','$email','$regtime','$onlineip','$regtime','$regtime','0','$qq','1','$phone','$tuser','$tuser','$state')");
			$uid=$db->insert_id();
			$db->query("replace into {$tablepre}memberfields (uid,nickname)	values('$uid','$nick')	");
			
			
			$db->query("insert into  {$tablepre}msgs(rid,ugid,uid,uname,tuid,tname,mtime,ip,msg,type) values('{$cfg[config][id]}','1','{$uid}','{$u}','{$cfg[config][defvideo]}','{$cfg[config][defvideonick]}','".gdate()."','{$onlineip}','用户注册','2')
			");
			
			$msg=user_login($u,$p2);
			if($msg===true){
				setcookie("username", $username, gdate()+315360000);
			    header("location:./room/index.php?rid=" . $rid);
			}
			else{ $echo= "<script>layer.alert('注册成功！$msg',{ icon: 1});</script>";}
		break;
	}
	
	$row = $db->fetch_row($db->query("select * from {$tablepre}sms where id = '1'"));
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>注册 <?=$cfg['config']['title']?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
<link rel="shortcut icon" type="image/x-icon" href="<?=$cfg['config']['ico']?>" />
<meta content="<?=$cfg['config']['keys']?>" name="keywords">
<meta content="<?=$cfg['config']['dc']?>" name="description">
<link href="room/images/base.css" rel="stylesheet" type="text/css"  />
<link href="room/images/login.css" rel="stylesheet" type="text/css"  />
<script src="room/script/jquery.min.js"></script>
<script src="room/script/layer.js"></script>
<script>
	var vcode_bool = false;
	function change_vcode() {
		vcode_bool = false;
		$('#imgCode').attr('src', '/include/code.php?_' + new Date().getTime());
	}
	function check_vcode() {
		$.ajax({
			type: "GET",
			url: '../ajax.php?act=check_vcode',
			dataType: "json",
			data: {"vcode": $('#vcode').val()},
			success: function(d) {
				console.log(d);
				if (d == '1') {
					//layer.msg('验证码正确', {shift: 0});
					vcode_bool = true;
					setTimeout('$(".rotateMobile").hide()', 1000);
					setTimeout('sendSMS()', 2000);
				} else {
					layer.msg('验证码错误', {shift: 6});
					vcode_bool = false;
				}
			}
		});
	}
	var ds;
	function sendSMS() {
		var phone = $('input[name="phone"]').val();
		var res = validatemobile(phone);
		if(!res) {
			return;
		}
		if(!vcode_bool) {
			change_vcode();
			$('.rotateMobile').show();
			return;
		}
		$.ajax({
			type: "GET",
			url: 'sms.php',
			dataType: "json",
			data: {"phone": phone},
			success: function(d) {
				if (parseInt(d.code) > 0) {
					layer.msg('验证码发送成功', {shift: 0});
					ds = setInterval('countdown()', 1000);
				} else {
					layer.msg(d.msg, {shift: 6});
				}
			}
		});
	}
	function countdown() {
		var times = $('#sendsms').html();
		times = parseInt(times);
		var isint = isNaN(times);
		if(isint) {
		  times = 61;
		  $('#sendsms').removeAttr('onclick');
		}
		times = times - 1;
		if(times <= -1) {
			clearInterval(ds);
			$('#sendsms').html('发送验证码');
			$('#sendsms').attr('onclick', 'sendSMS()');
			vcode_bool = false;
		} else {
			$('#sendsms').html(times);
			$('#sendsms').attr('');
		}
	}
	function validatemobile(mobile) {
		if(mobile.length==0) {
			layer.msg('请输入手机号码', {shift: 6});
			return false;
		}    
		if(mobile.length!=11) {
			layer.msg('请输入有效的手机号码', {shift: 6});
			return false;
		}

		var myreg = /^1\d{10}$/;
		if(!myreg.test(mobile)) {
			layer.msg('请输入有效的手机号码', {shift: 6});
			return false;
		}
		return true;
	}

	function register() {
		if ($('#u').val() == "") {
			$('#u').focus().css({
				border: "1px solid red",
				boxShadow: "0 0 2px red"
			});
			$('#userCue').html("<font color='red'><b>×用户名不能为空</b></font>");
			return false;
		}
		if ($('#u').val().length < 2 || $('#u').val().length > 16) {
			$('#u').focus().css({
				border: "1px solid red",
				boxShadow: "0 0 2px red"
			});
			$('#userCue').html("<font color='red'><b>×用户名位2-16字符</b></font>");
			return false;
		}
		$.ajax({
			type: 'get',
			url: 'ajax.php?act=regcheck',
			data: "username=" + $("#u").val() + '&temp=' + new Date(),
			dataType: 'html',
			success: function(result) {
				if (result!='1') {
					$('#u').focus().css({
						border: "1px solid red",
						boxShadow: "0 0 2px red"
					});
					if(result=='-1')
					$("#userCue").html("<font color='red'><b>×用户名含关键字，不能使用！</b></font>");
					else if(result=='0')
					$("#userCue").html("<font color='red'><b>×用户名被占用！</b></font>");
					return false;
				} else {
					$('#u').css({
						border: "1px solid #D7D7D7",
						boxShadow: "none"
					});
				}

			}
		});
		/*var semail=/^[\w\-\.]+@[\w\-\.]+(\.\w+)+$/;
		if($('#email').val().length < 7 || !semail.test( $('#email').val())){
			$('#userCue').html("<font color='red'><b>×邮箱格式错误！</b></font>");
			$('#email').focus().css({
				border: "1px solid red",
				boxShadow: "0 0 2px red"
			});
			return false;
		} else {
			$('#email').css({
				border: "1px solid #D7D7D7",
				boxShadow: "none"
			});
		}*/
		var sqq = /^[1-9]{1}[0-9]{4,9}$/;
		if (!sqq.test($('#qq').val()) || $('#qq').val().length < 5 || $('#qq').val().length > 12) {
			$('#qq').focus().css({
				border: "1px solid red",
				boxShadow: "0 0 2px red"
			});
			$('#userCue').html("<font color='red'><b>×QQ号码格式不正确</b></font>");
			return false;
		} else {
			$('#qq').css({
				border: "1px solid #D7D7D7",
				boxShadow: "none"
			});
			
		}
		if ($('#p').val().length < 6) {
			$('#p').focus();
			$('#userCue').html("<font color='red'><b>×密码不能小于" + 6 + "位</b></font>");
			return false;
		} else {
			$('#p').css({
				border: "1px solid #D7D7D7",
				boxShadow: "none"
			});
		}
		if ($('#p2').val() != $('#p').val()) {
			$('#p2').focus();
			$('#userCue').html("<font color='red'><b>×两次密码不一致！</b></font>");
			return false;
		} else {
			$('#p2').css({
				border: "1px solid #D7D7D7",
				boxShadow: "none"
			});
		}
		var mobile = $('#phone').val();
		if(mobile.length==0) {
			$('#userCue').html("<font color='red'><b>×请输入手机号码！</b></font>");
			$('#phone').focus().css({
				border: "1px solid red",
				boxShadow: "0 0 2px red"
			});
			return false;
		}    
		if(mobile.length!=11) {
			$('#userCue').html("<font color='red'><b>×请输入有效的手机号码！</b></font>");
			$('#phone').focus().css({
				border: "1px solid red",
				boxShadow: "0 0 2px red"
			});
			return false;
		}
		var myreg = /^0?1[3|4|5|7|8][0-9]\d{8}$/;
		if(!myreg.test(mobile)) {
			$('#userCue').html("<font color='red'><b>×请输入有效的手机号码！</b></font>");
			$('#phone').focus().css({
				border: "1px solid red",
				boxShadow: "0 0 2px red"
			});
			return false;
		} else {
			$('#phone').css({
				border: "1px solid #D7D7D7",
				boxShadow: "none"
			});
		}
		<?php
			if($row['enabled'] == '1') {
		?>
		if(!vcode_bool) {
			layer.msg('图形验证码错误！', {shift: 6});
			change_vcode();
			$('.rotateMobile').show();
			return false;
		}
		var mcode = $('#mcode').val();
		if(mcode.length==0) {
			$('#userCue').html("<font color='red'><b>×请输入手机验证码！</b></font>");
			$('#mcode').focus().css({
				border: "1px solid red",
				boxShadow: "0 0 2px red"
			});
			return false;
		} else {
			$('#mcode').css({
				border: "1px solid #D7D7D7",
				boxShadow: "none"
			});
		}
		<?php } ?>
		
		$('#regUser').submit();
	}
</script>
<style type="text/css">
	.rotateMobile{position:absolute;z-index:999;left:50%;top:50%;margin-top:-77px;margin-left:-203px;background-color:#5fb878;padding:0 20px;display:none;color:#000}
	.rotateMobile h1{text-align:center;margin:18px 0;font-size:16px;font-weight:bold;}
	.btn-send{background-color:#ebebeb;padding:5px;text-decoration:none;border:1px solid #CCC;font-size:12px;margin-left:10px;color:#000;}
	.rotateMobile_c{padding:0 35px 0 0;margin-top:26px;overflow:hidden;text-align:right;font-size:12px}
	.rotateMobile_c input{width:145px;height:23px;border:1px #81d3ff solid;padding:2px 10px;color:#a0a0a0}
	.rotateMobile_c input#rcode{width:90px}
	.registercode{text-align:left;width:180px}
	.buttonBlu{border-color:#52b0e1;border-style:solid;border-width:1px;text-shadow:0 -1px 0 rgba(0,0,0,0.2);text-align:center;background:#52b0e1;-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:3px;outline:0;width:80px;height:30px;font-size:16px;letter-spacing:2px;margin: 15px 10px 15px 110px;margin-bottom:15px;color:#fff;cursor: pointer;}
	.cancelled{font-size:12px;cursor:pointer}
	#fancybox-close {position: absolute;top: -25px;right: -25px;	width: 50px;height: 50px;background-image: url('../../images/fancy_close.png');cursor: pointer;z-index: 1103;display: none;
	}
	span#userCue{width: 500px; height: 100%; text-align: center; display: block; color: red;}
</style>
</head>

<body>
<div class="mainBg" >
	<div class="logoBar w1000 m0 cf">
		<div class="logo fl">
			<a href="javascript:;"><img src='<?=$cfg['config']['logo']?>' border=0></a>
		</div>
		<p class="fr" style="height:50px;">            
            <a href="/apps/kefu.php"  class="regBtn trans03" style="margin-top:10px;background: #ee6229;color:#fff;" >客服中心</a>            
		</p>
	</div>
    <form action="?act=reg" method="post" enctype="application/x-www-form-urlencoded" id="regUser">
		<div class="loginBox f14">
			<div class="loginMain cf">            
				<div class="loginLeft fl">
					<div class="loginTitle">
						<p class="userReg"></p>
					</div>        
					<div class="loginForm">
						<div class="oneLine cf" style="height: 20px;"><span id="userCue"></span></div>
						<div class="oneLine cf">
							<span class="itemName">用户名</span>
							<span class="star">*</span>
							<span><input name="u" type="text" maxlength="16" id="u" placeholder="6-16位字符"></span>
						</div>
						<div class="oneLine cf">
							<span class="itemName">昵称</span>
							<span class="star">*</span>
							<span><input name="nick" type="text" maxlength="50" id="nick"></span>
						</div>
						<div id="MainContent_AccountMainContent_divQQ" class="oneLine cf">
							<span class="itemName">QQ</span>
							<span class="star">*</span>
							<span><input name="qq" type="text" id="qq" value=""></span>								
						</div>
						<div class="oneLine cf">
							<span class="itemName">密码</span>
							<span class="star">*</span>
							<span><input name="p" type="password" id="p" placeholder="6-15位，可包含字母、数字及特殊符号" value=""></span>
						</div>
						<div class="oneLine cf">
							<span class="itemName">确认密码</span>
							<span class="star">*</span>
							<span><input name="p2" type="password" id="p2"></span>
						</div>
						<div id="MainContent_AccountMainContent_divMobile" class="oneLine cf">
							<span class="itemName">手机</span>
							<span class="star">*</span>
							<span><input name="phone" type="text" id="phone" style="width: 192px;"></span>
							<?php
								if($row['enabled'] == '1') {
							?>
							<span style="color:red;"><a href="javascript:void(0);" onclick="sendSMS()" id="sendsms" style="width: 70px; display: inline-block; text-align: center; padding: 6px 10px; background-color: #CCC; color: #FFF;" >发送验证码</a></span>
							<?php } ?>
						</div>
						<?php
							if($row['enabled'] == '1') {
						?>
						<div id="MainContent_AccountMainContent_divMobile" class="oneLine cf">
							<span class="itemName">验证码</span>
							<span class="star">*</span>
							<span><input name="mcode" type="text" id="mcode"></span>
						</div>
						<?php } ?>
						<div class="oneLine cf">
							<span class="itemName"></span>
							<span class="star">&nbsp;</span>
							<div class="dib">
								<p>
									<button id="reg"  class="regBtn2 trans03" style="border:0px;" onclick="return register();">同意条款并注册</button>
								</p>
								<p class="pt20 cf tc">
									<a href="regagreement.html" target="_blank" class="forgot" >《网站服务条款》</a>
								</p>
							</div>
						</div>
						<?php
							if($cfg['config']['loginqq'] == '1') {
						?>
							<div class="oneLine cf" style="padding: 0 0 8px 30px;">						
								<p class="cf w">
									<span class="fl" style="height: 35px; line-height: 35px;">
										<i style="float: left;">您可以用合作伙伴账号登录：</i>
										<a href="../include/Connect2.1/oauth/index.php" title="QQ账号登录" class="qq login-api"><i></i>QQ</a>
									</span>
									<span class="fr"></span>
								</p>
							</div>
						<?php } ?>
					</div>
				</div>
				<div class="loginRight fl">
					<div class="loginTitle">
						<p class="toLogin"></p>
					</div>
					<a href="logging.php?rid=<?=$rid?>" class="loginBtn2 trans03 mt40">立即登录</a>
				</div>
			</div>
			<div class="loginBt"></div>
		</div>    
    </form>
    <div class="reg_footer w" >
		<div class="fLinks cf">
			<div class="w1000 m0">
				<span class="fl">友情链接：</span>
				<ul class="fl">
					<li><a href="http://www.95599.cn/cn/" target="_blank">农业银行</a></li>
					<li><a href="http://www.icbc.com.cn/icbc/" target="_blank">工商银行</a></li>
					<li><a href="http://www.ccb.com/" target="_blank">建设银行</a></li>
					<li><a href="http://www.boc.cn" target="_blank">中国银行</a></li>
				</ul>
			</div>
			<div></div>
		</div>
		<div class="copy">
			<div id="MainContent_footer_divFooterLog" class="w1000 m0 cf">            
				<div class="fl">
					<p class="cfff">投资有风险，入市须谨慎</p>
					<p><span ><?=tohtml($cfg['config']['copyright'])?></span></p>
					<p></p>
				</div>
				<p id="MainContent_footer_pLogo4RJ" class="fr pt10">&nbsp;</p>
			</div>
		</div>
	</div>
</div>
<div class="rotateMobile" style="display: none;">
	<h1>请输入验证码</h1>
	<div class="rotateMobile_c">
		验证码：
		<input type="text" title="请输入验证码" placeholder="请输入验证码" maxlength="11" id="vcode" name="vcode">
		<img id="imgCode" src="/include/code.php" onclick="change_vcode();" style="vertical-align: middle; cursor: pointer;"/>				
	</div>
	<button onclick="check_vcode()" class="buttonBlu">确定</button>
	<span onclick="$('.rotateMobile').hide()" class="cancelled">取消</span>
</div>
<?=$echo?>
</body>
</html>
