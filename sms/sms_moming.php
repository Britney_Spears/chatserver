<?php

/****--------------------------------
duanxin.cm莫名短信
功能:	duanxin.cm PHP HTTP接口 发送短信
http://pc.duanxin.cm/login
修改日期:	2014-03-19
说明:http://api.duanxin.cm/?action=send&username=账号&password=MD532位密码&phone=号码&content=内容
状态:
	100 发送成功
	101 验证失败
	102 短信不足
	103 操作失败
	104 非法字符
	105 内容过多
	106 号码过多
	107 频率过快
	108 号码内容空
	109 账号冻结
	110 禁止频繁单条发送
	111 系统暂定发送
	112 号码不正确
	120 系统升级
--------------------------------*/
/*$username = '70210821';		//用户账号
$password = 'jzz630828';		//密码
//$phone	 = '15527139903';	//号码
//$content = 'duanxin.cm PHP HTTP接口';		//内容
$phone = $_GET['phone'];
$p1 = rand(100000, 999999);
$_SESSION['mcode'] = $p1;
$content = '您的验证码是：'. $p1 .'，3分钟内有效。如非您本人操作，可忽略本消息。';
//即时发送
$res = sendSMS($username,$password,$phone,$content);
echo $res;exit;*/

$url='http://120.55.242.177/OpenPlatform/OpenApi';           //接口地址
//$ac='1001@800128740001';		                             //用户账号
//$authkey = '87E8F861217F1BA9D2735F747DE25EE1';		         //认证密钥
$ac=$row['account'];		                             	//用户账号
$authkey = $row['password'];		         				//认证密钥
$cgid=$row['remark'];                                       //通道组编号
$p1 = rand(100000, 999999);
$_SESSION['mcode'] = $p1;
$c = '您的验证码是：'. $p1 .'，3分钟内有效。如非您本人操作，可忽略本消息。';		 //内容
$m= $_GET['phone'];                                         //号码
$_SESSION['phone'] = $m;
$csid='3';                                                   //签名编号 ,可以为空时，使用系统默认的编号
$t='';                                                       //发送时间,可以为空表示立即发送,yyyyMMddHHmmss 如:20130721182038

$ret = sendSMS($url,$ac,$authkey,$cgid,$m,$c,$csid,$t);

                                                             //短信发送接口
function sendSMS($url,$ac,$authkey,$cgid,$m,$c,$csid,$t) {
	$ret['code'] = '-1';
	$ret['msg'] = '发送失败';
	$data = array
		(
		'action'=>'sendOnce',                                //发送类型 ，可以有sendOnce短信发送，sendBatch一对一发送，sendParam	动态参数短信接口
		'ac'=>$ac,					                         //用户账号
		'authkey'=>$authkey,	                             //认证密钥
		'cgid'=>$cgid,                                       //通道组编号
		'm'=>$m,		                                     //号码,多个号码用逗号隔开
		'c'=>$c,		                 //如果页面是gbk编码，则转成utf-8编码，如果是页面是utf-8编码，则不需要转码
		'csid'=>$csid,                                       //签名编号 ，可以为空，为空时使用系统默认的签名编号
		't'=>$t                                              //定时发送，为空时表示立即发送
		);
	$xml= postSMS($url,$data);			                     //POST方式提交
    $re=simplexml_load_string(utf8_encode($xml));
    //$res = (array)$res['result'];
	//return $res[0];
	if(trim($re['result'])==1) {                              //发送成功 ，返回企业编号，员工编号，发送编号，短信条数，单价，余额
		foreach ($re->Item as $item) { 
			$stat['msgid'] =trim((string)$item['msgid']);
			$stat['total']=trim((string)$item['total']);
			$stat['price']=trim((string)$item['price']);
			$stat['remain']=trim((string)$item['remain']);
			$stat_arr[]=$stat;
		}
		if(is_array($stat_arr)) {
			$ret['code'] = 1;
			$ret['msg'] = "发送成功,返回值为".$re['result'];
		}		
    } else { //发送失败的返回值
	     switch(trim($re['result'])) {
			case  0: $ret['msg'] = "帐户格式不正确(正确的格式为:员工编号@企业编号)";break; 
			case  -1: $ret['msg'] = "服务器拒绝(速度过快、限时或绑定IP不对等)如遇速度过快可延时再发";break;
			case  -2: $ret['msg'] = " 密钥不正确";break;
			case  -3: $ret['msg'] = "密钥已锁定";break;
			case  -4: $ret['msg'] = "参数不正确(内容和号码不能为空，手机号码数过多，发送时间错误等)";break;
			case  -5: $ret['msg'] = "无此帐户";break;
			case  -6: $ret['msg'] = "帐户已锁定或已过期";break;
			case  -7:$ret['msg'] = "帐户未开启接口发送";break;
			case  -8: $ret['msg'] = "不可使用该通道组";break;
			case  -9: $ret['msg'] = "帐户余额不足";break;
			case  -10: $ret['msg'] = "内部错误";break;
			case  -11: $ret['msg'] = "扣费失败";break;
			default:break;
		}
	}
	return $ret;
}

function postSMS($url,$data='') {
	$row = parse_url($url);
	$host = $row['host'];
	$port = $row['port'] ? $row['port']:80;
	$file = $row['path'];
	while (list($k,$v) = each($data)) {
		$post .= rawurlencode($k)."=".rawurlencode($v)."&";	//转URL标准码
	}
	$post = substr( $post , 0 , -1 );
	$len = strlen($post);
	$fp = @fsockopen( $host ,$port, $errno, $errstr, 10);
	if (!$fp) {
		return "$errstr ($errno)\n";
	} else {
		$receive = '';
		$out = "POST $file HTTP/1.0\r\n";
		$out .= "Host: $host\r\n";
		$out .= "Content-type: application/x-www-form-urlencoded\r\n";
		$out .= "Connection: Close\r\n";
		$out .= "Content-Length: $len\r\n\r\n";
		$out .= $post;		
		fwrite($fp, $out);
		while (!feof($fp)) {
			$receive .= fgets($fp, 128);
		}
		fclose($fp);
		$receive = explode("\r\n\r\n",$receive);
		unset($receive[0]);
		return implode("",$receive);
	}
}
?>