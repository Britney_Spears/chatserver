<?php
require_once './include/common.inc.php';
$echo="";
switch($act){
	case "login":
		$msg=user_login($username,$password);
		if($msg===true){
			setcookie("username", $username, gdate()+315360000);
			header("location:./room/index.php?rid=" . $rid);
		} else{
			$echo ="<script>layer.msg('{$msg}',{shift: 6});</script>";
		}
	break;
	case "logout":
		unset($_SESSION['login_uid']);
		unset($_SESSION['login_user']);
		session_destroy(); 
		header("location:room/index.php?rid=" . $rid);
	break;
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>登录 <?=$cfg['config']['title']?></title>
<meta property="qc:admins" content="3101043371273675551006375747771676000" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
<link rel="shortcut icon" type="image/x-icon" href="<?=$cfg['config']['ico']?>" />
<meta content="<?=$cfg['config']['keys']?>" name="keywords">
<meta content="<?=$cfg['config']['dc']?>" name="description">
<link href="room/images/base.css" rel="stylesheet" type="text/css"  />
<link href="room/images/login.css" rel="stylesheet" type="text/css"  />
<script src="room/script/jquery.min.js"></script>
<script src="room/script/layer.js"></script>
<script src="room/script/device.min.js"></script>
<script>
function openWin(type, title, content, w, h) {
    layer.closeAll('iframe');
    layer.open({
        type: type,
        title: title,
        shadeClose: true,
        shade: false,
        area: [w + 'px', h + 'px'],
        content: content //iframe的url
    });
}
var childWindow;
function toQzoneLogin() {
	location.href = 'include/Connect2.1/oauth/index.php';
	//childWindow = window.open("include/Connect2.1/oauth/index.php","TencentLogin","width=450,height=320,menubar=0,scrollbars=1, resizable=0,status=1,titlebar=0,toolbar=0,location=1");
} 

function closeChildWindow() {
	childWindow.close();
}
</script>
</head>

<body>
<script>if (device.mobile()){window.location = './room/minilogin.php?rid=<?php echo $rid; ?>';}</script>
<div class="mainBg">
<div class="logoBar w1000 m0 cf">
		<div class="logo fl">
			<a href="javascript:;"><img src='<?=$cfg['config']['logo']?>' border=0></a>
		</div>
		<p class="fr" style="height:50px;">
            
            <a href="javascript:void(0);" onClick="openWin(2,'客服列表','apps/kefu.php',810,500)" class="regBtn trans03" style="margin-top:10px;background: #ee6229;color:#fff;" >客服中心</a>
            
		</p>
	</div>
    <div class="loginBox f14">
		<div class="loginMain cf">
            
    <div class="loginLeft fl h330">
        <div class="loginTitle">
            <p class="userLogin"></p>
        </div>
        <form action="?act=login&rid=<?=$rid?>" method="post" enctype="application/x-www-form-urlencoded"  name="loginform"  id="login_form" class="loginForm" >
        <div class="loginForm">
            <div class="oneLine cf">
                <span class="itemName">用户名</span>
                <span class="star">&nbsp;</span>
                <span>
                    <input name="username" type="text" value="<?=$_COOKIE['username']?>"></span>
                <span class="tishi" style="display: none"><i class="dui"></i></span>
            </div>
            <div class="oneLine cf">
                <span class="itemName">密码</span>
                <span class="star">&nbsp;</span>
                <span>
                    <input name="password" type="password" ></span>
                <span class="tishi" style="display: none"><i class="cuo">密码错误</i></span>
            </div>
            
            
            <div class="oneLine cf">
                <span class="itemName">&nbsp;</span>
                <span class="star">&nbsp;</span>
                <div class="ie7LoginWidth dib cf">
                    <p class="pr">
                        <button id="lnkLogin" class="loginBtn trans03" style="border:0px;" type="submit">登录</button>
                      <a class="tiyan f14" href="room/index.php?rid=<?=$rid?>">游客体验</a>
                    </p>
                    <p class="pt20 cf w">
                        <span class="fl"><a href="javascript:void(0);" onclick="layer.msg('忘记密码？请联系客服！');" class="forgot">忘记密码？</a></span>
                        <span class="fr"></span>
                    </p>
					<?php
						if($cfg['config']['loginqq'] == '1') {
					?>
						<p class="pt20 cf w">
							<span class="fl" style="height: 35px; line-height: 35px;">
								<i style="float: left;">您可以用合作伙伴账号登录：</i>
								<a href="../include/Connect2.1/oauth/index.php" title="QQ账号登录" class="qq login-api"><i></i>QQ</a>
							</span>
							<span class="fr"></span>
						</p>
					<?php } ?>
                </div>
            </div>
        </div>
        </form>
        
    </div>
    <div class="loginRight fl">
        <div class="loginTitle">
            <p class="toReg"></p>
        </div>
        <a href="register.php?rid=<?=$rid?>" class="regBtn mt40 trans03">立即注册</a>
        
        <!--<p class="c999 pt30 f12">使用社交账号登录</p>
        <p><a  class="qq_login" href="javascript: void(0);" onclick='toQzoneLogin()'></a></p>
        <p><a  class="weibo_login" href=""></a></p>-->
		
    </div>


		</div>
		<div class="loginBt"></div>
	</div>
    <div class="login_footer w" >
		<div class="fLinks cf">
			<div class="w1000 m0">
				<span class="fl">友情链接：</span>
				<ul class="fl">
					<li><a href="http://www.95599.cn/cn/" target="_blank">农业银行</a></li>
					<li><a href="http://www.icbc.com.cn/icbc/" target="_blank">工商银行</a></li>
					<li><a href="http://www.ccb.com/" target="_blank">建设银行</a></li>
					<li><a href="http://www.boc.cn" target="_blank">中国银行</a></li>
				</ul>
			</div>
			<div>
				
				
			</div>
		</div>
		<div class="copy">
			<div id="MainContent_footer_divFooterLog" class="w1000 m0 cf">				
				<div class="fl">
					<p class="cfff">投资有风险，入市须谨慎</p>
					<p><span ><?=tohtml($cfg['config']['copyright'])?></span>   </p>
					<p>
						</p>
				</div>
				<p id="MainContent_footer_pLogo4RJ" class="fr pt10">&nbsp;</p>
			</div>
		</div>
	</div>
</div>

<?=$echo?>
</body>
</html>
